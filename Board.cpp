
/* 
 * File:   Board.cpp
 * Author: 475531
 * 
 * Created on 11 April 2013, 8:37 AM
 */

#include "Board.h"
#include "Move.h"
#include <cstdarg>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <iterator>

Board::Board() {
    setDefaultParameters();
}

Board::~Board() {
}

std::vector<double> Board::getParameters() const {
    std::vector<double> params;

    params.push_back(queenValue);
    params.push_back(rookValue);
    params.push_back(bishopValue);
    params.push_back(knightValue);
    params.push_back(pawnValue);
   // params.push_back(startMaterial);

    params.push_back(knightMobility);
    params.push_back(pawnMobility);
    params.push_back(rookMobility);
    params.push_back(bishopMobility);
    params.push_back(queenMobility);
    params.push_back(kingMobility);

    params.push_back(knightAttackWeight);
    params.push_back(rookAttackWeight);
    params.push_back(bishopAttackWeight);
    params.push_back(queenAttackWeight);

    params.push_back(passedPawnReward);
    params.push_back(doubledPawnPenalty);
    params.push_back(isolatedPawnPenalty);
    params.push_back(isolatedAdvancedPawnWeight);
    params.push_back(supportedAdvancedPawnWeight);

    params.push_back(poorPawnShieldPenalty);
    params.push_back(pawnShieldPenalty1);
    params.push_back(pawnShieldPenalty2);
    params.push_back(pawnShieldPenalty3);
    params.push_back(pawnShieldPenalty4);
    params.push_back(pawnShieldPenalty5);
    params.push_back(centralKingPenalty);
    params.push_back(pawnShieldPenalty6);
    params.push_back(pawnShieldPenalty7);
    params.push_back(pawnShieldPenalty8);
    params.push_back(pawnShieldPenalty9);

    return params;
}

void Board::setParameters(const std::vector<double>&params) {

    int expected = 31;
    if (params.size() != expected) {
        std::cerr << "Parameter length incorrect. Expected " << expected << ", but found " << params.size() << std::endl;
    } else {

        queenValue = floor(params[0] + 0.5);
        rookValue = floor(params[1] + 0.5);
        bishopValue = floor(params[2] + 0.5);
        knightValue = floor(params[3] + 0.5);
        pawnValue = floor(params[4] + 0.5);
        startMaterial = queenValue + 2 * (rookValue + bishopValue + knightValue) + 8 * pawnValue;
        //  startMaterial = floor(params[5] + 0.5);

        knightMobility = params[5];
        pawnMobility = params[6];
        rookMobility = params[7];
        bishopMobility = params[8];
        queenMobility = params[9];
        kingMobility = params[10];

        knightAttackWeight = floor(params[11] + 0.5);
        rookAttackWeight = floor(params[12] + 0.5);
        bishopAttackWeight = floor(params[13] + 0.5);
        queenAttackWeight = floor(params[14] + 0.5);

        passedPawnReward = floor(params[15] + 0.5);

        doubledPawnPenalty = floor(params[16] + 0.5);
        isolatedPawnPenalty = floor(params[17] + 0.5);
        isolatedAdvancedPawnWeight = params[18];
        supportedAdvancedPawnWeight = params[19];

        poorPawnShieldPenalty = floor(params[20] + 0.5);
        pawnShieldPenalty1 = floor(params[21] + 0.5);
        pawnShieldPenalty2 = floor(params[22] + 0.5);
        pawnShieldPenalty3 = floor(params[23] + 0.5);
        pawnShieldPenalty4 = floor(params[24] + 0.5);
        pawnShieldPenalty5 = floor(params[25] + 0.5);
        centralKingPenalty = floor(params[26] + 0.5);
        pawnShieldPenalty6 = floor(params[27] + 0.5);
        pawnShieldPenalty7 = floor(params[28] + 0.5);
        pawnShieldPenalty8 = floor(params[29] + 0.5);
        pawnShieldPenalty9 = floor(params[30] + 0.5);
    }
}

void Board::setDefaultParameters() {

    //32 params

    queenValue = DEFAULT_QUEEN_VALUE;
    rookValue = DEFAULT_ROOK_VALUE;
    bishopValue = DEFAULT_BISHOP_VALUE;
    knightValue = DEFAULT_KNIGHT_VALUE;
    pawnValue = DEFAULT_PAWN_VALUE;
    startMaterial = DEFAULT_START_MATERIAL;

    knightMobility = DEFAULT_KNIGHT_MOBILITY;
    pawnMobility = DEFAULT_PAWN_MOBILITY;
    rookMobility = DEFAULT_ROOK_MOBILITY;
    bishopMobility = DEFAULT_BISHOP_MOBILITY;
    queenMobility = DEFAULT_QUEEN_MOBILITY;
    kingMobility = DEFAULT_KING_MOBILITY;

    knightAttackWeight = DEFAULT_KNIGHT_ATTACK_WEIGHT;
    rookAttackWeight = DEFAULT_ROOK_ATTACK_WEIGHT;
    bishopAttackWeight = DEFAULT_BISHOP_ATTACK_WEIGHT;
    queenAttackWeight = DEFAULT_QUEEN_ATTACK_WEIGHT;

    passedPawnReward = DEFAULT_PASSED_PAWN_REWARD;
    doubledPawnPenalty = DEFAULT_DOUBLED_PAWN_PENALTY;
    isolatedPawnPenalty = DEFAULT_ISOLATED_PAWN_PENALTY;
    isolatedAdvancedPawnWeight = DEFAULT_ISOLATED_ADVANCED_PAWN_WEIGHT;
    supportedAdvancedPawnWeight = DEFAULT_SUPPORTED_ADVANCED_PAWN_WEIGHT;

    poorPawnShieldPenalty = DEFAULT_POOR_PAWN_SHIELD_PENALTY;
    pawnShieldPenalty1 = DEFAULT_PAWN_SHIELD_PENALTY_1;
    pawnShieldPenalty2 = DEFAULT_PAWN_SHIELD_PENALTY_2;
    pawnShieldPenalty3 = DEFAULT_PAWN_SHIELD_PENALTY_3;
    pawnShieldPenalty4 = DEFAULT_PAWN_SHIELD_PENALTY_4;
    pawnShieldPenalty5 = DEFAULT_PAWN_SHIELD_PENALTY_5;
    centralKingPenalty = DEFAULT_CENTRAL_KING_PENALTY;
    pawnShieldPenalty6 = DEFAULT_PAWN_SHIELD_PENALTY_6;
    pawnShieldPenalty7 = DEFAULT_PAWN_SHIELD_PENALTY_7;
    pawnShieldPenalty8 = DEFAULT_PAWN_SHIELD_PENALTY_8;
    pawnShieldPenalty9 = DEFAULT_PAWN_SHIELD_PENALTY_9;
}

int Board::isKQKEnding() {
    if ((bitboard::getSparseBitCount(WHITE_PIECES_BOARD) > 1) && (bitboard::getSparseBitCount(BLACK_PIECES_BOARD) > 1)) {
        return 0;
    }
    if (bitboard::getSparseBitCount(WHITE_PIECES_BOARD) > 1) {
        if (bitboard::getSparseBitCount(WHITE_PIECES_BOARD) != 2) {
            return 0;
        }
        if (bitboard::getSparseBitCount(WHITE_QUEEN_BOARD) > 0) {
            return WHITE;
        } else {
            return 0;
        }
    } else {
        if (bitboard::getSparseBitCount(BLACK_PIECES_BOARD) != 2) {
            return 0;
        }
        if (bitboard::getSparseBitCount(BLACK_QUEEN_BOARD) > 0) {
            return BLACK;
        } else {

            return 0;
        }
    }
}

int Board::isKRKEnding() {
    if ((bitboard::getSparseBitCount(WHITE_PIECES_BOARD) > 1) && (bitboard::getSparseBitCount(BLACK_PIECES_BOARD) > 1)) {
        return 0;
    }
    if (bitboard::getSparseBitCount(WHITE_PIECES_BOARD) > 1) {
        if (bitboard::getSparseBitCount(WHITE_PIECES_BOARD) != 2) {
            return 0;
        }
        if (bitboard::getSparseBitCount(WHITE_ROOK_BOARD) > 0) {
            return WHITE;
        } else {
            return 0;
        }
    } else {
        if (bitboard::getSparseBitCount(BLACK_PIECES_BOARD) != 2) {
            return 0;
        }
        if (bitboard::getSparseBitCount(BLACK_ROOK_BOARD) > 0) {
            return BLACK;
        } else {

            return 0;
        }
    }
}

bool Board::hasSufficientMaterial() {
    int P = bitboard::getDenseBitCount(WHITE_PAWN_BOARD); //??? sparse or dense
    int N = bitboard::getSparseBitCount(WHITE_KNIGHT_BOARD);
    int B = bitboard::getSparseBitCount(WHITE_BISHOP_BOARD);
    int R = bitboard::getSparseBitCount(WHITE_ROOK_BOARD);
    int Q = bitboard::getSparseBitCount(WHITE_QUEEN_BOARD);

    int p = bitboard::getDenseBitCount(BLACK_PAWN_BOARD); // ??? sparse or dense
    int n = bitboard::getSparseBitCount(BLACK_KNIGHT_BOARD);
    int b = bitboard::getSparseBitCount(BLACK_BISHOP_BOARD);
    int r = bitboard::getSparseBitCount(BLACK_ROOK_BOARD);
    int q = bitboard::getSparseBitCount(BLACK_QUEEN_BOARD);


    if (P + p + R + r + Q + q == 0) {
        if (((N + B) <= 1) && ((n + b) <= 1)) {

            return false;
        }
    }
    return true;
}

short Board::getMoves() {

    return moves;
}

void Board::swapSides() {

    side *= -1;
    updateHash(sideHash);
}

// <editor-fold defaultstate="collapsed" desc="Pseudo-Move Generator">

int* Board::generatePseudoMoves() const {

    //Non-Sliders
    std::vector <int> knightMoves = generateKnightMoves();
    int N = knightMoves.size();
    std::vector <int> kingMoves = generateKingMoves(); //excludes castling
    int K = kingMoves.size();
    std::vector <int> pawnMoves = generatePawnMoves();
    int P = pawnMoves.size();
    //sliders
    /**
     * I will NOT be implementing Magic bitboards at the moment. Maybe when there's
     * more time.
     */
    std::vector <int> rookMoves = generateRookMoves();
    int R = rookMoves.size();
    std::vector <int> bishopMoves = generateBishopMoves();
    int B = bishopMoves.size();
    std::vector <int> queenMoves = generateQueenMoves();
    int Q = queenMoves.size();
    //    special moves
    std::vector <int> castlingMoves = generateCastlingMoves();
    int C = castlingMoves.size();
    std::vector <int> enPassantMoves = generateEnPassantMoves();
    int E = enPassantMoves.size();


    int X = N + K + P + R + B + Q + C + E;

    int* moves = new int[X + 1];


    std::copy(enPassantMoves.begin(), enPassantMoves.end(), moves);
    std::copy(castlingMoves.begin(), castlingMoves.end(), moves + E);
    std::copy(knightMoves.begin(), knightMoves.end(), moves + E + C);
    std::copy(bishopMoves.begin(), bishopMoves.end(), moves + E + C + N);
    std::copy(pawnMoves.begin(), pawnMoves.end(), moves + E + C + N + B);
    std::copy(queenMoves.begin(), queenMoves.end(), moves + E + C + N + P + B);
    std::copy(rookMoves.begin(), rookMoves.end(), moves + E + C + B + P + Q + N);
    std::copy(kingMoves.begin(), kingMoves.end(), moves + E + C + B + P + Q + R + N);
    moves[X] = 0;

    return moves;
}
// </editor-fold>

/**
 * Whether the current side attacks any of the positions specified by the defend bitboard
 * @param defend
 * @return 
 */
bool Board::attacks(uint64_t defend) {

    uint64_t attack = 0ull;
    uint64_t bb, target;
    if (side == WHITE) {
        target = ~WHITE_PIECES_BOARD;
    } else {
        target = ~BLACK_PIECES_BOARD;
    }

    // <editor-fold defaultstate="collapsed" desc="Rook+Queen Attacks">
    if (side == WHITE) {
        bb = WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD;
    } else {
        bb = BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD;
    }
    while (bb > 0) {
        int idx = 63 - bitboard::LS1B(bb); //using 63 since I indexed a8 as 0
        uint64_t tMoves = getRookMoves(idx, target);
        attack |= tMoves;
        bb &= bb - 1;
        if ((attack & defend) > 0) {
            return true;
        }
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bishop+Queen Attacks">
    if (side == WHITE) {
        bb = WHITE_BISHOP_BOARD | WHITE_QUEEN_BOARD;
    } else {
        bb = BLACK_BISHOP_BOARD | BLACK_QUEEN_BOARD;
    }
    while (bb > 0) {
        int idx = 63 - bitboard::LS1B(bb); //using 63 since I indexed a8 as 0
        uint64_t tMoves = getBishopMoves(idx, target);
        attack |= tMoves;
        bb &= bb - 1;
        if ((attack & defend) > 0) {
            return true;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Knight Attacks">
    if (side == WHITE) {
        bb = WHITE_KNIGHT_BOARD;
    } else {
        bb = BLACK_KNIGHT_BOARD;
    }
    while (bb > 0) {
        int idx = 63 - bitboard::LS1B(bb); //using 63 since I indexed a8 as 0
        uint64_t tMoves = target & OCCUPANCY_MASK_KNIGHT[idx];
        attack |= tMoves;
        bb &= bb - 1; //remove lowest order bit
        if ((attack & defend) > 0) {
            return true;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="King Attacks">
    if (side == WHITE) {
        bb = WHITE_KING_BOARD;
    } else {
        bb = BLACK_KING_BOARD;
    }
    int idx = 63 - bitboard::LS1B(bb); //using 63 since I indexed a8 as 0
    uint64_t tMoves = target & OCCUPANCY_MASK_KING[idx];
    attack |= tMoves;
    if ((attack & defend) > 0) {
        return true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Pawn Attacks">

    const uint64_t* pawnAttack;

    if (side == WHITE) {
        bb = WHITE_PAWN_BOARD;
        pawnAttack = WHITE_PAWN_ATTACKS;
        target = BLACK_PIECES_BOARD;
    } else {
        bb = BLACK_PAWN_BOARD;
        pawnAttack = BLACK_PAWN_ATTACKS;
        target = WHITE_PIECES_BOARD;
    }

    while (bb > 0) {
        int idx = 63 - bitboard::LS1B(bb); //using 63 since I indexed a8 as 0
        uint64_t tAttacks = target & pawnAttack[idx];
        attack |= tAttacks;
        bb &= bb - 1; //remove lowest order bit
        if ((attack & defend) > 0) {

            return true;
        }
    }
    // </editor-fold>

    return false;
}

// <editor-fold defaultstate="collapsed" desc="OR all bitboards">

void Board::orBitboards(uint64_t bb, int piece) {
    switch (piece) {
        case WHITE_PAWN:
            WHITE_PAWN_BOARD |= bb;
            WHITE_PIECES_BOARD |= bb;
            break;
        case WHITE_KNIGHT:
            WHITE_KNIGHT_BOARD |= bb;
            WHITE_PIECES_BOARD |= bb;
            break;
        case WHITE_BISHOP:
            WHITE_BISHOP_BOARD |= bb;
            WHITE_PIECES_BOARD |= bb;
            break;
        case WHITE_KING:
            WHITE_KING_BOARD |= bb;
            WHITE_PIECES_BOARD |= bb;
            break;
        case WHITE_ROOK:
            WHITE_ROOK_BOARD |= bb;
            WHITE_PIECES_BOARD |= bb;
            break;
        case WHITE_QUEEN:
            WHITE_QUEEN_BOARD |= bb;
            WHITE_PIECES_BOARD |= bb;
            break;
        case BLACK_PAWN:
            BLACK_PIECES_BOARD |= bb;
            BLACK_PAWN_BOARD |= bb;
            break;
        case BLACK_KNIGHT:
            BLACK_KNIGHT_BOARD |= bb;
            BLACK_PIECES_BOARD |= bb;
            break;
        case BLACK_BISHOP:
            BLACK_BISHOP_BOARD |= bb;
            BLACK_PIECES_BOARD |= bb;
            break;
        case BLACK_KING:
            BLACK_KING_BOARD |= bb;
            BLACK_PIECES_BOARD |= bb;
            break;
        case BLACK_ROOK:
            BLACK_ROOK_BOARD |= bb;
            BLACK_PIECES_BOARD |= bb;
            break;
        case BLACK_QUEEN:
            BLACK_QUEEN_BOARD |= bb;
            BLACK_PIECES_BOARD |= bb;

            break;
    }
    ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="AND all bitboards">

void Board::andBitboards(uint64_t bb, int piece) {
    switch (piece) {
        case WHITE_PAWN:
            WHITE_PAWN_BOARD &= bb;
            WHITE_PIECES_BOARD &= bb;
            break;
        case WHITE_KNIGHT:
            WHITE_KNIGHT_BOARD &= bb;
            WHITE_PIECES_BOARD &= bb;
            break;
        case WHITE_BISHOP:
            WHITE_BISHOP_BOARD &= bb;
            WHITE_PIECES_BOARD &= bb;
            break;
        case WHITE_KING:
            WHITE_KING_BOARD &= bb;
            WHITE_PIECES_BOARD &= bb;
            break;
        case WHITE_ROOK:
            WHITE_ROOK_BOARD &= bb;
            WHITE_PIECES_BOARD &= bb;
            break;
        case WHITE_QUEEN:
            WHITE_QUEEN_BOARD &= bb;
            WHITE_PIECES_BOARD &= bb;
            break;
        case BLACK_PAWN:
            BLACK_PIECES_BOARD &= bb;
            BLACK_PAWN_BOARD &= bb;
            break;
        case BLACK_KNIGHT:
            BLACK_KNIGHT_BOARD &= bb;
            BLACK_PIECES_BOARD &= bb;
            break;
        case BLACK_BISHOP:
            BLACK_BISHOP_BOARD &= bb;
            BLACK_PIECES_BOARD &= bb;
            break;
        case BLACK_KING:
            BLACK_KING_BOARD &= bb;
            BLACK_PIECES_BOARD &= bb;
            break;
        case BLACK_ROOK:
            BLACK_ROOK_BOARD &= bb;
            BLACK_PIECES_BOARD &= bb;
            break;
        case BLACK_QUEEN:
            BLACK_QUEEN_BOARD &= bb;
            BLACK_PIECES_BOARD &= bb;

            break;
    }
    ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;
}
// </editor-fold>

void Board::movePiece(int piece, int dest, int source) {

    removePiece(piece, source);
    addPiece(piece, dest);
}

bool Board::canMakeNullMove() {

    uint64_t king, minor, major;
    if (side == WHITE) {
        king = WHITE_KING_BOARD;
        minor = WHITE_KNIGHT_BOARD | WHITE_BISHOP_BOARD;
        major = WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD;
    } else {
        king = BLACK_KING_BOARD;
        minor = BLACK_KNIGHT_BOARD | BLACK_BISHOP_BOARD;
        major = BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD;
    }


    swapSides();
    bool check = attacks(king);
    swapSides();

    if (check) {
        return false;
    }
    if (major > 0) {
        return true;
    }
    if (bitboard::getSparseBitCount(minor) < 2) {

        return false;
    }
    return true;

}

void Board::addPiece(int piece, int idx) {

    board[idx] = piece;
    uint64_t bb = bitboard::getBitBoard(63 - idx);
    orBitboards(bb, piece);
}

void Board::removePiece(int piece, int idx) {

    board[idx] = EMPTY_SQUARE;
    uint64_t bb = ~bitboard::getBitBoard(63 - idx);
    andBitboards(bb, piece);
}

short Board::getRepetitions() {

    uint64_t hash = getHash();
    short count = 0;
    for (int i = 0; i < repetitions.size(); i++) {
        if (repetitions[i] == hash) {

            count++;
        }
    }
    return count;

}

// <editor-fold defaultstate="collapsed" desc="Make Move Function">

bool Board::makeMove(int move) {



    struct History prev;
    prev.blackCastle = blackCastle;
    prev.whiteCastle = whiteCastle;
    prev.enPassant = enPassant;
    prev.fiftyMove = fiftyRule;


    int idx = (side == WHITE) ? 0 : 1;
    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    enPassant = -1;

    if (move == NULL_MOVE) {
        swapSides();
        history.push(prev);
        return true;
    }

    updateHash(whiteCastleHash[whiteCastle]);
    updateHash(blackCastleHash[blackCastle]);





    int source = move::getSource(move);
    int dest = move::getDestination(move);
    int cp = move::getCapturedPiece(move);
    int mp = move::getMovingPiece(move);
    int origSide = side;
    uint64_t defend;


    if ((abs(mp) != PAWN) && (cp == EMPTY_SQUARE)) {
        fiftyRule++;
    } else {
        fiftyRule = 0;
    }
    if (mp < 0) {
        moves++;
    }
    int type = move::getType(move);

    if (type == MOVE_NORMAL && cp == EMPTY_SQUARE) {
        updateHash(piecesHash[abs(mp) - 1][idx][source]);
        updateHash(piecesHash[abs(mp) - 1][idx][dest]);
    }

    switch (move::getType(move)) {
        case MOVE_NORMAL:
            if (cp != EMPTY_SQUARE) {
                updateHash(piecesHash[abs(mp) - 1][idx][source]);
                updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                updateHash(piecesHash[abs(mp) - 1][idx][dest]);
                removePiece(cp, dest);
            }
            movePiece(mp, dest, source);

            if (abs(mp) == PAWN && abs(dest - source) == 16) {
                enPassant = (source + dest) / 2;
            }
            break;
        case MOVE_SHORT_CASTLE:
            updateHash(piecesHash[abs(mp) - 1][idx][source]);
            updateHash(piecesHash[abs(mp) - 1][idx][dest]);
            if (mp == WHITE_KING) {
                updateHash(piecesHash[ROOK - 1][idx][63]);
                updateHash(piecesHash[ROOK - 1][idx][61]);
                movePiece(WHITE_ROOK, 61, 63);
                movePiece(WHITE_KING, 62, 60);
                whiteCastle = WHITE_CASTLE_NONE;
            } else if (mp == BLACK_KING) {


                updateHash(piecesHash[ROOK - 1][idx][7]);
                updateHash(piecesHash[ROOK - 1][idx][5]);

                movePiece(BLACK_ROOK, 5, 7);
                movePiece(BLACK_KING, 6, 4);

                blackCastle = BLACK_CASTLE_NONE;
            }
            break;


        case MOVE_LONG_CASTLE:

            updateHash(piecesHash[abs(mp) - 1][idx][source]);
            updateHash(piecesHash[abs(mp) - 1][idx][dest]);

            if (mp == WHITE_KING) {

                updateHash(piecesHash[ROOK - 1][idx][56]);
                updateHash(piecesHash[ROOK - 1][idx][59]);

                movePiece(WHITE_ROOK, 59, 56);
                movePiece(WHITE_KING, 58, 60);

                whiteCastle = WHITE_CASTLE_NONE;
            } else if (mp == BLACK_KING) {

                updateHash(piecesHash[ROOK - 1][idx][0]);
                updateHash(piecesHash[ROOK - 1][idx][3]);

                movePiece(BLACK_ROOK, 3, 0);
                movePiece(BLACK_KING, 2, 4);
                blackCastle = BLACK_CASTLE_NONE;

            }
            break;

        case MOVE_CAPTURE_EN_PASSANT:

            updateHash(piecesHash[abs(mp) - 1][idx][source]);
            updateHash(piecesHash[abs(mp) - 1][idx][dest]);
            movePiece(mp, dest, source);
            if (mp == WHITE_PAWN) {
                removePiece(BLACK_PAWN, dest + 8);
                //Remove black pawn
                updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest + 8]);
            } else {
                removePiece(WHITE_PAWN, dest - 8);
                updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest - 8]);
            }
            break;
        default:
            //PROMOTION
            updateHash(piecesHash[PAWN - 1][idx][source]);


            if (cp != EMPTY_SQUARE) {
                removePiece(cp, dest);
                updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
            }

            removePiece(mp, source);

            switch (type) {
                case MOVE_PROMOTION_QUEEN:
                    updateHash(piecesHash[QUEEN - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_QUEEN_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_QUEEN_BOARD;
                    } else {
                        BLACK_QUEEN_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_QUEEN_BOARD;
                    }
                    board[dest] = QUEEN * (side);
                    break;
                case MOVE_PROMOTION_ROOK:
                    updateHash(piecesHash[ROOK - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_ROOK_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_ROOK_BOARD;
                    } else {
                        BLACK_ROOK_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_ROOK_BOARD;
                    }
                    board[dest] = ROOK * (side);
                    break;
                case MOVE_PROMOTION_BISHOP:
                    updateHash(piecesHash[BISHOP - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_BISHOP_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_BISHOP_BOARD;
                    } else {
                        BLACK_BISHOP_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_BISHOP_BOARD;
                    }
                    board[dest] = BISHOP * (side);
                    break;
                case MOVE_PROMOTION_KNIGHT:
                    updateHash(piecesHash[KNIGHT - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_KNIGHT_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_KNIGHT_BOARD;
                    } else {
                        BLACK_KNIGHT_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_KNIGHT_BOARD;
                    }
                    board[dest] = KNIGHT * (side);
                    break;
            }

            // board[source] = EMPTY_SQUARE;
            break;
    }
    if (whiteCastle != WHITE_CASTLE_NONE
            || blackCastle != BLACK_CASTLE_NONE) {

        if (board[60] != WHITE_KING) {
            whiteCastle = WHITE_CASTLE_NONE;
        }
        if (board[4] != BLACK_KING) {
            blackCastle = BLACK_CASTLE_NONE;
        }
        if (board[56] != WHITE_ROOK) {
            if (whiteCastle == WHITE_CASTLE_BOTH
                    || whiteCastle == WHITE_CASTLE_SHORT) {
                whiteCastle = WHITE_CASTLE_SHORT;
            } else {
                whiteCastle = WHITE_CASTLE_NONE;
            }
        }
        if (board[63] != WHITE_ROOK) {
            if (whiteCastle == WHITE_CASTLE_BOTH
                    || whiteCastle == WHITE_CASTLE_LONG) {
                whiteCastle = WHITE_CASTLE_LONG;
            } else {
                whiteCastle = WHITE_CASTLE_NONE;
            }
        }

        if (board[0] != BLACK_ROOK) {
            if (blackCastle == BLACK_CASTLE_BOTH
                    || blackCastle == BLACK_CASTLE_SHORT) {
                blackCastle = BLACK_CASTLE_SHORT;
            } else {
                blackCastle = BLACK_CASTLE_NONE;
            }
        }

        if (board[7] != BLACK_ROOK) {
            if (blackCastle == BLACK_CASTLE_BOTH
                    || blackCastle == BLACK_CASTLE_LONG) {
                blackCastle = BLACK_CASTLE_LONG;
            } else {
                blackCastle = BLACK_CASTLE_NONE;
            }
        }
    }
    updateHash(whiteCastleHash[whiteCastle]);
    updateHash(blackCastleHash[blackCastle]);

    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    if (mp == WHITE_KING) {
        whiteKing = dest;
    } else if (mp == BLACK_KING) {
        blackKing = dest;
    }

    swapSides();
    history.push(prev);
    repetitions.push_back(this->getHash());
    ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;
    if (type == MOVE_SHORT_CASTLE) {
        if (origSide == WHITE) {
            if (((BLACK_PAWN_BOARD & PREVENT_WHITE_SHORT_CASTLE) > 0)) {
                return false;
            }
        } else {
            if (((WHITE_PAWN_BOARD & PREVENT_BLACK_SHORT_CASTLE) > 0)) {
                return false;
            }
        }
        defend = (origSide == WHITE) ? E1F1G1_MASK : E8F8G8_MASK;
    } else if (type == MOVE_LONG_CASTLE) {
        if (origSide == WHITE) {
            if ((BLACK_PAWN_BOARD & PREVENT_WHITE_LONG_CASTLE) > 0) {
                return false;
            }
        } else {
            if ((WHITE_PAWN_BOARD & PREVENT_BLACK_LONG_CASTLE) > 0) {
                return false;
            }
        }
        defend = (origSide == WHITE) ? C1D1E1_MASK : C8D8E8_MASK;
    } else {

        defend = (origSide == WHITE) ? WHITE_KING_BOARD : BLACK_KING_BOARD;
    }
    return !attacks(defend);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Undo Move Function">

bool Board::undoMove(int move) {
    try {
        struct History temp = history.top();
        history.pop();

        if (move == NULL_MOVE) {
            enPassant = temp.enPassant;
            whiteCastle = temp.whiteCastle;
            blackCastle = temp.blackCastle;
            fiftyRule = temp.fiftyMove;
            if (enPassant != -1) {
                updateHash(enPassantHash[enPassant]);
            }
            swapSides();
            return true;
        }

        repetitions.pop_back();
        if (enPassant != -1) {
            updateHash(enPassantHash[enPassant]);
        }

        updateHash(whiteCastleHash[whiteCastle]);
        updateHash(blackCastleHash[blackCastle]);
        enPassant = temp.enPassant;
        whiteCastle = temp.whiteCastle;
        blackCastle = temp.blackCastle;
        fiftyRule = temp.fiftyMove;

        if (enPassant != -1) {
            updateHash(enPassantHash[enPassant]);
        }

        updateHash(whiteCastleHash[whiteCastle]);
        updateHash(blackCastleHash[blackCastle]);

        swapSides();

        int idx = side == WHITE ? 0 : 1;

        int mp = move::getMovingPiece(move);
        int source = move::getSource(move);
        int dest = move::getDestination(move);
        int type = move::getType(move);

        if (mp < 0) {
            moves--;
        }

        switch (type) {
            case MOVE_SHORT_CASTLE:

                updateHash(piecesHash[abs(mp) - 1][idx][source]);
                updateHash(piecesHash[abs(mp) - 1][idx][dest]);

                if (mp == WHITE_KING) {

                    updateHash(piecesHash[ROOK - 1][idx][61]);
                    updateHash(piecesHash[ROOK - 1][idx][63]);
                    movePiece(WHITE_KING, 60, 62);
                    movePiece(WHITE_ROOK, 63, 61);
                } else if (mp == BLACK_KING) {

                    updateHash(piecesHash[ROOK - 1][idx][5]);
                    updateHash(piecesHash[ROOK - 1][idx][7]);
                    movePiece(BLACK_KING, 4, 6);
                    movePiece(BLACK_ROOK, 7, 5);
                }
                break;
            case MOVE_LONG_CASTLE:

                updateHash(piecesHash[abs(mp) - 1][idx][source]);
                updateHash(piecesHash[abs(mp) - 1][idx][dest]);

                if (mp == WHITE_KING) {


                    updateHash(piecesHash[ROOK - 1][idx][56]);
                    updateHash(piecesHash[ROOK - 1][idx][59]);

                    movePiece(WHITE_KING, 60, 58);
                    movePiece(WHITE_ROOK, 56, 59);

                } else if (mp == BLACK_KING) {

                    updateHash(piecesHash[ROOK - 1][idx][3]);
                    updateHash(piecesHash[ROOK - 1][idx][0]);

                    movePiece(BLACK_KING, 4, 2);
                    movePiece(BLACK_ROOK, 0, 3);
                }
                break;
            case MOVE_CAPTURE_EN_PASSANT:

                movePiece(mp, source, dest);

                updateHash(piecesHash[abs(mp) - 1][idx][source]);
                updateHash(piecesHash[abs(mp) - 1][idx][dest]);

                if (side == WHITE) {


                    addPiece(BLACK_PAWN, dest + 8);
                    updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest + 8]);
                } else {
                    addPiece(WHITE_PAWN, dest - 8);
                    updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest - 8 ]);
                }
                break;

            default:
            {
                int cp = move::getCapturedPiece(move);

                switch (type) {
                    case MOVE_NORMAL:
                        updateHash(piecesHash[abs(mp) - 1][idx][source]);
                        updateHash(piecesHash[abs(mp) - 1][idx][dest]);

                        if (cp == EMPTY_SQUARE) {
                            movePiece(mp, source, dest);
                        } else {
                            updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                            movePiece(mp, source, dest);
                            addPiece(cp, dest);
                        }
                        break;

                    case MOVE_PROMOTION_QUEEN:

                        updateHash(piecesHash[PAWN - 1][idx][source]);
                        updateHash(piecesHash[QUEEN - 1][idx][dest]);

                        if (cp == EMPTY_SQUARE) {
                            removePiece(QUEEN*side, dest);
                            addPiece(PAWN*side, source);
                        } else {
                            updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                            removePiece(QUEEN*side, dest);
                            addPiece(PAWN*side, source);
                            addPiece(cp, dest);
                        }

                        break;
                    case MOVE_PROMOTION_ROOK:

                        updateHash(piecesHash[PAWN - 1][idx][source]);
                        updateHash(piecesHash[ROOK - 1][idx][dest]);

                        if (cp == EMPTY_SQUARE) {
                            removePiece(ROOK*side, dest);
                            addPiece(PAWN*side, source);
                        } else {
                            updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                            removePiece(ROOK*side, dest);
                            addPiece(PAWN*side, source);
                            addPiece(cp, dest);
                        }

                        break;
                    case MOVE_PROMOTION_BISHOP:

                        updateHash(piecesHash[PAWN - 1][idx][source]);
                        updateHash(piecesHash[BISHOP - 1][idx][dest]);

                        if (cp == EMPTY_SQUARE) {
                            removePiece(BISHOP*side, dest);
                            addPiece(PAWN*side, source);
                        } else {
                            updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                            removePiece(BISHOP*side, dest);
                            addPiece(PAWN*side, source);
                            addPiece(cp, dest);
                        }
                        break;
                    case MOVE_PROMOTION_KNIGHT:
                        updateHash(piecesHash[PAWN - 1][idx][source]);
                        updateHash(piecesHash[KNIGHT - 1][idx][dest]);

                        if (cp == EMPTY_SQUARE) {
                            removePiece(KNIGHT*side, dest);
                            addPiece(PAWN*side, source);
                        } else {
                            updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                            removePiece(KNIGHT*side, dest);
                            addPiece(PAWN*side, source);
                            addPiece(cp, dest);
                        }
                        break;

                }

            }
        }

        if (mp == WHITE_KING) {
            whiteKing = source;
        } else if (mp == BLACK_KING) {
            blackKing = source;
        }
        ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
        EMPTY_BOARD = ~ALL_PIECES_BOARD;
        return true;
    } catch (int e) {
        std::cout << "Error occured in undo move: " << e << "\n";

        return false;
    }
}
// </editor-fold>

short Board::getSide() {

    return side;
}

void Board::shiftPieceBoards(int N) {

    WHITE_PAWN_BOARD <<= N;
    BLACK_PAWN_BOARD <<= N;
    WHITE_KNIGHT_BOARD <<= N;
    BLACK_KNIGHT_BOARD <<= N;
    WHITE_BISHOP_BOARD <<= N;
    BLACK_BISHOP_BOARD <<= N;
    WHITE_ROOK_BOARD <<= N;
    BLACK_ROOK_BOARD <<= N;
    WHITE_QUEEN_BOARD <<= N;
    BLACK_QUEEN_BOARD <<= N;
    WHITE_KING_BOARD <<= N;
    BLACK_KING_BOARD <<= N;

}

// <editor-fold defaultstate="collapsed" desc="Return FEN-string">

std::string Board::getPosition() {

    std::string fen = "";
    int count = 0;
    int spaces;
    for (int i = 0; i < 8; i++) {
        spaces = 0;
        for (int j = 0; j < 8; j++) {
            switch (board[count]) {
                case WHITE_KING:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "K";
                    break;
                case BLACK_KING:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "k";
                    break;
                case WHITE_QUEEN:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "Q";
                    break;
                case BLACK_QUEEN:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "q";
                    break;
                case WHITE_ROOK:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "R";
                    break;
                case BLACK_ROOK:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "r";
                    break;
                case WHITE_BISHOP:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "B";
                    break;
                case BLACK_BISHOP:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "b";
                    break;
                case WHITE_KNIGHT:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "N";
                    break;
                case BLACK_KNIGHT:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "n";
                    break;
                case WHITE_PAWN:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "P";
                    break;
                case BLACK_PAWN:
                    if (spaces > 0) {
                        fen += ntos(spaces);
                        spaces = 0;
                    }
                    fen += "p";
                    break;
                default:
                    spaces++;
            }
            count++;
        }
        if (spaces > 0) {
            fen += ntos(spaces);
            spaces = 0;
        }
        if (count < 63) {
            fen += "/";
        }
    }
    fen += " ";

    fen += (side == WHITE) ? "w " : "b ";

    if ((whiteCastle == WHITE_CASTLE_NONE) && (blackCastle == BLACK_CASTLE_NONE)) {
        fen += "-";
    } else {
        if (whiteCastle == WHITE_CASTLE_BOTH) {
            fen += "KQ";
        } else if (whiteCastle == WHITE_CASTLE_SHORT) {
            fen += "K";
        } else if (whiteCastle == WHITE_CASTLE_LONG) {
            fen += "Q";
        }
        if (blackCastle == BLACK_CASTLE_BOTH) {
            fen += "kq";
        } else if (blackCastle == BLACK_CASTLE_SHORT) {
            fen += "k";
        } else if (blackCastle == BLACK_CASTLE_LONG) {
            fen += "q";
        }
    }
    fen += " ";

    if (enPassant == -1) {
        fen += "-";
    } else {
        char c = 'a' + (enPassant % 8);
        fen += c;

        c = '8' - (enPassant / 8);
        fen += c;
    }
    fen += " " + ntos(fiftyRule) + " " + ntos(moves);

    return fen;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FEN-string setup">

bool Board::setPosition(std::string fen) {


    std::vector<std::string> arr;
    std::istringstream iss(fen);
    std::copy(std::istream_iterator<std::string > (iss),
            std::istream_iterator<std::string > (),
            std::back_inserter<std::vector<std::string> >(arr));


    if (arr.size() != 6) {
        return false;
    }


    WHITE_PAWN_BOARD = BLACK_PAWN_BOARD = WHITE_KNIGHT_BOARD = BLACK_KNIGHT_BOARD =
            WHITE_BISHOP_BOARD = BLACK_BISHOP_BOARD = WHITE_ROOK_BOARD = BLACK_ROOK_BOARD =
            WHITE_QUEEN_BOARD = BLACK_QUEEN_BOARD = WHITE_KING_BOARD = BLACK_KING_BOARD =
            WHITE_PIECES_BOARD = BLACK_PIECES_BOARD = ALL_PIECES_BOARD = 0;

    whiteKing = blackKing = -1;

    whiteCastle = WHITE_CASTLE_NONE;
    blackCastle = BLACK_CASTLE_NONE;

    hash[0] = hash[1] = 0;
    std::string position = arr[0];
    side = (arr[1] == "w") ? WHITE : BLACK;
    std::string castling = arr[2];
    std::string ep = arr[3];
    fiftyRule = atoi(arr[4].c_str());
    moves = atoi(arr[5].c_str());


    memset(board, 0, sizeof (board));

    int pos = 0;
    for (std::string::size_type i = 0; i < position.size(); i++) {


        if (position[i] != '/') {
            shiftPieceBoards(1);
            switch (position[i]) {
                case 'K':
                    whiteKing = pos;
                    board[pos] = WHITE_KING;
                    WHITE_KING_BOARD |= 1;
                    updateHash(piecesHash[KING - 1][0][pos]);
                    break;
                case 'k':
                    blackKing = pos;
                    board[pos] = BLACK_KING;
                    BLACK_KING_BOARD |= 1;
                    updateHash(piecesHash[KING - 1][1][pos]);
                    break;
                case 'Q':
                    board[pos] = WHITE_QUEEN;
                    WHITE_QUEEN_BOARD |= 1;
                    updateHash(piecesHash[QUEEN - 1][0][pos]);
                    break;
                case 'q':
                    board[pos] = BLACK_QUEEN;
                    BLACK_QUEEN_BOARD |= 1;
                    updateHash(piecesHash[QUEEN - 1][1][pos]);
                    break;
                case 'R':
                    board[pos] = WHITE_ROOK;
                    WHITE_ROOK_BOARD |= 1;
                    updateHash(piecesHash[ROOK - 1][0][pos]);
                    break;
                case 'r':
                    board[pos] = BLACK_ROOK;
                    BLACK_ROOK_BOARD |= 1;
                    updateHash(piecesHash[ROOK - 1][1][pos]);
                    break;
                case 'B':
                    board[pos] = WHITE_BISHOP;
                    WHITE_BISHOP_BOARD |= 1;
                    updateHash(piecesHash[BISHOP - 1][0][pos]);
                    break;
                case 'b':
                    board[pos] = BLACK_BISHOP;
                    BLACK_BISHOP_BOARD |= 1;
                    updateHash(piecesHash[BISHOP - 1][1][pos]);
                    break;
                case 'N':
                    board[pos] = WHITE_KNIGHT;
                    WHITE_KNIGHT_BOARD |= 1;
                    updateHash(piecesHash[KNIGHT - 1][0][pos]);
                    break;
                case 'n':
                    board[pos] = BLACK_KNIGHT;
                    BLACK_KNIGHT_BOARD |= 1;
                    updateHash(piecesHash[KNIGHT - 1][1][pos]);
                    break;
                case 'P':
                    board[pos] = WHITE_PAWN;
                    WHITE_PAWN_BOARD |= 1;
                    updateHash(piecesHash[PAWN - 1][0][pos]);
                    break;
                case 'p':
                    board[pos] = BLACK_PAWN;
                    BLACK_PAWN_BOARD |= 1;
                    updateHash(piecesHash[PAWN - 1][1][pos]);
                    break;
                default:
                    pos += (position[i] - '0') - 1;
                    shiftPieceBoards((position[i] - '0') - 1);
                    break;
            }
            pos++;
        }
    }
    WHITE_PIECES_BOARD = WHITE_PAWN_BOARD | WHITE_KNIGHT_BOARD | WHITE_BISHOP_BOARD
            | WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD | WHITE_KING_BOARD;
    BLACK_PIECES_BOARD = BLACK_PAWN_BOARD | BLACK_KNIGHT_BOARD | BLACK_BISHOP_BOARD
            | BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD | BLACK_KING_BOARD;
    ALL_PIECES_BOARD = WHITE_PIECES_BOARD | BLACK_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;


    //std::cout<<bin(MASK_RANK_8);
    for (std::string::size_type i = 0; i < castling.size(); i++) {

        switch (castling[i]) {

            case 'K':
                whiteCastle = WHITE_CASTLE_SHORT;
                break;
            case 'Q':
                if (whiteCastle == WHITE_CASTLE_SHORT) {
                    whiteCastle = WHITE_CASTLE_BOTH;
                } else {
                    whiteCastle = WHITE_CASTLE_LONG;
                }
                break;
            case 'k':
                blackCastle = BLACK_CASTLE_SHORT;
                break;
            case 'q':
                if (blackCastle == BLACK_CASTLE_SHORT) {
                    blackCastle = BLACK_CASTLE_BOTH;
                } else {
                    blackCastle = BLACK_CASTLE_LONG;
                }
                break;
        }
    }

    if (ep == "-") {
        enPassant = -1;
    } else {
        enPassant = (ep[0] - 'a') + ROW * (ROW - (ep[1] - '0'));
    }

    updateHash(whiteCastleHash[whiteCastle]);
    updateHash(blackCastleHash[blackCastle]);

    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    if (side == BLACK) {
        updateHash(sideHash);
    }

    movesPlayed.clear();
    repetitions.clear();
    repetitions.push_back(getHash());
    while (!history.empty()) {

        history.pop();
    }
    return true;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Draws the board">

void Board::show() {



    //  std::cout << "Lock: " << getLock() << std::endl;

    for (int i = 0; i < 64; i++) {
        if (i != 0 && i % 8 == 0) {
            std::cout << "\n";
        }
        short c = board[i];

        char k;

        switch (c) {
            case WHITE_PAWN:
                k = 'P';
                break;
            case WHITE_KNIGHT:
                k = 'N';
                break;
            case WHITE_BISHOP:
                k = 'B';
                break;
            case WHITE_KING:
                k = 'K';
                break;
            case WHITE_ROOK:
                k = 'R';
                break;
            case WHITE_QUEEN:
                k = 'Q';
                break;
            case BLACK_PAWN:
                k = 'p';
                break;
            case BLACK_KNIGHT:
                k = 'n';
                break;
            case BLACK_BISHOP:
                k = 'b';
                break;
            case BLACK_KING:
                k = 'k';
                break;
            case BLACK_ROOK:
                k = 'r';
                break;
            case BLACK_QUEEN:
                k = 'q';
                break;
            default:
                k = '.';
        }
        std::cout << k << " ";
    }
    std::cout << "\n" << "\n";

    return;
    std::cout << "White King: \n";
    bitboard::out(WHITE_KING_BOARD);

    std::cout << "White Queen: \n";
    bitboard::out(WHITE_QUEEN_BOARD);

    std::cout << "White Bishop: \n";
    bitboard::out(WHITE_BISHOP_BOARD);

    std::cout << "White Knight: \n";
    bitboard::out(WHITE_KNIGHT_BOARD);

    std::cout << "White Rook: \n";
    bitboard::out(WHITE_ROOK_BOARD);

    std::cout << "White Pawn: \n";
    bitboard::out(WHITE_PAWN_BOARD);

    std::cout << "Black King: \n";
    bitboard::out(BLACK_KING_BOARD);

    std::cout << "Black All: \n";
    bitboard::out(BLACK_PIECES_BOARD);

    std::cout << "Black Queen: \n";
    bitboard::out(BLACK_QUEEN_BOARD);

    std::cout << "Black Bishop: \n";
    bitboard::out(BLACK_BISHOP_BOARD);

    std::cout << "Black Knight: \n";
    bitboard::out(BLACK_KNIGHT_BOARD);

    std::cout << "Black Rook: \n";
    bitboard::out(BLACK_ROOK_BOARD);

    std::cout << "Black Pawn: \n";
    bitboard::out(BLACK_PAWN_BOARD);



}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Castling Move Generation">

std::vector<int> Board::generateCastlingMoves() const {
    std::vector<int> moves;
    if (side == WHITE) {
        bool flag = (whiteCastle == WHITE_CASTLE_BOTH);
        if ((whiteCastle == WHITE_CASTLE_SHORT || flag) && ((ALL_PIECES_BOARD & F1G1_MASK) == 0)) {
            bool check = ((getRookMoves(61, ~WHITE_PIECES_BOARD) & BLACK_KING_BOARD) > 0);
            int move = move::construct(60, 62, WHITE_KING, EMPTY_SQUARE, MOVE_SHORT_CASTLE,
                    check, false);
            moves.push_back(move);
        } else {
            int k = 0;
        }

        if ((flag || whiteCastle == WHITE_CASTLE_LONG) && ((ALL_PIECES_BOARD & B1C1D1_MASK) == 0)) {
            bool check = ((getRookMoves(59, ~WHITE_PIECES_BOARD) & BLACK_KING_BOARD) > 0);
            int move = move::construct(60, 58, WHITE_KING, EMPTY_SQUARE, MOVE_LONG_CASTLE,
                    check, false);
            moves.push_back(move);
        }
        return moves;
    } else {
        bool flag = (blackCastle == BLACK_CASTLE_BOTH);
        if (flag || blackCastle == BLACK_CASTLE_SHORT) {
            if ((ALL_PIECES_BOARD & F8G8_MASK) == 0) {
                bool check = ((getRookMoves(5, ~BLACK_PIECES_BOARD) & WHITE_KING_BOARD) > 0);
                int move = move::construct(4, 6, BLACK_KING, EMPTY_SQUARE, MOVE_SHORT_CASTLE,
                        check, false);
                moves.push_back(move);
            }
        }
        if (flag || blackCastle == BLACK_CASTLE_LONG) {
            if ((ALL_PIECES_BOARD & B8C8D8_MASK) == 0) {

                bool check = ((getRookMoves(3, ~BLACK_PIECES_BOARD) & WHITE_KING_BOARD) > 0);
                int move = move::construct(4, 2, BLACK_KING, EMPTY_SQUARE, MOVE_LONG_CASTLE,
                        check, false);
                moves.push_back(move);
            }
        }
        return moves;
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Queen Move Generation">

std::vector<int> Board::generateQueenMoves() const {

    std::vector<int> moves;
    uint64_t bb, target, king;
    if (side == WHITE) {
        bb = WHITE_QUEEN_BOARD;
        king = BLACK_KING_BOARD;
        target = ~WHITE_PIECES_BOARD;
    } else {
        bb = BLACK_QUEEN_BOARD;
        king = WHITE_KING_BOARD;
        target = ~BLACK_PIECES_BOARD;
    }

    while (bb > 0) {
        int idx = 63 - bitboard::LS1B(bb); //using 63 since I indexed a8 as 0
        uint64_t tMoves = getBishopMoves(idx, target) | getRookMoves(idx, target);
        while (tMoves > 0) {
            //get moves from bitboard

            int dest = 63 - bitboard::LS1B(tMoves);
            bool discoveredCheck = false;
            uint64_t temp = getBishopMoves(dest, target) | getRookMoves(dest, target);
            bool directCheck = (temp & king) > 0;
            int move = move::construct(idx, dest, side * QUEEN, board[dest], MOVE_NORMAL,
                    directCheck, discoveredCheck);
            moves.push_back(move);
            tMoves &= tMoves - 1;
        }
        bb &= bb - 1;
    }
    return moves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Bishop Move Generation Helper">

uint64_t Board::getBishopMoves(int from, uint64_t target, uint64_t pieces) const {

    uint64_t urMoves = NE_BOARD[from] & pieces;
    urMoves = (urMoves << 7) | (urMoves << 14) | (urMoves << 21) | (urMoves << 28) | (urMoves << 35) | (urMoves << 42);
    urMoves &= NE_BOARD[from];
    urMoves ^= NE_BOARD[from];
    urMoves &= target;

    uint64_t ulMoves = NW_BOARD[from] & pieces;
    ulMoves = (ulMoves << 9) | (ulMoves << 18) | (ulMoves << 27) | (ulMoves << 36) | (ulMoves << 45) | (ulMoves << 54);
    ulMoves &= NW_BOARD[from];
    ulMoves ^= NW_BOARD[from];
    ulMoves &= target;

    uint64_t drMoves = SE_BOARD[from] & pieces;
    drMoves = (drMoves >> 9) | (drMoves >> 18) | (drMoves >> 27) | (drMoves >> 36) | (drMoves >> 45) | (drMoves >> 54);
    drMoves &= SE_BOARD[from];
    drMoves ^= SE_BOARD[from];
    drMoves &= target;

    uint64_t dlMoves = SW_BOARD[from] & pieces;
    dlMoves = (dlMoves >> 7) | (dlMoves >> 14) | (dlMoves >> 21) | (dlMoves >> 28) | (dlMoves >> 35) | (dlMoves >> 42);
    dlMoves &= SW_BOARD[from];
    dlMoves ^= SW_BOARD[from];
    dlMoves &= target;

    return urMoves | ulMoves | drMoves | dlMoves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Bishop Move Generation">

std::vector<int> Board::generateBishopMoves() const {

    std::vector<int> moves;
    uint64_t bb, target, king;
    if (side == WHITE) {
        bb = WHITE_BISHOP_BOARD;
        king = BLACK_KING_BOARD;
        target = ~WHITE_PIECES_BOARD;
    } else {
        bb = BLACK_BISHOP_BOARD;
        king = WHITE_KING_BOARD;
        target = ~BLACK_PIECES_BOARD;
    }

    while (bb > 0) {
        int k = bitboard::LS1B(bb);
        int idx = 63 - k; //using 63 since I indexed a8 as 0
        uint64_t tMoves = getBishopMoves(idx, target);
        bool discoveredCheck = false;
        if (king & MASK_RANKS[idx]) {
            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), 0, king, MASK_RANKS[idx]);
        } else if (king & MASK_FILES[idx]) {
            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), 0, king, MASK_FILES[idx]);
        }

        while (tMoves > 0) {
            //get moves from bitboard

            int dest = 63 - bitboard::LS1B(tMoves);
            //TODO 

            bool directCheck = ((getBishopMoves(dest, target) & king) > 0);
            int move = move::construct(idx, dest, side * BISHOP, board[dest], MOVE_NORMAL,
                    directCheck, discoveredCheck);
            moves.push_back(move);
            tMoves &= tMoves - 1;
        }
        bb &= bb - 1;
    }
    return moves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Rook Move Generation Helper">

uint64_t Board::getRookMoves(int from, uint64_t target, uint64_t pieces) const {

    uint64_t rightMoves = RIGHT_BOARD[from] & pieces;
    rightMoves = (rightMoves >> 1) | (rightMoves >> 2) | (rightMoves >> 3) | (rightMoves >> 4) | (rightMoves >> 5) | (rightMoves >> 6);
    rightMoves &= RIGHT_BOARD[from];
    rightMoves ^= RIGHT_BOARD[from];
    rightMoves &= target;

    uint64_t leftMoves = LEFT_BOARD[from] & pieces;
    leftMoves = (leftMoves << 1) | (leftMoves << 2) | (leftMoves << 3) | (leftMoves << 4) | (leftMoves << 5) | (leftMoves << 6);
    leftMoves &= LEFT_BOARD[from];
    leftMoves ^= LEFT_BOARD[from];
    leftMoves &= target;


    uint64_t upMoves = UP_BOARD[from] & pieces;
    upMoves = (upMoves << 8) | (upMoves << 16) | (upMoves << 24) | (upMoves << 32) | (upMoves << 40) | (upMoves << 48);
    upMoves &= UP_BOARD[from];
    upMoves ^= UP_BOARD[from];
    upMoves &= target;

    uint64_t downMoves = DOWN_BOARD[from] & pieces;
    downMoves = (downMoves >> 8) | (downMoves >> 16) | (downMoves >> 24) | (downMoves >> 32) | (downMoves >> 40) | (downMoves >> 48);
    downMoves &= DOWN_BOARD[from];
    downMoves ^= DOWN_BOARD[from];
    downMoves &= target;

    //std::cout << bitboard::show(downMoves) << "\n" << "\n";

    return rightMoves | leftMoves | upMoves | downMoves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Rook Move Generation">

std::vector<int> Board::generateRookMoves() const {

    std::vector<int> moves;
    uint64_t bb, target, king;
    if (side == WHITE) {
        bb = WHITE_ROOK_BOARD;
        king = BLACK_KING_BOARD;
        target = ~WHITE_PIECES_BOARD;
    } else {
        bb = BLACK_ROOK_BOARD;
        king = WHITE_KING_BOARD;
        target = ~BLACK_PIECES_BOARD;
    }

    while (bb > 0) {
        int k = bitboard::LS1B(bb);
        int idx = 63 - k; //using 63 since I indexed a8 as 0

        uint64_t tMoves = getRookMoves(idx, target);

        bool discoveredCheck = false;
        if (king & MASK_A1_H8_DIAG[idx]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), 0, king, MASK_A1_H8_DIAG[idx]);
        } else if (king & MASK_A8_H1_DIAG[idx]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), 0, king, MASK_A8_H1_DIAG[idx]);
        }

        while (tMoves > 0) {
            //get moves from bitboard

            int dest = 63 - bitboard::LS1B(tMoves);

            bool directCheck = ((getRookMoves(dest, target) & king) > 0);

            int move = move::construct(idx, dest, side * ROOK, board[dest], MOVE_NORMAL,
                    directCheck, discoveredCheck);
            moves.push_back(move);
            tMoves &= tMoves - 1;
        }

        bb &= bb - 1;
    }
    return moves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="En Passant Move Generation">

std::vector<int> Board::generateEnPassantMoves() const {

    std::vector<int> moves;
    if (enPassant == -1) {
        return moves;
    }
    bool directCheck;

    uint64_t tMoves, king;
    if (side == WHITE) {
        king = BLACK_KING_BOARD;
        directCheck = (WHITE_PAWN_ATTACKS[enPassant] & BLACK_KING_BOARD) > 0;
        int shift = 63 - enPassant - 9;
        uint64_t mask = CLEAR_RANK_6 & (5ull << shift) & CLEAR_RANK_4;
        tMoves = mask & WHITE_PAWN_BOARD;
    } else {
        king = WHITE_KING_BOARD;
        directCheck = (BLACK_PAWN_ATTACKS[enPassant] & WHITE_KING_BOARD) > 0;
        int shift = 63 - enPassant + 7;
        uint64_t mask = CLEAR_RANK_3 & (5ull << shift) & CLEAR_RANK_5;
        tMoves = mask & BLACK_PAWN_BOARD;
    }
    while (tMoves > 0) {
        int k = bitboard::LS1B(tMoves);
        int m = 63 - enPassant;
        int from = 63 - k;
        bool discoveredCheck = false; //todo
        if (king & MASK_A1_H8_DIAG[from]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_A1_H8_DIAG[from]);
        } else if (king & MASK_A8_H1_DIAG[from]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_A8_H1_DIAG[from]);
        } else if (king & MASK_RANKS[from]) {
            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_RANKS[from]);
        } else if (king & MASK_FILES[from]) {

            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_FILES[from]);
        }



        int move = move::construct(from, enPassant, side * PAWN, EMPTY_SQUARE,
                MOVE_CAPTURE_EN_PASSANT, directCheck, discoveredCheck);
        moves.push_back(move);
        tMoves &= tMoves - 1;
    }
    return moves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Pawn Move Generation ">

std::vector<int> Board::generatePawnMoves()const {

    std::vector<int> moves;
    uint64_t bb, target, king;

    const uint64_t* movement;
    const uint64_t* attack;

    if (side == WHITE) {
        king = BLACK_KING_BOARD;
        bb = WHITE_PAWN_BOARD;
        movement = WHITE_PAWN_MOVES;
        attack = WHITE_PAWN_ATTACKS;
        target = BLACK_PIECES_BOARD;
    } else {
        king = WHITE_KING_BOARD;
        bb = BLACK_PAWN_BOARD;
        movement = BLACK_PAWN_MOVES;
        attack = BLACK_PAWN_ATTACKS;
        target = WHITE_PIECES_BOARD;
    }

    while (bb > 0) {
        int k = bitboard::LS1B(bb);
        int idx = 63 - k; //using 63 since I indexed a8 as 0
        uint64_t tMoves = EMPTY_BOARD & movement[idx];
        uint64_t tAttacks = target & attack[idx];

        tMoves |= tAttacks;

        while (tMoves > 0) {
            //get moves from bitboard

            int m = bitboard::LS1B(tMoves);
            int dest = 63 - m;

            int capture = board[dest];
            if (dest < 8 || dest > 55) { //promotion                
                bool directCheck = false;
                //TODO: discovered check
                bool discoveredCheck = false; //what to do about check bit? especially for discovered check
                int move = move::construct(idx, dest, side * PAWN, capture, MOVE_PROMOTION_QUEEN,
                        directCheck, discoveredCheck);
                moves.push_back(move);
                move = move::construct(idx, dest, side * PAWN, capture, MOVE_PROMOTION_ROOK,
                        directCheck, discoveredCheck);
                moves.push_back(move);
                move = move::construct(idx, dest, side * PAWN, capture, MOVE_PROMOTION_BISHOP,
                        directCheck, discoveredCheck);
                moves.push_back(move);
                move = move::construct(idx, dest, side * PAWN, capture, MOVE_PROMOTION_KNIGHT,
                        directCheck, discoveredCheck);
                moves.push_back(move);
            } else {

                bool directCheck = (attack[dest] & king) > 0;

                bool discoveredCheck = false;

                if (king & MASK_A1_H8_DIAG[idx]) {
                    discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_A1_H8_DIAG[idx]);
                } else if (king & MASK_A8_H1_DIAG[idx]) {
                    discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_A8_H1_DIAG[idx]);
                } else if (king & MASK_RANKS[idx]) {
                    discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_RANKS[idx]);
                } else if (king & MASK_FILES[idx]) {
                    discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_FILES[idx]);
                }

                bool flag = true;
                if (abs(dest - idx) == 16) {
                    //if it's a 2 move push, check that there's nothing directly in front of it
                    flag = ((board[(dest + idx) / 2] == EMPTY_SQUARE));
                }

                if (flag) {

                    int move = move::construct(idx, dest, side * PAWN, capture, MOVE_NORMAL,
                            directCheck, discoveredCheck);
                    moves.push_back(move);
                }
            }
            tMoves &= tMoves - 1;
        }
        bb &= bb - 1; //remove lowest order bit
    }
    return moves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="King Move Generation">

std::vector<int> Board::generateKingMoves() const {

    std::vector<int> moves;
    uint64_t bb, target, king;

    if (side == WHITE) {
        king = BLACK_KING_BOARD;
        bb = WHITE_KING_BOARD;
        target = ~WHITE_PIECES_BOARD;
    } else {
        bb = BLACK_KING_BOARD;
        king = WHITE_KING_BOARD;
        target = ~BLACK_PIECES_BOARD;
    }
    int k = bitboard::LS1B(bb);
    int idx = 63 - k; //using 63 since I indexed a8 as 0
    uint64_t tMoves = target & OCCUPANCY_MASK_KING[idx];

    while (tMoves > 0) {
        //get moves from bitboard
        int m = bitboard::LS1B(tMoves);
        int dest = 63 - m;
        //TODO: discovered check
        bool discoveredCheck = false; //what to do about check bit? especially for discovered check

        if (king & MASK_A1_H8_DIAG[idx]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_A1_H8_DIAG[idx]);
        } else if (king & MASK_A8_H1_DIAG[idx]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_A8_H1_DIAG[idx]);
        } else if (king & MASK_RANKS[idx]) {
            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_RANKS[idx]);
        } else if (king & MASK_FILES[idx]) {

            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), bitboard::getBitBoard(m), king, MASK_FILES[idx]);
        }


        int move = move::construct(idx, dest, side * KING, board[dest], MOVE_NORMAL, false,
                discoveredCheck);
        moves.push_back(move);
        tMoves &= tMoves - 1;
    }

    return moves;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Knight Move Generation">

std::vector<int> Board::generateKnightMoves() const {

    std::vector<int> moves;
    uint64_t bb, target, king;


    if (side == WHITE) {
        king = BLACK_KING_BOARD;
        bb = WHITE_KNIGHT_BOARD;
        target = ~WHITE_PIECES_BOARD;
    } else {
        king = WHITE_KING_BOARD;
        bb = BLACK_KNIGHT_BOARD;
        target = ~BLACK_PIECES_BOARD;
    }

    while (bb > 0) {
        int k = bitboard::LS1B(bb);
        int idx = 63 - k; //using 63 since I indexed a8 as 0
        uint64_t tMoves = target & OCCUPANCY_MASK_KNIGHT[idx];
        bool discoveredCheck = false;
        if (king & MASK_A1_H8_DIAG[idx]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), 0, king, MASK_A1_H8_DIAG[idx]);
        } else if (king & MASK_A8_H1_DIAG[idx]) {
            discoveredCheck = isDiscoveredCheckOnDiagonal(bitboard::getBitBoard(k), 0, king, MASK_A8_H1_DIAG[idx]);
        } else if (king & MASK_RANKS[idx]) {
            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), 0, king, MASK_RANKS[idx]);
        } else if (king & MASK_FILES[idx]) {
            discoveredCheck = isDiscoveredCheckOnHorizontal(bitboard::getBitBoard(k), 0, king, MASK_FILES[idx]);
        }
        while (tMoves > 0) {
            //get moves from bitboard

            int dest = 63 - bitboard::LS1B(tMoves);
            bool directCheck = (OCCUPANCY_MASK_KNIGHT[dest] & king) > 0;
            int move = move::construct(idx, dest, side * KNIGHT, board[dest], MOVE_NORMAL,
                    directCheck, discoveredCheck);
            moves.push_back(move);
            tMoves &= tMoves - 1;
        }
        bb &= bb - 1; //remove lowest order bit
    }
    return moves;
}
// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="Discovered Checks">

bool Board::isDiscoveredCheckOnHorizontal(uint64_t source, uint64_t destination, uint64_t king, uint64_t line) const {
    uint64_t attack, target;
    if (side == WHITE) {
        attack = WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD;
        target = ~WHITE_PIECES_BOARD | source | (~destination);
    } else {
        attack = BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD;
        target = ~BLACK_PIECES_BOARD | source | (~destination);
    }
    target &= line;
    attack &= line;
    if (attack == 0) {
        return false;
    }

    bool capture = (destination & ALL_PIECES_BOARD) > 0;

    while (attack > 0) {
        int from = 63 - bitboard::LS1B(attack); //using 63 since I indexed a8 as 0

        uint64_t temp = ALL_PIECES_BOARD;

        temp &= ~source;
        if (!capture)
            temp |= destination;
        uint64_t moves = getRookMoves(from, target, temp);
        if (moves & king) {

            return true;
        }
        attack &= attack - 1;
    }
    return false;
}

bool Board::isDiscoveredCheckOnDiagonal(uint64_t source, uint64_t destination, uint64_t king, uint64_t diagonal) const {
    uint64_t attack, target;
    if (side == WHITE) {
        attack = WHITE_BISHOP_BOARD | WHITE_QUEEN_BOARD;
        target = ~WHITE_PIECES_BOARD | source | (~destination);
    } else {
        attack = BLACK_BISHOP_BOARD | BLACK_QUEEN_BOARD;
        target = ~BLACK_PIECES_BOARD | source | (~destination);
    }
    target &= diagonal;
    attack &= diagonal;
    if (attack == 0) {
        return false;
    }

    bool capture = (destination & ALL_PIECES_BOARD) > 0;
    while (attack > 0) {
        int from = 63 - bitboard::LS1B(attack); //using 63 since I indexed a8 as 0
        uint64_t temp = ALL_PIECES_BOARD;
        temp &= ~source;
        if (!capture)
            temp |= destination;
        uint64_t moves = getBishopMoves(from, target, temp);
        if (moves & king) {

            return true;
        }
        attack &= attack - 1;
    }
    return false;
}
// </editor-fold>

int Board::evaluate() {


    if (fiftyRule >= 100) {
        return -DRAW_VALUE;
    }

    int eval = 0;

    // <editor-fold defaultstate="collapsed" desc="Material Evaluation">
    int P = bitboard::getDenseBitCount(WHITE_PAWN_BOARD); //??? sparse or dense
    int N = bitboard::getSparseBitCount(WHITE_KNIGHT_BOARD);
    int B = bitboard::getSparseBitCount(WHITE_BISHOP_BOARD);
    int R = bitboard::getSparseBitCount(WHITE_ROOK_BOARD);
    int Q = bitboard::getSparseBitCount(WHITE_QUEEN_BOARD);

    int p = bitboard::getDenseBitCount(BLACK_PAWN_BOARD); // ??? sparse or dense
    int n = bitboard::getSparseBitCount(BLACK_KNIGHT_BOARD);
    int b = bitboard::getSparseBitCount(BLACK_BISHOP_BOARD);
    int r = bitboard::getSparseBitCount(BLACK_ROOK_BOARD);
    int q = bitboard::getSparseBitCount(BLACK_QUEEN_BOARD);


    if (P + p + R + r + Q + q == 0) {
        if (((N + B) <= 1) && ((n + b) <= 1)) {
            return -DRAW_VALUE;
        }
    }

    int whiteMaterial = queenValue * (Q) + rookValue * (R) + bishopValue * (B) +
            knightValue * (N) + pawnValue * (P);

    int blackMaterial = queenValue * (q) + rookValue * (r) + bishopValue * (b) +
            knightValue * (n) + pawnValue * (p);

    eval = side * (whiteMaterial - blackMaterial);
    // </editor-fold>

    eval += evaluatePawnStructure();
    eval += evaluateMobilityandKingSafety(whiteMaterial, blackMaterial);

    return eval;
}

// <editor-fold defaultstate="collapsed" desc="Evaluate Mobility + King Safety">

int Board::evaluateMobilityandKingSafety(int wMaterial, int bMaterial) {

    uint64_t whiteKingArea = OCCUPANCY_MASK_KING[whiteKing];
    uint64_t blackKingArea = OCCUPANCY_MASK_KING[blackKing];

    int wAttackers = 0;
    int bAttackers = 0;
    int wAttackTot = 0;
    int bAttackTot = 0;
    int count;
    double temp;
    uint64_t bb;
    int whiteMobility = 0;
    int blackMobility = 0;
    bb = WHITE_KNIGHT_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        temp += (knightMobility * bitboard::getSparseBitCount(OCCUPANCY_MASK_KNIGHT[idx]));
        if (OCCUPANCY_MASK_KNIGHT[idx] & blackKingArea) {
            wAttackTot += knightAttackWeight;
            wAttackers++;
        }
        bb &= bb - 1;
    }
    whiteMobility += (count > 0) ? int(temp / count) : 0;

    bb = BLACK_KNIGHT_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        temp += (knightMobility * bitboard::getSparseBitCount(OCCUPANCY_MASK_KNIGHT[idx]));
        if (OCCUPANCY_MASK_KNIGHT[idx] & whiteKingArea) {
            bAttackTot += knightAttackWeight;
            bAttackers++;
        }
        bb &= bb - 1;
    }
    blackMobility += (count > 0) ? int(temp / count) : 0;

    bb = WHITE_PAWN_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        temp += (pawnMobility * bitboard::getDenseBitCount(
                (EMPTY_BOARD & WHITE_PAWN_MOVES[idx]) | (ALL_PIECES_BOARD & WHITE_PAWN_ATTACKS[idx])
                ));
        bb &= bb - 1;
    }
    whiteMobility += (count > 0) ? int(temp / count) : 0;

    bb = BLACK_PAWN_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        temp += (pawnMobility * bitboard::getDenseBitCount(
                (EMPTY_BOARD & BLACK_PAWN_MOVES[idx]) | (ALL_PIECES_BOARD & BLACK_PAWN_ATTACKS[idx])
                ));
        bb &= bb - 1;
    }
    blackMobility += (count > 0) ? int(temp / count) : 0;


    bb = WHITE_ROOK_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        uint64_t att = getRookMoves(idx, (~WHITE_PAWN_BOARD) | (~WHITE_KING_BOARD));
        //uint64_t att = getRookMoves(idx, FULL_BOARD);
        temp += (rookMobility * bitboard::getDenseBitCount(att));
        if (att & blackKingArea) {
            wAttackTot += rookAttackWeight;
            wAttackers++;
        }

        bb &= bb - 1;
    }
    whiteMobility += (count > 0) ? int(temp / count) : 0;

    bb = BLACK_ROOK_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        uint64_t att = getRookMoves(idx, (~BLACK_PAWN_BOARD) | (~BLACK_KING_BOARD));
        //uint64_t att = getRookMoves(idx, FULL_BOARD);
        temp += (rookMobility * bitboard::getDenseBitCount(att));
        if (att & whiteKingArea) {
            bAttackTot += rookAttackWeight;
            bAttackers++;
        }
        bb &= bb - 1;
    }
    blackMobility += (count > 0) ? int(temp / count) : 0;

    bb = WHITE_BISHOP_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        uint64_t att = getBishopMoves(idx, ~WHITE_PAWN_BOARD);
        //  uint64_t att = getBishopMoves(idx, FULL_BOARD);
        temp += (bishopMobility * bitboard::getDenseBitCount(att));
        if (att & blackKingArea) {
            wAttackTot += bishopAttackWeight;
            wAttackers++;
        }
        bb &= bb - 1;
    }
    whiteMobility += (count > 0) ? int(temp / count) : 0;

    bb = BLACK_BISHOP_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        uint64_t att = getBishopMoves(idx, ~BLACK_PAWN_BOARD);
        //uint64_t att = getBishopMoves(idx, FULL_BOARD);
        temp += (bishopMobility * bitboard::getDenseBitCount(att));
        if (att & whiteKingArea) {
            bAttackTot += bishopAttackWeight;
            bAttackers++;
        }
        bb &= bb - 1;
    }
    blackMobility += (count > 0) ? int(temp / count) : 0;

    bb = WHITE_QUEEN_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);

        uint64_t att = getBishopMoves(idx, ~WHITE_PAWN_BOARD) | getRookMoves(idx, ~WHITE_PAWN_BOARD);
        // uint64_t att = getBishopMoves(idx, FULL_BOARD) | getRookMoves(idx, FULL_BOARD);
        temp += (queenMobility * bitboard::getDenseBitCount(att));
        if (att & blackKingArea) {
            wAttackTot += queenAttackWeight;
            wAttackers++;
        }
        bb &= bb - 1;
    }
    whiteMobility += (count > 0) ? int(temp / count) : 0;

    bb = BLACK_QUEEN_BOARD;
    temp = 0.0f;
    count = 0;
    while (bb > 0) {
        count++;
        int idx = 63 - bitboard::LS1B(bb);
        uint64_t att = getBishopMoves(idx, ~BLACK_PAWN_BOARD) | getRookMoves(idx, ~BLACK_PAWN_BOARD);
        // uint64_t att = getBishopMoves(idx, FULL_BOARD) | getRookMoves(idx, FULL_BOARD);
        temp += (queenMobility * bitboard::getDenseBitCount(att));
        if (att & whiteKingArea) {
            bAttackTot += queenAttackWeight;
            bAttackers++;
        }
        bb &= bb - 1;
    }
    blackMobility += (count > 0) ? int(temp / count) : 0;


    bool wFlag = (BLACK_QUEEN_BOARD > 0) && ((BLACK_BISHOP_BOARD > 0) || (BLACK_ROOK_BOARD > 0) ||
            (BLACK_KNIGHT_BOARD > 0));
    bool bFlag = (WHITE_QUEEN_BOARD > 0) && ((WHITE_BISHOP_BOARD > 0) || (WHITE_ROOK_BOARD > 0) ||
            (WHITE_KNIGHT_BOARD > 0));
    int whiteKingSafety = 0;
    int blackKingSafety = 0;


    if (!wFlag) {
        whiteKingSafety = (int) (kingMobility * bitboard::getSparseBitCount(OCCUPANCY_MASK_KING[whiteKing]));
    } else {
        whiteKingSafety = whiteKingShield();
        whiteKingSafety -= scale(bAttackTot, bAttackers);

        /* Scale the middlegame king evaluation against remaining enemy material */
        whiteKingSafety *= bMaterial;
        whiteKingSafety /= startMaterial;
    }

    if (!bFlag) {
        blackKingSafety += (int) (kingMobility * bitboard::getSparseBitCount(OCCUPANCY_MASK_KING[blackKing]));
    } else {

        blackKingSafety = blackKingShield();
        blackKingSafety -= scale(wAttackTot, wAttackers);

        /* Scale the middlegame king evaluation against remaining enemy material */
        blackKingSafety *= wMaterial;
        blackKingSafety /= startMaterial;

    }
    return side * ((whiteKingSafety - blackKingSafety)+(whiteMobility - blackMobility));
}
// </editor-fold>

int Board::scale(int attack_value, int attackers) {
    switch (attackers) {
        case 0: return 0;
        case 1: return attack_value / 2;
        case 2: return attack_value;
        case 3: return (attack_value * 4) / 3;
        case 4: return (attack_value * 3) / 2;

        default: return attack_value * 2;
    }
}
// <editor-fold defaultstate="collapsed" desc="Black Pawn Shield">

int Board::blackKingShield() {

    int result = 0;
    uint64_t mask = ~MASK_RANKS[blackKing]; // nothing on king's rank

    //If king is kingside
    if (BLACK_KING_BOARD & (KINGSIDE_MASK & CLEAR_FILE_F)) {

        mask &= KINGSIDE_MASK;

        uint64_t perfect = (BPS_PERFECT >> (blackKing)) & mask;
        uint64_t good = (BPS_GOOD_KINGSIDE >> (blackKing)) & mask;
        uint64_t ok = (BPS_OK >> (blackKing)) & mask;
        uint64_t poor = (BPS_GOOD_QUEENSIDE >> (blackKing)) & mask;

        if (perfect == (perfect & BLACK_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & BLACK_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & BLACK_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & BLACK_PAWN_BOARD)) {
            return poorPawnShieldPenalty; //poor pawn shield
        }
        result += pawnShieldPenalty1;
        int closePawns = bitboard::getSparseBitCount(BLACK_PAWN_BOARD & OCCUPANCY_MASK_KING[blackKing]);
        result += std::min(0, (2 - closePawns) * pawnShieldPenalty2);
        int times = 0;
        if ((MASK_FILES[blackKing] & BLACK_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty3;
            times++;
        }
        if ((blackKing + 1) % 8 != 0) {
            if ((MASK_FILES[blackKing + 1] & BLACK_PAWN_BOARD) == 0) {
                result += pawnShieldPenalty4;
                result += pawnShieldPenalty4 * times;
            }
        }
        if ((MASK_FILES[blackKing - 1] & BLACK_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty5;
            result += pawnShieldPenalty5 * times;
            times++;
        }


    }//if king is queenside
    else if (BLACK_KING_BOARD & QUEENSIDE_MASK) {

        mask &= QUEENSIDE_MASK;
        int idx = blackKing;
        if (BLACK_KING_BOARD & MASK_FILE_C) {
            idx--;
        }
        uint64_t perfect = (BPS_PERFECT >> (idx)) & mask;
        uint64_t good = (BPS_GOOD_QUEENSIDE >> (idx)) & mask;
        uint64_t ok = (BPS_OK >> (idx)) & mask;
        uint64_t poor = (BPS_GOOD_KINGSIDE >> (idx)) & mask;

        if (perfect == (perfect & BLACK_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & BLACK_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & BLACK_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & BLACK_PAWN_BOARD)) {
            return poorPawnShieldPenalty; //poor pawn shield
        }
        result += pawnShieldPenalty1;
        int closePawns = bitboard::getSparseBitCount(BLACK_PAWN_BOARD & (OCCUPANCY_MASK_KING[blackKing] | OCCUPANCY_MASK_KING[idx]));
        result += std::min(0, (2 - closePawns) * pawnShieldPenalty2);
        int times = 0;
        if ((MASK_FILES[blackKing] & BLACK_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty3;
            times++;
        }
        if ((blackKing) % 8 != 0) {
            if ((MASK_FILES[blackKing - 1] & BLACK_PAWN_BOARD) == 0) {
                result += pawnShieldPenalty4;
                result += pawnShieldPenalty4 * times;
            }
        }
        if ((MASK_FILES[blackKing + 1] & BLACK_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty5;
            result += pawnShieldPenalty5 * times;
            times++;
        }


    }//king in center of board
    else {
        result += centralKingPenalty;

        uint64_t perfect = (BPS_PERFECT >> (blackKing)) & mask;
        uint64_t poor1 = (BPS_GOOD_KINGSIDE >> (blackKing)) & mask;
        uint64_t ok = (BPS_OK >> (blackKing)) & mask;
        uint64_t poor2 = (BPS_GOOD_QUEENSIDE >> (blackKing)) & mask;

        if (perfect == (perfect & BLACK_PAWN_BOARD)) {
            return result;
        }
        if (poor1 == (poor1 & BLACK_PAWN_BOARD)) {
            result += pawnShieldPenalty6;
            return result;
        }
        if (ok == (ok & BLACK_PAWN_BOARD)) {
            return result;
        }

        if (poor2 == (poor2 & BLACK_PAWN_BOARD)) {
            result += pawnShieldPenalty6;
            return result;
        }
        int closePawns = bitboard::getSparseBitCount(BLACK_PAWN_BOARD & OCCUPANCY_MASK_KING[blackKing]);
        result += std::min(0, (1 - closePawns) * pawnShieldPenalty7);
        int times = 0;
        if ((MASK_FILES[blackKing] & BLACK_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty8;
            times++;
        }
        if ((MASK_FILES[blackKing - 1] & BLACK_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty9;
            result += pawnShieldPenalty9 * times;
            times++;
        }
        if ((MASK_FILES[blackKing + 1] & BLACK_PAWN_BOARD) == 0) {

            result += pawnShieldPenalty9;
            result += pawnShieldPenalty9 * times;
        }
    }
    return result;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="White Pawn Shield">

int Board::whiteKingShield() {

    int result = 0;

    uint64_t mask = ~MASK_RANKS[whiteKing]; // nothing on king's rank

    //If king is kingside
    if (WHITE_KING_BOARD & (KINGSIDE_MASK & CLEAR_FILE_F)) {
        mask &= KINGSIDE_MASK;

        uint64_t perfect = (WPS_PERFECT << (63 - whiteKing)) & mask;
        uint64_t good = (WPS_GOOD_KINGSIDE << (63 - whiteKing)) & mask;
        uint64_t ok = (WPS_OK << (63 - whiteKing)) & mask;
        uint64_t poor = (WPS_GOOD_QUEENSIDE << (63 - whiteKing)) & mask;

        if (perfect == (perfect & WHITE_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & WHITE_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & WHITE_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & WHITE_PAWN_BOARD)) {
            return poorPawnShieldPenalty; //poor pawn shield
        }
        result += pawnShieldPenalty1;
        int closePawns = bitboard::getSparseBitCount(WHITE_PAWN_BOARD & OCCUPANCY_MASK_KING[whiteKing]);
        result += std::min(0, (2 - closePawns) * pawnShieldPenalty2);
        int times = 0;
        if ((MASK_FILES[whiteKing] & WHITE_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty3;
            times++;
        }
        if ((whiteKing + 1) % 8 != 0) {
            if ((MASK_FILES[whiteKing + 1] & WHITE_PAWN_BOARD) == 0) {
                result += pawnShieldPenalty4;
                result += pawnShieldPenalty4 * times;
            }
        }
        if ((MASK_FILES[whiteKing - 1] & WHITE_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty5;
            result += pawnShieldPenalty5 * times;
            times++;
        }

    }//if king is queenside
    else if (WHITE_KING_BOARD & QUEENSIDE_MASK) {

        mask &= QUEENSIDE_MASK;
        int idx = whiteKing;
        if (WHITE_KING_BOARD & MASK_FILE_C) {
            idx--;
        }
        uint64_t perfect = (WPS_PERFECT << (63 - idx)) & mask;
        uint64_t good = (WPS_GOOD_QUEENSIDE << (63 - idx)) & mask;
        uint64_t ok = (WPS_OK << (63 - idx)) & mask;
        uint64_t poor = (WPS_GOOD_KINGSIDE << (63 - idx)) & mask;

        if (perfect == (perfect & WHITE_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & WHITE_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & WHITE_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & WHITE_PAWN_BOARD)) {
            return poorPawnShieldPenalty; //poor pawn shield
        }
        result += pawnShieldPenalty1;
        int closePawns = bitboard::getSparseBitCount(WHITE_PAWN_BOARD & (OCCUPANCY_MASK_KING[whiteKing] | OCCUPANCY_MASK_KING[idx]));
        result += std::min(0, (2 - closePawns) * pawnShieldPenalty2);
        int times = 0;
        if ((MASK_FILES[whiteKing] & WHITE_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty3;
            times++;
        }
        if ((whiteKing) % 8 != 0) {
            if ((MASK_FILES[whiteKing - 1] & WHITE_PAWN_BOARD) == 0) {
                result += pawnShieldPenalty4;
                result += pawnShieldPenalty4 * times;
                times++;
            }
        }

        if ((MASK_FILES[whiteKing + 1] & WHITE_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty5;
            result += pawnShieldPenalty5 * times;
            times++;
        }



    }//king in center of board
    else {
        result += centralKingPenalty;

        uint64_t perfect = (WPS_PERFECT << (63 - whiteKing)) & mask;
        uint64_t poor1 = (WPS_GOOD_KINGSIDE << (63 - whiteKing)) & mask;
        uint64_t ok = (WPS_OK << (63 - whiteKing)) & mask;
        uint64_t poor2 = (WPS_GOOD_QUEENSIDE << (63 - whiteKing)) & mask;

        if (perfect == (perfect & WHITE_PAWN_BOARD)) {
            return result;
        }
        if (poor1 == (poor1 & WHITE_PAWN_BOARD)) {
            result += pawnShieldPenalty6;
            return result;
        }
        if (ok == (ok & WHITE_PAWN_BOARD)) {
            return result;
        }

        if (poor2 == (poor2 & WHITE_PAWN_BOARD)) {
            result += pawnShieldPenalty6;
            return result;
        }
        int closePawns = bitboard::getSparseBitCount(WHITE_PAWN_BOARD & OCCUPANCY_MASK_KING[whiteKing]);
        result += std::min(0, (1 - closePawns) * pawnShieldPenalty7);
        int times = 0;
        if ((MASK_FILES[whiteKing] & WHITE_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty8;
            times++;
        }
        if ((MASK_FILES[whiteKing - 1] & WHITE_PAWN_BOARD) == 0) {
            result += pawnShieldPenalty9;
            result += pawnShieldPenalty9 * times;
            times++;
        }
        if ((MASK_FILES[whiteKing + 1] & WHITE_PAWN_BOARD) == 0) {

            result += pawnShieldPenalty9;
            result += pawnShieldPenalty9 * times;
        }
    }
    return result;
}
// </editor-fold>

uint64_t Board::getOpeningHash() const {
    uint64_t temp = getLock();
    //TODO uncomment this
    if (enPassant != -1) {

        return temp ^ (enPassantHash[enPassant][0]);
    }
    return temp;
}

// <editor-fold defaultstate="collapsed" desc="Evaluate Pawn Structure">

int Board::evaluatePawnStructure() {

    int A = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_A) - 1);
    int a = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_A) - 1);
    int B = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_B) - 1);
    int b = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_B) - 1);
    int C = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_C) - 1);
    int c = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_C) - 1);
    int D = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_D) - 1);
    int d = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_D) - 1);
    int E = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_E) - 1);
    int e = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_E) - 1);
    int F = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_F) - 1);
    int f = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_F) - 1);
    int G = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_G) - 1);
    int g = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_G) - 1);
    int H = std::max(0, bitboard::getSparseBitCount(WHITE_PAWN_BOARD & MASK_FILE_H) - 1);
    int h = std::max(0, bitboard::getSparseBitCount(BLACK_PAWN_BOARD & MASK_FILE_H) - 1);

    int doubled = doubledPawnPenalty * ((A - a) + (B - b) + (C - c) + (D - d) + (E - e) + (F - f) + (G - g) + (H - h));

    int whitePassed = 0;
    int blackPassed = 0;

    int whiteIsolated = 0;
    int blackIsolated = 0;

    //TODO: must do pawn advancement
    int whiteAdvanceBonus = 0;
    int blackAdvanceBonus = 0;

    uint64_t bb = WHITE_PAWN_BOARD;
    while (bb > 0) {
        bool bonusGranted = false;
        int idx = 63 - bitboard::LS1B(bb);
        if ((FILE_NEIGHBOUR[idx] & WHITE_PAWN_BOARD) == 0) {
            whiteIsolated++;
            if (idx < 24) { //if on 6th or 7th rank
                bonusGranted = true;
                int rate = 3 - idx / 8; //2 for rank 7, 1 for rank 6
                whiteAdvanceBonus += rate * isolatedAdvancedPawnWeight * pawnValue;
            }

        }

        if ((WHITE_FRONT_SPANS[idx] & BLACK_PAWN_BOARD) == 0) {
            whitePassed++;
            if (idx < 24 && !bonusGranted) { //if on 6th or 7th rank
                int rate = 3 - idx / 8; //2 for rank 7, 1 for rank 6
                whiteAdvanceBonus += rate * supportedAdvancedPawnWeight * pawnValue;
            }
        }

        bb &= bb - 1;
    }
    bb = BLACK_PAWN_BOARD;
    while (bb > 0) {
        bool bonusGranted = false;
        int idx = 63 - bitboard::LS1B(bb);
        if ((FILE_NEIGHBOUR[idx] & BLACK_PAWN_BOARD) == 0) {
            blackIsolated++;
            if (idx >= 40) { //if on 6th or 7th rank
                bonusGranted = true;
                int rate = idx / 8 - 4; //2 for rank 7, 1 for rank 6
                blackAdvanceBonus += rate * isolatedAdvancedPawnWeight * pawnValue;
            }
        }
        if ((BLACK_FRONT_SPANS[idx] & WHITE_PAWN_BOARD) == 0) {
            blackPassed++;
            if (idx >= 40 && !bonusGranted) { //if on 6th or 7th rank
                int rate = idx / 8 - 4; //2 for rank 7, 1 for rank 6
                blackAdvanceBonus += rate * supportedAdvancedPawnWeight * pawnValue;
            }
        }
        bb &= bb - 1;
    }
    int isolated = isolatedPawnPenalty * (whiteIsolated - blackIsolated);
    int passed = passedPawnReward * (whitePassed - blackPassed);
    int bonus = whiteAdvanceBonus - blackAdvanceBonus;

    return side * (doubled + isolated + passed + bonus);
}
// </editor-fold>


/*******************************************************************************/

// <editor-fold defaultstate="collapsed" desc="Bitboard Operations">

void bitboard::out(uint64_t n) {

    std::cout << show(n) << "\n\n";
}

std::string bitboard::show(uint64_t n) {
    const int size = sizeof (n)*8;
    std::string res;
    bool s = 0;
    for (int a = 0; a < size; a++) {
        bool bit = n >> (size - 1);
        if (bit)
            s = 1;
        if (s)
            res.push_back(bit + '0');
        n <<= 1;
    }
    if (!res.size())
        res.push_back('0');
    while (res.size() < size) {
        res = "0" + res;
    }
    std::string ans = "";
    for (std::string::size_type i = 0; i < res.size(); i++) {

        if (i != 0 && i % 8 == 0) {

            ans += "\n";
        }
        ans += res[i];

    }
    return ans;
}

uint64_t bitboard::getBitBoard(int idx) {
    uint64_t t = 1ull;

    return t << idx;
}

int bitboard::LS1B(uint64_t bb) {
    if (bb == 0) {
        return -1;
    }
    const uint64_t debruijn = (uint64_t) 0x03f79d71b4cb0a89ull;
    int temp = ((bb & -bb) * debruijn) >> 58;

    return LS1B_INDEX[temp]; //returns index from right. To reference in board array, use 63-temp
}

int bitboard::getSparseBitCount(uint64_t bb) {
    int count;
    for (count = 0; bb; count++) {

        bb &= bb - 1;
    }
    return count;
}

/**
 * Use this for dense bitboards (see Hamming weight - http://en.wikipedia.org/wiki/Hamming_weight)
 * @param bb the bitboard
 * @return the number of bits set
 */
int bitboard::getDenseBitCount(uint64_t bb) {
    bb -= (bb >> 1) & 0x5555555555555555ull;
    bb = (bb & 0x3333333333333333ull) + ((bb >> 2) & 0x3333333333333333ull);
    bb = (bb + (bb >> 4)) & 0x0f0f0f0f0f0f0f0full;
    return (bb * 0x0101010101010101ull) >> 56;
}
// </editor-fold>
