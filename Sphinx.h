/* 
 * File:   Sphinx.h
 * Author: 475531
 *
 * Created on 11 April 2013, 8:37 AM
 */

#ifndef SPHINX_H
#define	SPHINX_H

#include "KillerHeuristic.h"
#include "TranspositionTable.h"
#include "Line.h"
#include "OpeningBook.h"
#include <vector>
#include "Board.h"
#include <string>
#include <pthread.h>
#include "Move.h"
#include "EndgameTable.h"

typedef struct PRINCIPAL_LINE {
    int cmove; // Number of moves in the line.
    int argmove[50]; // The line.
} PRINCIPAL_LINE;

class Sphinx {
public:

    void setOption(int option, bool val) {
        if (option < NUM_OPTIONS) {
            options[option] = val;
        }
    }

    static const short OPTION_OPENING_BOOK = 0;
    static const short OPTION_TRANSPOSITION_TABLE = 1;
    static const short OPTION_VERBOSE = 2;
    static const short OPTION_ENDGAME_TABLEBASE = 3;
    static const short NUM_OPTIONS = 4;

    void showOpeningBook() {

        if (openingBook == NULL) {
            openingBook = new OpeningBook();
            openingBook->read();
        }

        if (openingBook->isOpen()) {
            openingBook->print(openingBook->getMoves(&board));
        }
    }

    short getSide() {
        return board.getSide();
    }

    void setMaxDepth(int a) {
        MAX_DEPTH = a;
    }

    std::string getName() {
        return name;
    }

    Sphinx(std::string);
    Sphinx();
    virtual ~Sphinx();
    void setPosition(std::string);
    bool playMove(std::string);
    void interrupt();
    bool isInterrupted();
    Line search(long int);
    bool isThinking();

    void reset();
    void setMinDepth(int);
    
    long int calculateTime(long int);

private:
    
    int isSimpleEndgame();

    bool compare(int a, int b);
    void quicksort(int*, int, int);
    int partition(int*, int, int);

    std::string name;

    bool options[3];


    timeval searchStarted;
    long int timeAllowed;
    int MAX_DEPTH;
    int MIN_DEPTH;
    bool thinking;
    bool interrupted;
    Line previousLine;

    KillerHeuristic killerHeuristic;
    long int nodes;
    bool useLastMove;
    int depth;
    int sequenceNum;
    TranspositionTable* table;
    EndgameTable* tableBase;
    OpeningBook* openingBook;
    int alphaBeta(int, int, int, PRINCIPAL_LINE*, bool);
    int alphaBeta(int, int, int, bool);
    int completionCheck(int);
    
    int quiescenceSearch(int, int);
    int* getNoisyMoves();
    void rankMoves(int*, int);
    bool isTimeLeft();
    
protected:
        Board board;
    virtual int evaluatePosition();
};

typedef struct arguments {
    double time;
    Sphinx* obj;
} Arguments;


#endif	/* SPHINX_H */

