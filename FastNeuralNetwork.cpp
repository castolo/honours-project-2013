/* 
 * File:   FastNeuralNetwork.cpp
 * Author: Workstation
 * 
 * Created on 30 August 2013, 6:49 PM
 */

#include "FastNeuralNetwork.h"
#include "Constants.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>

unsigned int FastNeuralNetwork::seed = time(NULL);

FastNeuralNetwork::FastNeuralNetwork() {
    inputs = outputs = hidden = 0;
}

FastNeuralNetwork::~FastNeuralNetwork() {

    delete[] inputLayer;
    delete[] hiddenLayer;
    delete[] outputLayer;

    for (int i = 0; i <= inputs; i++) {
        delete[] wInputHidden[i];
    }
    delete[] wInputHidden;

    for (int j = 0; j <= hidden; j++) {
        delete[] wHiddenOutput[j];
    }
    delete[] wHiddenOutput;

}

FastNeuralNetwork::FastNeuralNetwork(int I, int H, int O) {
    this->inputs = I;
    inputLayer = new double[I + 1];
    for (int i = 0; i < I; i++) {
        inputLayer[i] = 0;
    }
    inputLayer[I] = -1; //bias

    this->outputs = O;
    outputLayer = new double[O];
    for (int i = 0; i < O; i++) {
        outputLayer[i] = 0;
    }

    this->hidden = H;
    hiddenLayer = new double[H + 1];
    for (int i = 0; i < H; i++) {
        hiddenLayer[i] = 0;
    }
    hiddenLayer[H] = -1; //bias

    wInputHidden = new double*[inputs + 1];
    for (int i = 0; i <= inputs; i++) {
        wInputHidden[i] = new double[hidden];
        for (int j = 0; j < hidden; j++) {
            wInputHidden[i][j] = 0;
        }
    }
    wHiddenOutput = new double*[hidden + 1];
    for (int i = 0; i <= hidden; i++) {
        wHiddenOutput[i] = new double[outputs];
        for (int j = 0; j < outputs; j++) {
            wHiddenOutput[i][j] = 0;
        }
    }
    randomizeWeights();
}

void FastNeuralNetwork::randomizeWeights() {
    for (int i = 0; i <= inputs; i++) {
        for (int j = 0; j < hidden; j++) {
            double v = 2 * ((double) random(&seed) / (double) 2147483647) - 1;
            wInputHidden[i][j] = v;
        }
    }
    for (int i = 0; i <= hidden; i++) {
        for (int j = 0; j < outputs; j++) {

            double v = 2 * ((double) random(&seed) / (double) 2147483647) - 1;
            wHiddenOutput[i][j] = v;
        }
    }
}

std::vector<double> FastNeuralNetwork::activate(const Board & board) const {

    const int king[6] = {1, 0, 0, 0, 0, 0};
    const int queen[6] = {0, 1, 0, 0, 0, 0};
    const int rook[6] = {0, 0, 1, 0, 0, 0};
    const int bishop[6] = {0, 0, 0, 1, 0, 0};
    const int knight[6] = {0, 0, 0, 0, 1, 0};
    const int pawn[6] = {0, 0, 0, 0, 0, 1};
    const int empty[6] = {0, 0, 0, 0, 0, 0};
    std::vector<double> inputs, outputs;

    inputs.reserve(this->inputs);

    for (int i = 0; i < 64; i++) {

        int piece = board[i];
        int side;
        if (piece > 0) {
            side = 1;
        } else if (piece < 0) {
            side = -1;
        } else {
            side = 0;
        }

        const int *array;

        switch (abs(piece)) {
            case KING:
                array = king;
                break;
            case QUEEN:
                array = queen;
                break;
            case ROOK:
                array = rook;
                break;
            case BISHOP:
                array = bishop;
                break;
            case KNIGHT:
                array = knight;
                break;
            case PAWN:
                array = pawn;
                break;
            default:
                array = empty;
                break;
        }
        for (int j = 0; j < 6; j++) {
            inputs.push_back(array[j] * side);
        }
    }
    outputs = activate(inputs);
    return outputs;
}

std::vector<double> FastNeuralNetwork::activate(std::vector<double> inputs) const {

    std::vector<double> outputs;
    if (inputs.size() != this->inputs) {
        std::cerr << "Incorrect input dimensions: Should be " << this->inputs << ", but found " << inputs.size() << std::endl;
        outputs.push_back(0);
        return outputs;
    }

    for (int i = 0; i<this->inputs; i++) {
        inputLayer[i] = inputs[i];
    }

    for (int i = 0; i < hidden; i++) {
        hiddenLayer[i] = 0;
        for (int j = 0; j <= this->inputs; j++) {
            hiddenLayer[i] += inputLayer[j] * wInputHidden[j][i];
        }
        hiddenLayer[i] = activationFunction(hiddenLayer[i]);
    }

    for (int i = 0; i < this->outputs; i++) {
        outputLayer[i] = 0;
        for (int j = 0; j <= hidden; j++) {
            outputLayer[i] += hiddenLayer[j] * wHiddenOutput[j][i];
        }
        outputLayer[i] = activationFunction(outputLayer[i]);
        outputs.push_back(outputLayer[i]);
    }
    return outputs;
}

void FastNeuralNetwork::update(const std::vector<double> &weights) {

    if (weights.size() != getNumberOfWeights()) {
        std::cerr << "Error: dimensions do not match. Found " << weights.size() << ", but expected " <<
                getNumberOfWeights() << std::endl;
    } else {
        int idx = 0;
        for (int i = 0; i <= inputs; i++) {
            for (int j = 0; j < hidden; j++) {
                wInputHidden[i][j] = weights[idx++];
            }
        }
        for (int i = 0; i <= hidden; i++) {
            for (int j = 0; j < outputs; j++) {
                wHiddenOutput[i][j] = weights[idx++];
            }
        }
    }
}

int FastNeuralNetwork::getNumberOfWeights() const {
    return (inputs + 1) * hidden + (hidden + 1) * outputs;
}

std::vector<double> FastNeuralNetwork::getWeights() const {
    std::vector<double> weights;
    weights.reserve(getNumberOfWeights());

    for (int i = 0; i <= inputs; i++) {
        for (int j = 0; j < hidden; j++) {
            weights.push_back(wInputHidden[i][j]);
        }
    }

    for (int i = 0; i <= hidden; i++) {
        for (int j = 0; j < outputs; j++) {
            weights.push_back(wInputHidden[i][j]);
        }
    }
    return weights;
}

inline double FastNeuralNetwork::activationFunction(double input) const {
    //Sigmoid
    double out = 2 / (1 + exp(-input)) - 1; //between -1 and 1
    return out;
}

//void NeuralNetwork::show() const {
//
//    using namespace std;
//    cout << "Input layer: " << inputs << " neurons\n\n";
//    for (int i = 0; i < layers.size() - 1; i++) {
//        cout << "Hidden Layer " << i + 1 << ": ";
//        for (int j = 0; j < layers[i].getCount(); j++) {
//            cout << "[";
//            for (int k = 0; k < layers[i].getNeurons()[j].getNumberInputs(); k++) {
//                cout << layers[i].getNeurons()[j].getWeights()[k];
//
//                if (k != layers[i].getNeurons()[j].getNumberInputs() - 1) {
//                    cout << ", ";
//                }
//            }
//            cout << "] ";
//        }
//        cout << "\n\n";
//    }
//    cout << "Output layer: ";
//    int i = layers.size() - 1;
//    for (int j = 0; j < layers[i].getCount(); j++) {
//        cout << "[";
//        for (int k = 0; k < layers[i].getNeurons()[j].getNumberInputs(); k++) {
//            cout << layers[i].getNeurons()[j].getWeights()[k];
//            if (k != layers[i].getNeurons()[j].getNumberInputs() - 1) {
//
//                cout << ", ";
//            }
//        }
//        cout << "] ";
//    }
//    cout << endl << endl;
//}
//

