#include "HashBoard.h"
#include "Board.h"
#include <string>
#include <cstring>
#include <time.h>
#include <cstdlib>
#include<stdlib.h>

HashBoard::HashBoard() {

    seed = time(NULL);
    
    for (int i = 0; i<RAND_NUM_LENGTH; i++) {
        valuesHash[i][0] =RAND_NUMS[i];
        valuesHash[i][1] = randLong();
    }

    memcpy(&sideHash, valuesHash[0], 2 * sizeof (uint64_t));

    int k = 1;
    for (int i = 0; i < 4; i++) {
        memcpy(&whiteCastleHash[i], valuesHash[k], 2 * sizeof (uint64_t));
        memcpy(&blackCastleHash[i], valuesHash[k + 1], 2 * sizeof (uint64_t));
        k += 2;
    }
    for (int i = 0; i < 64; i++) {
        memcpy(&enPassantHash[i], valuesHash[k], 2 * sizeof (uint64_t));
        k++;
        for (int j = 0; j < 6; j++) {
            memcpy(& piecesHash[j][0][i], valuesHash[k], 2 * sizeof (uint64_t));
            k++;
            memcpy(& piecesHash[j][1][i], valuesHash[k], 2 * sizeof (uint64_t));
            k++;
        }
    }
    hash [0] = hash[1] = 0;

}

HashBoard::~HashBoard() {
}

void HashBoard::updateHash(uint64_t vals[]) {
    hash[0] ^= vals[0];
    hash[1] ^= vals[1];
}

uint64_t HashBoard::getLock() const {
    return hash[0];
}

uint64_t HashBoard::getHash() {
    return hash[1];
}



uint64_t HashBoard::randLong() {

    int diff = sizeof (uint64_t) - sizeof (int);
    uint64_t temp = random(&seed);
    temp <<= diff * 8;
    temp |= random(&seed);
    return temp;
}
