/* 
 * File:   Tournament.h
 * Author: Steve
 *
 * Created on 24 June 2013, 8:44 PM
 */

#ifndef TOURNAMENT_H
#define	TOURNAMENT_H

#include "Sphinx.h"
#include "ExtendedSphinx.h"
#include <vector>
#include "Match.h"

class Tournament {
public:

    int* getResults();
    void start();
    void start(std::string);
    void print();
    void interrupt();
    std::string toString();
    Tournament(std::vector<ExtendedSphinx*>&);
    Tournament(Sphinx*,ExtendedSphinx*);
    Tournament(int, bool);
    Tournament();
    virtual ~Tournament();

    int getMatches() {
        return M;
    }
private:

    bool simulation;
    /**
     * Seed used for generating random integers
     */
    unsigned int seed;





    std::string getTime();

    std::vector<Sphinx*> engines;
    std::vector<Match> matches;
    int M;

    std::string itoa(int n) {
        std::ostringstream convert;
        convert << n;
        return convert.str();
    }
};

#endif	/* TOURNAMENT_H */

