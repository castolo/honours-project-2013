/* 
 * File:   TranspositionTable.cpp
 * Author: Steve James
 * 
 * Created on 19 April 2013, 11:19 AM
 */

#include "TranspositionTable.h"
#include "Move.h"
#include <stdlib.h>
#include <string>
#include<string.h>

TranspositionTable::TranspositionTable() {
    map.reserve(SIZE);
    for (int i = 0; i < SIZE; i++) {
        map[i] = NULL_ENTRY;
    }
}

TranspositionTable::~TranspositionTable() {
    //map.clear();
    std::vector<HashEntry>().swap(map);
}

void TranspositionTable::clear() {
    // memset(map, 0, sizeof (map));
    map.clear();
    for (int i = 0; i < SIZE; i++) {
        map[i] = NULL_ENTRY;
    }

}

HashEntry TranspositionTable::get(uint64_t hash, uint64_t lock) {
    for (int i = 0; i < SIZE; i++) {
        int idx = hashCode(hash, i);
        if (map[idx] == NULL_ENTRY) {
            return NULL_ENTRY;
        } else if (map[idx].hash == hash) {
            if (map[idx].lock == lock) {
                return map[idx];
            } else {
                return NULL_ENTRY;
            }
        }
    }
    return NULL_ENTRY;
}

HashEntry TranspositionTable::get(uint64_t hash, uint64_t lock, int sequence) {
    for (int i = 0; i < SIZE; i++) {
        int idx = hashCode(hash, i);
        if (map[idx] == NULL_ENTRY) {
            return NULL_ENTRY;
        } else if (map[idx].hash == hash) {
            if (map[idx].lock != lock) {
                return NULL_ENTRY;
            } else if (abs(map[idx].ancient - sequence) <= ANCIENT_DIFFERENCE) {
                return map[idx];
            } else {
                return NULL_ENTRY;
            }
        }
    }
    return NULL_ENTRY;
}

void TranspositionTable::put(uint64_t hash, uint64_t lock, int move, int eval, int depth, int flag, int ancient) {

    depth = (depth < 0) ? 0 : depth;

    HashEntry entry = {move, eval, depth, flag, ancient, hash, lock};

    for (int i = 0; i < SIZE; i++) {
        int idx = hashCode(hash, i);
        if (map[idx] == NULL_ENTRY) {
            map[idx] = entry;
            i = SIZE;
        } else if (map[idx].hash == hash && map[idx].lock == lock) {
            i = SIZE;
            if ((map[idx].depth <= depth) || (abs(map[idx].ancient - ancient) > ANCIENT_DIFFERENCE)) {
                map[idx] = entry;
            } else if ((map[idx].move <= 0) && (move > 0)) {
                map[idx] = entry;
                //map[idx].move = move;
            }
        } else if (map[idx].hash == hash && map[idx].lock != lock) {
            map[idx] = entry;
            i = SIZE;
        } else if (abs(map[idx].ancient - ancient) > ANCIENT_DIFFERENCE) {
            map[idx] = entry;
            i = SIZE;
        }
    }
}

unsigned int TranspositionTable::size() {
    return SIZE;
}

std::vector<int> TranspositionTable::extractPrincipalVariation(Board* board, int depth) {

    std::vector<int> ans;
    uint64_t hash = board->getHash();
    uint64_t lock = board->getLock();
    HashEntry entry = get(hash, lock);
    for (; (depth > 0) && (entry != NULL_ENTRY) && (entry.move > 0); depth--) {
        int move = entry.move;
        ans.push_back(move);
        board->makeMove(move);
        entry = get(board->getHash(), board->getLock());
    }
    for (int i = ans.size() - 1; i >= 0; i--) {
        board->undoMove(ans[i]);
    }
    return ans;

}