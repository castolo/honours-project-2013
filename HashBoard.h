/**
 * This class is responsible for implementing a Zobrist hashing scheme. It allows 
 * any inheriting class to focus on move generation and board 
 * representation, without having to also worry about its hash function.
 * The child class only has to concern itself with updating the hash when a move
 * is made or unmade. The class actually keeps track of 2 hashes. The first is
 * used to reference databases and opening books and so the random number that
 * are used in the hashing are hard-coded. The second uses random numbers
 * generated at runtime and is used for the transposition tables. Using both of
 * these hashes together ensures that the probability of 2 different positions
 * having the same number is effectively 0.
 *
 * @author Steve James
 * @since 25/12/12
 */

#ifndef HASHBOARD_H
#define	HASHBOARD_H
#include <stdint.h>
#include "Constants.h"
#include <string>

class HashBoard {
protected:

    /**
     * Seed used for generating random integers
     */
    unsigned int seed;
    /**
     * These values are used in calculating the hash when there is a possibility
     * of an en passant capture
     */
    uint64_t enPassantHash [64][2];
    /**
     * These values correspond to [piece_type][side][board_position]. For
     * example, if x = piecesHash[2][0][7], then x will be used in the hash if a
     * white knight is located at position 7
     */
    uint64_t piecesHash [6][2][64][2];
    /**
     * Values representing white's different abilities to castle - kingside,
     * queenside, both, none.
     */
    uint64_t whiteCastleHash [4][2];
    /**
     * Values representing black's different abilities to castle - kingside,
     * queenside, both, none.
     */
    uint64_t blackCastleHash [4][2];
    /**
     * Random number used only when black is playing
     */
    uint64_t sideHash[2];
    /**
     * Array to hold all random numbers used in the above
     */
    uint64_t valuesHash[RAND_NUM_LENGTH][2];
    /**
     * The actual hash value of the board
     */
    uint64_t hash[2];

public:
    /**
     * Creates a new HashBoard. This method loads the random number to be used
     * in Zobrist hashing and populates the arrays. Currently, these numbers are
     * not generated at runtime, but stored in a file. This allows for the hash
     * values to be used to determine which openings can be played in a specific
     * position. This may be changed in future releases.
     */
    HashBoard();
    virtual ~HashBoard();


    /**
     * Updates (XORs) the hash with the two new value
     *
     * @param val the new values to be included in the hash
     */
    void updateHash(uint64_t[]);
    /**
     * Get the hash key corresponding to the board's position, ignoring en
     * passant possibilities
     *
     * @return the hash key
     */
    uint64_t getOpeningHash(int);
    /**
     * Returns the Zobrist hash of the board
     *
     * @return the hash of the board
     */
    uint64_t getHash();
    /**
     * Returns the Zobrist hash of the board used for referencing databases and
     * opening books
     *
     * @return the hash of the board
     */
    uint64_t getLock() const;

private:

    uint64_t randLong();

};

#endif	/* HASHBOARD_H */
