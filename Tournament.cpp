/* 
 * File:   Tournament.cpp
 * Author: Steve
 * 
 * Created on 24 June 2013, 8:44 PM
 */

#include "Tournament.h"
#include "Match.h"
#include <cstdlib>

Tournament::Tournament(std::vector<ExtendedSphinx*>& engines) {
    simulation = true;
    seed = time(NULL);
    for (int i = 0; i < engines.size(); i++) {
        this->engines.push_back(engines[i]);
    }
    int N = engines.size();
    M = N * (N - 1);
    matches.clear();
    for (int i = 0; i < N - 1; i++) {
        for (int j = i + 1; j < N; j++) {
            matches.push_back(Match(engines[i], engines[j], i, j, true));
            matches.push_back(Match(engines[j], engines[i], j, i, true));
        }
    }
}

Tournament::Tournament(Sphinx *A, ExtendedSphinx *B) {
    simulation = true;
    seed = time(NULL);
    engines.push_back(A);
    engines.push_back(B);
    int N = engines.size();
    M = 2000;
    matches.clear();

    for (int i = 0; i < M/2; i++) {
        matches.push_back(Match(A, B, 0));
        matches.push_back(Match(A, B, 1));
    }
}

Tournament::Tournament(int N, bool simulation = false) {
    this->simulation = simulation;
    seed = time(NULL);
    for (int i = 0; i < N; i++) {
        Sphinx* engine;
        if (simulation) {
            engine = new ExtendedSphinx("ExPlayer " + itoa(i + 1), -1);
        } else {
            engine = new Sphinx("Player " + itoa(i + 1));
        }
        engines.push_back(engine);
    }

    M = N * (N - 1);
    matches.clear();
    for (int i = 0; i < N - 1; i++) {
        for (int j = i + 1; j < N; j++) {
            matches.push_back(Match(engines[i], engines[j], i, j));
            matches.push_back(Match(engines[j], engines[i], j, i));
        }
    }
}

/**
 * Get the results of a tournament. These consist of the name
 * wins, draws losses and points (multiplied by 2 so as to be represented by an integer)
 * of each of the participants. 
 * <b> NOTE: Be sure to delete the returned array once used</b>
 * @return the tournaments results
 */
int* Tournament::getResults() {

    int N = engines.size();
    int *results = new int[N];
    for (int i = 0; i < N; i++) {
        results[i] = 0;
    }
    for (int i = 0; i < matches.size(); i++) {

        if (matches[i].getResult() == Match::ILLEGAL_MOVE) {
            results[matches[i].getWhiteID()]++;
            results[matches[i].getBlackID()]++;
        } else if (matches[i].getShortResult() == "1-0") {
            results[matches[i].getWhiteID()] += 2;
        } else if (matches[i].getShortResult() == "0-1") {
            results[matches[i].getBlackID()] += 2;
        } else {
            results[matches[i].getWhiteID()]++;
            results[matches[i].getBlackID()]++;
        }
    }
    return results;
}

std::string Tournament::toString() {
    std::ostringstream oss;
    int results[engines.size()][4]; //results here are captured as 2 for a win, 1 for draw, then 
    //divided by 2 at the end
    for (int i = 0; i < engines.size(); i++) {
        for (int j = 0; j < 4; j++) {
            results[i][j] = 0;
        }
    }
    for (int i = 0; i < matches.size(); i++) {

        if (matches[i].getShortResult() == "1-0") {

            results[matches[i].getWhiteID()][0]++;
            results[matches[i].getBlackID()][2]++;

            results[matches[i].getWhiteID()][3] += 2;

        } else if (matches[i].getShortResult() == "0-1") {
            results[matches[i].getBlackID()][0]++;
            results[matches[i].getWhiteID()][2]++;

            results[matches[i].getBlackID()][3] += 2;
        } else {
            results[matches[i].getWhiteID()][1]++;
            results[matches[i].getBlackID()][1]++;

            results[matches[i].getWhiteID()][3]++;
            results[matches[i].getBlackID()][3]++;
        }
    }
    std::string line = "*********************************************************";
    oss << line << std::endl;
    oss << "* Player\tWins\tDraws\tLosses\tPoints\t*" << std::endl << line << std::endl;
    for (int i = 0; i < engines.size(); i++) {

        std::string name;
        if (simulation) {
            ExtendedSphinx *e = static_cast<ExtendedSphinx*> (engines[i]);
            name = e->getName();
        } else {
            name = engines[i]->getName();
        }
        oss << "* " << name << "\t" << results[i][0] << "\t" <<
                results[i][1] << "\t" << results[i][2] << "\t" << results[i][3]*0.5 << "\t*" << std::endl;
    }
    oss << line << std::endl;
    return oss.str();
}

void Tournament::print() {
    std::cout << toString();
}

void Tournament::interrupt() {

    for (int i = 0; i < matches.size(); i++) {
        matches[i].interrupt();
    }

}

Tournament::Tournament() {
    M = -1;
}

Tournament::~Tournament() {

    for (int i = 0; i < engines.size(); i++) {
        delete engines[i];
    }
    engines.clear();
}

std::string Tournament::getTime() {
    time_t t = time(NULL);
    struct tm * now = localtime(&t);

    char buf[25];
    sprintf(buf, "%02d%02d%02d%02d%02d", now->tm_year % 100,
            now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min);

    return std::string(buf);
}

void Tournament::start(std::string path) {
    for (int i = 0; i < matches.size(); i++) {
        std::cout << "Game " << i + 1 << " in progress..." << std::endl;
        matches[i].commence();
        matches[i].write(path, true);
        std::cout << "Game finished with result: " << matches[i].getStringResult() << std::endl;
        engines[matches[i].getWhiteID()]->reset();
        engines[matches[i].getBlackID()]->reset();
    }
}

void Tournament::start() {
    std::string time = getTime();
    std::cout << "Starting tournament..." << std::endl;
    std::cout << "Matches scheduled: " << M << "\n" << std::endl;

    for (int i = 0; i < matches.size(); i++) {
        std::cout << "Game " << i + 1 << " in progress..." << std::endl;
        matches[i].commence();

        matches[i].write("tournaments/" + time);
        std::cout << "Game finished with result: " << matches[i].getStringResult() << std::endl;
        engines[matches[i].getWhiteID()]->reset();
        engines[matches[i].getBlackID()]->reset();
    }
    print();

}