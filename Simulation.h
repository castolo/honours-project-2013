/* 
 * File:   Simulation.h
 * Author: Workstation
 *
 * Created on 15 August 2013, 10:20 AM
 */

#ifndef SIMULATION_H
#define	SIMULATION_H

#include "GAParams.h"
#include "ExtendedSphinx.h"
#include <random>
#ifdef _WIN32
#include <io.h>
#else
#include <sys/stat.h>
#endif



static const char* DIR = "simulation";
static const char* LOG_FILE = "simulation/log.txt";

class Simulation {
public:
    Simulation();
    virtual ~Simulation();
    virtual void start();
    void interrupt();

    void test();
    
protected:
    bool interrupted;
    int generation;
    unsigned int seed;
    //std::mersenne_twister_engine generator;
    std::default_random_engine generator;
    std::vector<ExtendedSphinx*> population, tempPopulation;
    void log(std::string, bool = true);
    std::string getTime();
    std::vector<double> getRandomParameters();
    int rand(int, int);
    double rand(float, float, int);
    void savePopulation();
    float* calculateFitnessScore(const int*);
    int select(const float*, int);
    std::vector<std::vector<double> > crossover(int, int);
    void mutate(std::vector<double> &);


    int getBest(const float*);
    double N(double, double);

    void createDir(const char* folder = DIR) {
#ifdef _WIN32
        mkdir(folder);
#else
        mkdir(folder, 0777);
#endif
    }

};



#endif	/* SIMULATION_H */

