/* 
 * File:   Line.h
 * Author: Workstation
 *
 * Created on 19 April 2013, 11:19 AM
 */

#ifndef LINE_H
#define	LINE_H

#include <vector>
#include <string>

class Line {
public:
    Line();
    Line(std::vector<int>, int, int, long int, long int);
    Line(const Line& orig);
    virtual ~Line();

    int getDepth() {
        return depth;
    }

    int getBestMove() {

        if (line.size() == 0) {
            return 0;  
        } else {

            return line[0];
            
        }

    }

    std::vector<int> getMoves() {
        return line;
    }


    std::string toString();

    int getEval() {
        return evaluation;
    }

private:
    std::vector<int> line;
    int evaluation;
    int depth;
    long int nodes;
    long int time;

};

#endif	/* LINE_H */
