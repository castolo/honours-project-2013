/* 
 * File:   ParallelTournament.h
 * Author: mint
 *
 * Created on September 5, 2013, 12:11 PM
 */

#ifndef PARALLELTOURNAMENT_H
#define	PARALLELTOURNAMENT_H


#include <string>

class ParallelTournament {
public:

    int* getResults();
    void start();
    std::string toString();
    ParallelTournament(int, std::string, std::string, int);
    virtual ~ParallelTournament();
    int getMatches() {
        return M;
    }
private:

    int N, M;
    int base;
    int**results;
    std::string games, engines;
    void setWin(int,int);
    void setDraw(int,int);
};

#endif	/* PARALLELTOURNAMENT_H */

