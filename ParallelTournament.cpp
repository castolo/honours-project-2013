/* 
 * File:   ParallelTournament.cpp
 * Author: mint
 * 
 * Created on September 5, 2013, 12:11 PM
 */

#include <vector>

#include "ParallelTournament.h"
#include "Constants.h"
#include "Parallel.h"
#include "Match.h"

ParallelTournament::ParallelTournament(int N, std::string p1, std::string p2, int base) {
    this->N = N;
    M = N * (N - 1);


    results = new int*[N];
    for (int i = 0; i < N; i++) {
        results[i] = new int[4];
        for (int j = 0; j < 4; j++) {
            results[i][j] = 0;
        }
    }

    engines = p1;
    games = p2;
    this->base = base;

}

int* ParallelTournament::getResults() {

    int *ans = new int[N];
    for (int i = 0; i < N; i++) {
        ans[i] = results[i][3];
    }
    return ans;

}

std::string ParallelTournament::toString() {

    std::ostringstream oss;
    std::string line = "*********************************************************";
    oss << line << std::endl;
    oss << "* Player\tWins\tDraws\tLosses\tPoints\t*" << std::endl << line << std::endl;
    for (int i = 0; i < N; i++) {

        std::string name = "Engine " + ntos(base + i);
        oss << "* " << name << "\t\t" << results[i][0] << "\t" <<
                results[i][1] << "\t" << results[i][2] << "\t" << results[i][3]*0.5 << "\t*" << std::endl;
    }
    oss << line << std::endl;
    return oss.str();

}

void ParallelTournament::start() {
    std::vector<std::string> files;
    for (int i = 0; i < N; i++) {
        files.push_back(engines + "/" + ntos(i + base) + ".txt");
    }
    std::vector<std::pair<std::string, std::string> > matchups;
    std::vector<std::pair<int, int> > ids;
    for (int i = 0; i < N - 1; i++) {
        for (int j = i + 1; j < N; j++) {
            ids.push_back(std::make_pair(i, j));
            ids.push_back(std::make_pair(j, i));
            matchups.push_back(std::make_pair(files[i], files[j]));
            matchups.push_back(std::make_pair(files[j], files[i]));
        }
    }


    int size = mpi::getSize();

    int k = 0;
    int l = 0;
    for (int i = 0; i < M / (size - 1); i++) {

        for (int j = 1; j < size; j++) {
            std::pair<std::string, std::string> pair = matchups[k++];
            std::string message = pair.first + " " + pair.second + " " + games;
            mpi::send(message, j);
        }

        for (int j = 1; j < size; j++) {
            std::cout << "Waiting on rank "<<j<<" for result"<<std::endl;
            std::string message = mpi::receive(j);
            int result = atoi(message.c_str());
            std::pair<int, int> pair = ids[l++];
            int w = pair.first;
            int b = pair.second;
            if (abs(result) < Match::CHECKMATE) {
                setDraw(w, b);
            } else if (abs(result) == Match::CHECKMATE) {
                if (result > 0) {
                    setWin(w, b);
                } else {
                    setWin(b, w);
                }
            } else if (abs(result) == Match::ILLEGAL_MOVE) {
                setDraw(w,b);
                //                if (result < 0) {
                //                    setWin(w, b);
                //                } else {
                //                    setWin(b, w);
                //                }
            } else {
                if (result < 0) {
                    setWin(w, b);
                } else {
                    setWin(b, w);
                }
            }
        }

    }



    for (int j = 1; j <= M % (size - 1); j++) {
        std::pair<std::string, std::string> pair = matchups[k++];
        std::string message = pair.first + " " + pair.second + " " + games;
        mpi::send(message, j);
    }

    for (int j = 1; j <= M % (size - 1); j++) {
        std::string message = mpi::receive(j);
        int result = atoi(message.c_str());
        std::pair<int, int> pair = ids[l++];
        int w = pair.first;
        int b = pair.second;
        if (abs(result) < Match::CHECKMATE) {
            setDraw(w, b);
        } else if (abs(result) == Match::CHECKMATE) {
            if (result > 0) {
                setWin(w, b);
            } else {
                setWin(b, w);
            }
        } else if (abs(result) == Match::ILLEGAL_MOVE) {
            if (result < 0) {
                setWin(w, b);
            } else {
                setWin(b, w);
            }
        } else {
            if (result < 0) {
                setWin(w, b);
            } else {
                setWin(b, w);
            }
        }
    }


    /*
    for each match
        find an open processor
        send mpi command
        mark processor as closed
        if command received
                read command, store result, mark processor as opened
       
        sleep(10)??
     until all matches played
     */
}

ParallelTournament::~ParallelTournament() {
    for (int i = 0; i < N; i++) {
        delete [] results[i];
    }
    delete [] results;
}

void ParallelTournament::setDraw(int a, int b) {

    results[a][1]++;
    results[b][1]++;
    results[a][3]++;
    results[b][3]++;

}

void ParallelTournament::setWin(int a, int b) {

    results[a][0]++;
    results[b][2]++;
    results[a][3] += 2;

}