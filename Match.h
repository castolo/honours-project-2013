/* 
 * File:   Match.h
 * Author: Steve
 *
 * Created on 23 June 2013, 12:28 PM
 */

#ifndef MATCH_H
#define	MATCH_H

#include <vector>
#include <string>
#include "Constants.h"
#include "Board.h"
#include "Sphinx.h"
#include "ExtendedSphinx.h"

class Match {
public:

    static const short UNSTARTED = 0;
    static const short IN_PROGRESS = 1;

    static const short STALEMATE = 2;
    static const short THREE_MOVE_REPETITION = 3;
    static const short FIFTY_MOVE_RULE = 4;
    static const short INSUFFICIENT_MATERIAL = 5;

    static const short CHECKMATE = 6;

    static const short ILLEGAL_MOVE = 7;
    static const short TIMEOUT = 8;

    Match(Sphinx*, Sphinx*, std::string, std::string);

        Match(Sphinx*, ExtendedSphinx*, int);
    
    Match(Sphinx*, Sphinx*, int, int, bool = false);

    Match();
    virtual ~Match();

    void commence();

    std::string getWhiteName() {
        return wName;
    }

    std::string getBlackName() {
        return bName;
    }

    int getWhiteID() {
        return wID;
    }

    int getBlackID() {
        return bID;
    }

    short getResult() {
        return result;
    }
    bool isInterrupted(){
        return interrupted;
    }
    
    void interrupt() {
        interrupted = true;
        if (white) white->interrupt();
        if (black) black->interrupt();
    }

    std::string getStringResult() {

        if (abs(result) <= IN_PROGRESS) {
            return "1/2-1/2*";
        } else if (abs(result) <= INSUFFICIENT_MATERIAL) {
            std::string s = "1/2-1/2 ";
            switch (abs(result)) {
                case STALEMATE: return s + "(stalemate)";
                case THREE_MOVE_REPETITION: return s + "(3-fold repetition)";
                case FIFTY_MOVE_RULE: return s + "(fifty-move rule)";
                default: return s + "(insufficient material)";
            }
        } else if (abs(result) == CHECKMATE) {

            if (result > 0) {
                return "1-0";
            } else {
                return "0-1";
            }
        } else if (abs(result) == ILLEGAL_MOVE) {
            if (result < 0) {
                return "1-0 (illegal move - " + illegalMove + ")";
            } else {
                return "0-1 (illegal move - " + illegalMove + ")";
            }
        } else {
            if (result < 0) {
                return "1-0 (time out)";
            } else {
                return "0-1 (time out)";
            }
        }

    }

    std::string getShortResult() {


        if (abs(result) <= IN_PROGRESS) {
            return "1/2-1/2 (in progess)";
        } else if (abs(result) <= INSUFFICIENT_MATERIAL) {
            return "1/2-1/2";
        } else if (abs(result) == CHECKMATE) {

            if (result > 0) {
                return "1-0";
            } else {
                return "0-1";
            }
        } else if (abs(result) == ILLEGAL_MOVE) {
            if (result < 0) {
                return "1-0";
            } else {
                return "0-1";
            }
        } else {
            if (result < 0) {
                return "1-0";
            } else {
                return "0-1";
            }
        }

    }
    void write(std::string, bool = false);
private:
    std::string illegalMove;
    int getRepetitions(uint64_t);
    Board board;
    bool interrupted;

    std::string position;
    short result;
    std::vector<uint64_t> positions;
    std::vector<int> moves;
    std::vector<std::string> thoughts;
    Sphinx* white;
    Sphinx* black;
    std::string wName, bName;
    int wID, bID;



};

#endif	/* MATCH_H */

