/* 
 * File:   Layer.h
 * Author: Workstation
 *
 * Created on 19 July 2013, 4:30 PM
 */

#ifndef LAYER_H
#define	LAYER_H

#include "Neuron.h"

class NeuralLayer {
public:
    NeuralLayer(int, int);
    virtual ~NeuralLayer();

    int getCount() const{
        return numNeurons;
    }

    void setWeight(int,int,double);
    
    std::vector<Neuron> getNeurons() const{
        return neurons;
    }

private:
    int numNeurons;
    std::vector<Neuron> neurons;
};

#endif	/* LAYER_H */

