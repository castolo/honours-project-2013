/* 
 * File:   OpeningBook.cpp
 * Author: Workstation
 * 
 * Created on 12 June 2013, 1:34 PM
 */

#include "OpeningBook.h"
#include "Board.h"
#include "Move.h"
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iomanip>

OpeningBook::OpeningBook() {
    map.clear();
    keySet.clear();
    open = true;
    seed = time(NULL);
}

OpeningBook::OpeningBook(const OpeningBook& orig) {
    map = orig.map;
    keySet = orig.keySet;
    open = orig.open;
    seed = orig.open;
}

OpeningBook::~OpeningBook() {
}

// <editor-fold defaultstate="collapsed" desc="Build Opening Book">

int OpeningBook::findMove(std::string s, int* moves, std::string fen) {

    replace(s, "+", "");
    replace(s, "#", "");

    if (s.find("O") != std::string::npos) {
        replace(s, "O", "0");
    }

    Board b;
    b.setPosition(fen);


    for (int i = 0; int move = moves[i]; i++) {

        std::string n = move::toInputNotation(move);
        std::string notation = "";
        bool flag = false;
        int type = move::getType(move);
        switch (abs(move::getMovingPiece(move))) {
            case KING:
                if (type == MOVE_SHORT_CASTLE) {
                    notation = "0-0";
                    flag = true;
                    break;
                }
                if (type == MOVE_LONG_CASTLE) {
                    notation = "0-0-0";
                    flag = true;
                    break;
                }
                notation += "K";
                break;
            case QUEEN:
                notation += "Q";
                break;
            case ROOK:
                notation += "R";
                break;
            case BISHOP:
                notation += "B";
                break;
            case KNIGHT:
                notation += "N";
                break;
        }

        if (!flag) {
            if (move::getCapturedPiece(move) != EMPTY_SQUARE || type == MOVE_CAPTURE_EN_PASSANT) {
                if (abs(move::getMovingPiece(move)) == PAWN) {
                    notation += n[0];
                }
                notation += "x";
            }

            notation += n[2];
            notation += n[3];

            switch (type) {
                case MOVE_PROMOTION_QUEEN:
                    notation += "=Q";
                    break;
                case MOVE_PROMOTION_ROOK:
                    notation += "=R";
                    break;
                case MOVE_PROMOTION_BISHOP:
                    notation += "=B";
                    break;
                case MOVE_PROMOTION_KNIGHT:
                    notation += "=N";
                    break;
            }
        }

        if (notation == s) {

            if (b.makeMove(move))
                return move;
        }

        std::string temp = s;
        replace(temp, "x", "");
        replace(temp, "-", "");
        if (temp.size() == 4) {
            char extra = temp[1];
            std::string ns = "";
            ns += s[0];
            for (int i = 2; i < s.size(); i++) {
                ns += s[i];
            }

            if (notation == ns) {

                if (isalpha(extra)) {

                    if (n[0] == extra) {
                        if (b.makeMove(move))
                            return move;
                    }

                } else {

                    if (n[1] == extra) {
                        if (b.makeMove(move))
                            return move;
                    }

                }
            }
        }



    }
    return 0;
}

void OpeningBook::addStats() {
    using namespace std;
    string path = "C:\\Users\\Workstation\\Downloads\\software\\millionbase-2.22.pgn";
    ifstream file(path.c_str());
    string line;
    string opening = "", prev = "";

    cout << "Games read:\n" << endl;
    int blackElo, whiteElo;
    string result;
    int count = 0;
    while (file.good()) {
        bool flag = false;
        blackElo = whiteElo = 0;
        result = "";
        //read info
        getline(file, line);
        line = trim(line);
        while (line != "") {

            if (line.find("[BlackElo") != string::npos) {
                replace(line, "[BlackElo \"", "");
                replace(line, "\"]", "");
                blackElo = atoi(line.c_str());
            } else if (line.find("[WhiteElo") != string::npos) {
                replace(line, "[WhiteElo \"", "");
                replace(line, "\"]", "");
                whiteElo = atoi(line.c_str());
            } else if (line.find("[Result") != string::npos) {
                replace(line, "[Result \"", "");
                replace(line, "\"]", "");
                result = line;
            }

            getline(file, line);
            line = trim(line);
        }
        if (blackElo > 0 && whiteElo > 0 && result != "") {
            flag = true;
        }

        if (blackElo < 2200 || whiteElo < 2200) {
            flag = false;
        }

        //read moves
        getline(file, line);
        line = trim(line);
        string moves = "";
        while (line != "") {

            if (flag) {
                moves += line + " ";
                //process moves
            }

            if (!file.good()) {
                line = "";
            } else {
                getline(file, line);
                line = trim(line);
            }
        }
        if (flag) {


            moves = trim(moves);
            vector<string> v = split(moves, ' ');

            Board b;
            b.setPosition(START_POSITION);
            int c = 0;
            bool good = true;
            for (int i = 0; i < v.size(); i++) {

                if (c > 60) {
                    break;
                }

                string a = trim(v[i]);
                if (a == result || a.find(".") != string::npos) {
                } else {

                    int * mov = b.generatePseudoMoves();
                    int move = findMove(trim(a), mov, b.getPosition());
                    delete[]mov;
                    if (move > 0) {
                        b.makeMove(move);
                        c++;
                        //   b.show();
                    } else {
                        //   cout << "FAILED: \n" << a << "\n" << moves;
                        good = false;
                        break;
                    }
                }
            }
            if (good) {

                Board board;
                board.setPosition(START_POSITION);
                c = 0;
                for (int i = 0; i < v.size(); i++) {

                    if (c > 60) {
                        break;
                    }

                    string a = trim(v[i]);
                    if (a == result || a.find(".") != string::npos) {
                    } else {

                        int * mov = board.generatePseudoMoves();
                        int move = findMove(trim(a), mov, board.getPosition());
                        delete[]mov;
                        if (move > 0) {


                            uint64_t key = board.getOpeningHash();
                            if (map.find(key) != map.end()) {


                                std::string m = move::toInputNotation(move);
                                std::vector<OpeningMove> bucket = map[key];


                                for (int j = 0; j < bucket.size(); j++) {
                                    OpeningMove om = bucket[j];
                                    om.occurences++;
                                    bucket[j] = om;
                                }
                                map[key] = bucket;
                                for (int j = 0; j < bucket.size(); j++) {
                                    OpeningMove om = bucket[j];
                                    if (om.getMove() == m) {
                                        int res = 0;
                                        uint64_t totELO = om.getCount() * om.getELO();
                                        int mELO = (board.getSide() == WHITE) ? whiteElo : blackElo;
                                        int oELO = (board.getSide() == BLACK) ? whiteElo : blackElo;
                                        totELO += mELO;
                                        om.count++;
                                        om.averageELO = round(totELO / (static_cast<double> (om.count)));

                                        if (result == "1-0" && board.getSide() == WHITE) {
                                            om.wins++;
                                            res = 1;
                                        } else if (result == "0-1" && board.getSide() == WHITE) {
                                            om.losses++;
                                            res = -1;
                                        } else if (result == "0-1" && board.getSide() != WHITE) {
                                            om.wins++;
                                            res = 1;
                                        } else if (result == "1-0" && board.getSide() != WHITE) {
                                            om.losses++;
                                            res = -1;
                                        } else {
                                            res = 0;
                                            om.draws++;
                                        }

                                        if (mELO > 2500) {
                                            om.gmCount++;
                                        }
                                        bucket[j] = om;
                                        map[key] = bucket;
                                        break;
                                    }
                                }
                            }
                            board.makeMove(move);
                            c++;
                            //   b.show();
                        } else {
                            //   cout << "FAILED: \n" << a << "\n" << moves;
                            good = false;
                            break;
                        }
                    }
                }





                count++;
                cout << count << endl;
            }

        }
    }
}

void OpeningBook::build() {

    std::string path = "openings.txt";
    std::ifstream file(path.c_str());



    while (file.good()) {
        std::string opening;
        std::getline(file, opening);
        std::string line;
        std::getline(file, line);
        std::vector<std::string> moves = split(line, ' ');
        addOpening(opening, moves);
    }
    addStats();
}

void OpeningBook::write() {


    std::ofstream file;
    file.open("openingBook.txt");
    for (int i = 0; i < keySet.size(); i++) {

        file << keySet[i] << "|";
        std::vector<OpeningMove> vec = map[keySet[i]];
        for (int j = 0; j < vec.size(); j++) {
            OpeningMove m = vec[j];
            file << m.getMove() << ";";
            file << m.getOpening() << ";";
            file << m.getCount() << ";" << m.getGMCount() << ";" << m.getELO() << ";" << m.getOccurences() << ";";
            file << m.getWins() << ";" << m.getDraws() << ";" << m.getLosses();
            file << "|";
        }
        file << std::endl;

    }
    file.close();

}

// </editor-fold>

void OpeningBook::addOpening(std::string opening,const std::vector<std::string>& stringMoves) {
    Board board;
    board.setPosition(START_POSITION);


    for (int i = 0; i < stringMoves.size(); i++) {

        int finalMove = -1;
        int * actualMoves = board.generatePseudoMoves();

        for (int j = 0; int move = actualMoves[j]; j++) {

            if (move::toInputNotation(move) == stringMoves[i]) {
                finalMove = move;
                break;
            }
        }
        delete[]actualMoves;
        OpeningMove openingMove(opening, stringMoves[i]);
        uint64_t key = board.getOpeningHash();
        if (map.find(key) != map.end()) {
            std::vector<OpeningMove> v = map[key];
            v.push_back(openingMove);
            map[key] = v;
        } else {
            std::vector<OpeningMove> v;
            v.push_back(openingMove);
            map[key] = v;
            keySet.push_back(key);
        }

        board.makeMove(finalMove);
    }
    OpeningMove openingMove(opening, "");
    uint64_t key = board.getOpeningHash();
    if (map.find(key) != map.end()) {
        std::vector<OpeningMove> v = map[key];
        v.push_back(openingMove);
        map[key] = v;
    } else {
        std::vector<OpeningMove> v;
        v.push_back(openingMove);
        keySet.push_back(key);
        map[key] = v;
    }
}

void OpeningBook::print(const std::vector<OpeningMove> &moves) {
    using namespace std;

    cout << "*****************************************************************************************************************************************\n";
    cout << "*\t\t\tOpening \t\t\t\tMove \tCount\tAve ELO\t\tWins\tDraws\tLosses\tGM\tRating\t*\n";
    cout << "*****************************************************************************************************************************************\n";
    bool flag = false;
    for (int i = 0; i < moves.size(); i++) {
        OpeningMove m = moves[i];
        if (m.count > 0) {
            flag = true;
            break;
        }
    }

    for (int i = 0; i < moves.size(); i++) {
        OpeningMove m = moves[i];
        if (m.count > 0 || !flag || m.getMove() == "<end>") {
            string o = m.opening;
            while (o.size() < 60) {
                o += " ";
            }
            char s[20];
            sprintf(s, "%.3f", 100 * rateOpening(m));

            cout << "* " << o.substr(0, 59) << "\t" << m.getMove() << "\t" << m.count << "\t  " << m.averageELO << "\t\t" <<
                    m.wins << "\t" << m.draws << "\t" << m.losses << "\t" << m.gmCount << "\t" << string(s) << "\t*\n";
        }
    }
    cout << "*****************************************************************************************************************************************\n";
    // count, averageELO, delta, wins, draws, losses, gmCount;

}

double OpeningBook::rateOpening(OpeningMove o) {

    double x = static_cast<double> (o.count);
    if (x == 0) {
        return 0;
    }

    double u = x / o.occurences;

    double v = 2 * u * ((o.wins + 0.5 * o.draws) / x);
    double w = o.averageELO > 2400 ? u * (o.averageELO - 2400) / 1000.0 : 0;
    double y = u * o.gmCount / x;


    return v + w + y;


}

OpeningMove OpeningBook::getMove( Board* board) {
    std::vector<OpeningMove> moves = getMoves(board);

    if (moves.size() == 0) {
        return OpeningMove("", "");
    }

    double scores[moves.size()];

    double totScore = 0;
    for (int i = 0; i < moves.size(); i++) {
        double score = rateOpening(moves[i]);
        scores[i] = score;
        totScore += score;
    }

    int N = totScore * 1000 + 1;
    OpeningMove selections[N];
    int count = 0;
    for (int i = 0; i < moves.size(); i++) {
        int s = scores[i] * 1000;
        while (s--) {
            selections[count] = moves[i];
            count++;
        }
    }
    while (count < N) {
        selections[count] = OpeningMove("", "");
        count++;
    }

    OpeningMove o("", "");
    for (int j = 0; j < 2; j++) {
        int i = random(&seed) % N;
        o = selections[i];

        if (o.getMove() != "") {
            bool flag = false;
            int * actualMoves = board->generatePseudoMoves();
            for (int k = 0; int move = actualMoves[k]; k++) {
                if (move::toInputNotation(move) == o.getMove()) {
                    flag = true;
                    break;
                }
            }
            delete[]actualMoves;
            if (!flag) {
                o = OpeningMove("", "");
            }
        }
        if (o.getMove() != "") {
            break;
        }
    }
    //  std::cout << o.getOpening() << "\n";
    return o;


}

std::vector<OpeningMove> OpeningBook::getMoves(Board* board) {
    uint64_t key = board->getOpeningHash();
    std::vector<OpeningMove> moves;
    if (map.find(key) != map.end()) {
        moves = map[key];
    }

    int * m = board->generatePseudoMoves();
    for (int i = 0; int move = m[i]; i++) {

        if (board->makeMove(move)) {

            key = board->getOpeningHash();
            if (map.find(key) != map.end()) {
                OpeningMove best("", "");
                std::vector<OpeningMove> temp = map[key];
                for (int j = 0; j < temp.size(); j++) {
                    if (temp[j].count > 0) {
                        OpeningMove om("--> " + temp[j].opening, move::toInputNotation(move),
                                temp[j].count, temp[j].averageELO, temp[j].occurences, temp[j].wins,
                                temp[j].draws, temp[j].losses, temp[j].gmCount);
                        bool found = false;
                        for (int k = 0; k < moves.size(); k++) {
                            if (moves[k].move == move::toInputNotation(move)) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {
                            if (om.count > best.count) {
                                best = om;
                            }
                        }
                    }
                }
                if (best.count > 0) {
                    moves.push_back(best);
                }
            }

        }
        board->undoMove(move);

    }
    delete[]m;
    return moves;
}

void OpeningBook::read() {
    map.clear();
    std::string path = "openingBook.txt";
    std::ifstream file(path.c_str());

    std::string line;
    while (file.good()) {

        std::getline(file, line);
        std::vector<std::string> v1 = split(line, '|');
        uint64_t key = strtoull(v1[0].c_str(), NULL, 10);
        std::vector<OpeningMove> bucket;
        for (int i = 1; i < v1.size(); i++) {
            std::vector<std::string> data = split(v1[i], ';');
            if (data.size() == 9) {
                OpeningMove m(data[1], data[0], atoi(data[2].c_str()), atoi(data[4].c_str()),
                        atoi(data[5].c_str()), atoi(data[6].c_str()), atoi(data[7].c_str()),
                        atoi(data[8].c_str()), atoi(data[3].c_str()));

                bucket.push_back(m);
            }
        }
        map[key] = bucket;
    }

}

