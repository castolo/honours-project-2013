/* 
 * File:   GAParams.h
 * Author: mint
 *
 * Created on September 8, 2013, 11:16 AM
 */

#ifndef GAPARAMS_H
#define	GAPARAMS_H
#include "Constants.h"


class GAParams {
public:
    static int POPULATION_SIZE;
    static int MAX_GENERATIONS;
    static float CROSSOVER_RATE;
    static float MUTATION_RATE;
    static const int CROSSOVER_POINT = NUM_EVAL_WEIGHTS;
    static const int ELITISM = 1;
    static int TIME_PER_MOVE;
    static int GENERATION_START;
    static int MIN_DEPTH;
    static int MAX_MOVES;
    static void load();

};

#endif	/* GAPARAMS_H */

