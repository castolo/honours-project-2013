/* 
 * File:   ParallelSimulation.cpp
 * Author: mint
 * 
 * Created on September 5, 2013, 12:11 PM
 */

#include "ParallelSimulation.h"
#include "ParallelTournament.h"
#include "Parallel.h"

ParallelSimulation::ParallelSimulation() : Simulation() {
}

ParallelSimulation::~ParallelSimulation() {
}

void ParallelSimulation::start() {

    Simulation::interrupted = false;
    Simulation::population.clear();
    Simulation::generation = 0;
    Simulation::log("Simulation starting");

    GAParams::load();

    // <editor-fold defaultstate="collapsed" desc="Randomly create initial population">
    if (GAParams::GENERATION_START > 0) {
        std::string dir = DIR;
        dir += "/generation_" + ntos(GAParams::GENERATION_START);
        for (int i = 0; i < GAParams::POPULATION_SIZE; i++) {
            std::string name = ntos((GAParams::GENERATION_START * GAParams::POPULATION_SIZE) + i);
            ExtendedSphinx *engine = new ExtendedSphinx(GAParams::GENERATION_START);
            std::cout << dir + "/" + name + ".txt" << std::endl;
            engine->read(dir + "/" + name + ".txt");
            population.push_back(engine);
        }
    } else {
        for (int i = 0; i < GAParams::POPULATION_SIZE; i++) {
            std::string name = ntos((generation * GAParams::POPULATION_SIZE) + i);
            ExtendedSphinx *engine = new ExtendedSphinx(generation);
            engine->setName(name);
            std::vector<double> params = getRandomParameters();
            engine->setEvaluationParameters(params);
            params = engine->getParameters();
            population.push_back(engine);
        }
    }
    Simulation::log("Initial population of size " + ntos(GAParams::POPULATION_SIZE) + " created");
    // </editor-fold>

    for (generation = GAParams::GENERATION_START; generation < GAParams::MAX_GENERATIONS; generation++) {

        // <editor-fold defaultstate="collapsed" desc="Save population to text file">
        savePopulation();
        std::string engineLocation = DIR;
        engineLocation += ("/generation_" + ntos(generation));
        createDir(engineLocation.c_str());
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Play tournament and get fitness function">

        std::string gameLocation = DIR;
        gameLocation += ("/generation_" + ntos(generation)) + "/games";
        createDir(gameLocation.c_str());
        ParallelTournament t = ParallelTournament(GAParams::POPULATION_SIZE, engineLocation,
                gameLocation, generation * GAParams::POPULATION_SIZE);
        Simulation::log("Starting tournament " + ntos(generation) + " (" + ntos(t.getMatches()) + " matches scheduled)");
        t.start();
        Simulation::log("Tournament " + ntos(generation) + " completed with result:\n" + t.toString());
        int* results = t.getResults();
        Simulation::log("Calculating fitness scores");
        float* fitness = calculateFitnessScore(results);
        delete [] results;
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Generate new population">

        int i = 0;
        int N = population.size();
        std::vector<std::vector<double> > newPop;
        std::vector<std::pair<int, int> > parents;

        Simulation::log("Generating new population " + ntos(generation + 1));

        //ELITISM
        // for (int k = 0; k < GAParams::ELITISM; k++) {
        int A = getBest(fitness);
        newPop.push_back(population.at(A)->getParameters());
        parents.push_back(std::make_pair(atoi(population.at(A)->getName().c_str()), -2));
        i++;
        //}

        while (i < N) {

            int A = select(fitness, N);
            int B = select(fitness, N);

            std::vector<std::vector<double> > C = crossover(A, B);
            bool singleton = (C.size() == 1);
            for (int j = 0; j < C.size(); j++) {

                mutate(C[j]);

                if (singleton) {
                    parents.push_back(std::make_pair(atoi(population.at(A)->getName().c_str()), -1));
                } else {
                    parents.push_back(std::make_pair(atoi(population.at(A)->getName().c_str()), atoi(population.at(B)->getName().c_str())));
                    int t = A;
                    A = B;
                    B = t;
                }


                newPop.push_back(C[j]);
            }
            i += C.size();
        }
        delete[]fitness;
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Set the new population to be used next by the GA">

        //SET POPULATION TO NEW POP
        for (int i = 0; i < population.size(); i++) {
            delete population[i];
        }
        population.clear();

        for (int i = 0; i < GAParams::POPULATION_SIZE; i++) {
            std::string name = ntos(((generation + 1) * GAParams::POPULATION_SIZE) + i);

            int A = parents.at(i).first;
            int B = parents.at(i).second;

            ExtendedSphinx *engine = new ExtendedSphinx(generation);
            engine->setName(name);
            engine->setParents(A, B);
            std::vector<double> params = newPop.at(i);
            engine->setAllParameters(params);
            ExtendedSphinx *engine2 = new ExtendedSphinx(generation);
            engine2->setName(name);
            engine2->setParents(A, B);
            engine2->setAllParameters(params);
            population.push_back(engine);
        }
        Simulation::log("Population from generation " + ntos(generation) + " created");
        // </editor-fold>

    }

    int S = mpi::getSize();
    for (int i = 1; i < S; i++) {
        mpi::send("quit", i);
    }
}