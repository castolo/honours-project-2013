/* 
 * File:   Match.cpp
 * Author: Steve
 * 
 * Created on 23 June 2013, 12:28 PM
 */

#include "Match.h"
#include <iostream>
#include <fstream>
#include <ctime>
#ifdef _WIN32
#include <io.h>
#else
#include <sys/stat.h>
#endif
#include "Move.h"
#include "ExtendedSphinx.h"
#include "GAParams.h"

Match::Match() {
    result = UNSTARTED;
    interrupted = false;
    wID = -1;
    bID = -1;
}

Match::Match(Sphinx* A, ExtendedSphinx* B, int isWhite) {
    if (isWhite == 0) {
        white = A;
        black = B;
        wName = A->getName();
        bName = B->getName();

    } else {
        white = B;
        black = A;
        wName = B->getName();
        bName = A->getName();
    }

    wID = isWhite;
    bID = (isWhite + 1) % 2;
    result = UNSTARTED;
    interrupted = false;
}

Match::Match(Sphinx* w, Sphinx* b, int wi, int bi, bool simulation) {

    white = w;
    black = b;

    if (simulation) {
        ExtendedSphinx* e = static_cast<ExtendedSphinx*> (w);
        wName = e->getName();
        e = static_cast<ExtendedSphinx*> (b);
        bName = e->getName();
    } else {
        wName = w->getName();
        bName = b->getName();
    }
    result = UNSTARTED;
    interrupted = false;
    wID = wi;
    bID = bi;
}

Match::Match(Sphinx* w, Sphinx* b, std::string wn, std::string bn) {
    result = UNSTARTED;
    this->white = w;
    this->black = b;
    this->wName = wn;
    this->bName = bn;
    interrupted = false;
    wID = -1;
    bID = -1;
}

void Match::commence() {
    int nMoves = 0;
    // black->reset();
    //white->reset();
    white->setOption(Sphinx::OPTION_VERBOSE, false);
    black->setOption(Sphinx::OPTION_VERBOSE, false);

    white->setMinDepth(GAParams::MIN_DEPTH);
    black->setMinDepth(GAParams::MIN_DEPTH);

    position = START_POSITION;
    std::vector<std::string> sMoves;

    board.setPosition(position);
    positions.push_back(board.getHash());
    result = IN_PROGRESS;

    GAParams::load();

    while ((result == IN_PROGRESS) && (!interrupted) && (nMoves < 2 * GAParams::MAX_MOVES)) {

        nMoves++;
        //board.show();

        long int time = GAParams::TIME_PER_MOVE * 30;
        int move;
        Line line;
        int side = board.getSide();
        if (side == WHITE) {

            white->setPosition(START_POSITION);
            for (int i = 0; i < sMoves.size(); i++) {
                white->playMove(sMoves[i]);
            }
            line = white->search(time);
            move = line.getBestMove();
        } else {
            black->setPosition(START_POSITION);
            for (int i = 0; i < sMoves.size(); i++) {
                black->playMove(sMoves[i]);
            }
            line = black->search(time);
            move = line.getBestMove();
        }
        if (!interrupted) {
            int * moves = board.generatePseudoMoves();
            bool legal = false;
            for (int i = 0; int m = moves[i]; i++) {
                if (m == move) {
                    if (board.makeMove(m)) {
                        sMoves.push_back(move::toInputNotation(m));
                        //     std::cout << move::toInputNotation(m) << std::endl;
                        legal = true;
                        break;
                    } else {
                        board.undoMove(m);
                        legal = true;
                        break;
                    }
                }
            }

            delete[] moves;

            if (legal) {
                this->moves.push_back(move);
                this->thoughts.push_back(line.toString());
                position = board.getPosition();
                positions.push_back(board.getHash());
                legal = false;
                int * temp = board.generatePseudoMoves();
                for (int i = 0; int k = temp[i]; i++) {
                    if (board.makeMove(k)) {
                        legal = true;
                        board.undoMove(k);
                        break;
                    } else {
                        board.undoMove(k);
                    }
                }
                delete[]temp;
                if (!legal) {
                    uint64_t king = board.getKing();
                    board.swapSides();
                    if (board.attacks(king)) {
                        result = side * CHECKMATE;
                    } else {
                        result = side * STALEMATE;
                    }
                    board.swapSides();
                }
            } else {
                illegalMove = move::toInputNotation(move);
                result = ILLEGAL_MOVE * side;
            }

            if (result == IN_PROGRESS) {
                int reps = getRepetitions(board.getHash());
                if (reps >= 3) {
                    result = side * THREE_MOVE_REPETITION;
                } else if (board.getFiftyRule() >= 100) {
                    result = side * FIFTY_MOVE_RULE;
                } else if (!board.hasSufficientMaterial()) {

                    result = side * INSUFFICIENT_MATERIAL;
                }
            }

        }
    }
    black->reset();
    white->reset();
}

void Match::write(std::string folder, bool simulation) {
    std::ofstream file;
    std::string title = wName + "-" + bName;

    if (!simulation) {
#ifdef _WIN32
        mkdir("tournaments");
        mkdir(folder.c_str());
#else
        mkdir("tournaments", 0777);
        mkdir(folder.c_str(), 0777);
#endif
    }

    time_t t = time(NULL);
    struct tm * now = localtime(&t);
    char buf[50];
    char buf2[50];
    sprintf(buf, "%04d.%02d.%02d %02d:%02d:%02d", 1900 + now->tm_year,
            now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
    sprintf(buf2, "%02d.%02d.%02d", now->tm_hour, now->tm_min, now->tm_sec);
    std::string date = std::string(buf);
    std::string temp = std::string(buf2);
    file.open(folder + "/" + title + temp + ".pgn");
    file << "[Date \"" << date << "\"]" << std::endl;
    file << "[White \"" << wName << "\"]" << std::endl;
    file << "[Black \"" << bName << "\"]" << std::endl;

    file << "[Result \"" << getStringResult() << "\"]" << "\n" << std::endl;
    for (int i = 0; i < moves.size(); i++) {
        if (i % 2 == 0) {
            file << (i / 2) + 1 << ". ";
        }
        file << move::toInputNotation(moves[i]) << " {" << thoughts[i] << "} ";
        if (i % 10 == 0 && (i != moves.size() - 1) && i != 0) {

            file << std::endl;
        }
    }
    file << getShortResult();
    file.close();
}

int Match::getRepetitions(uint64_t pos) {
    int count = 0;
    for (int i = 0; i < positions.size(); i++) {
        if (positions[i] == pos) {

            count++;
        }
    }
    return count;
}

Match::~Match() {
    if (!white)
        delete white;
    if (!black)
        delete black;
}

