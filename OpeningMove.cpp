/* 
 * File:   OpeningMove.cpp
 * Author: Workstation
 * 
 * Created on 12 June 2013, 2:03 PM
 */

#include "OpeningMove.h"

OpeningMove::OpeningMove(std::string o, std::string m) {
    opening = o;
    move = m;
    count = averageELO = occurences = wins = draws = losses = gmCount = 0;
}

OpeningMove::OpeningMove(std::string o, std::string m, int c, int e, int d, int w, int dr, int l, int g) {
    opening = o;
    move = m;
    count = c;
    averageELO = e;
    occurences = d;
    wins = w;
    draws = dr;
    losses = l;
    gmCount = g;
}

OpeningMove::~OpeningMove() {
}

