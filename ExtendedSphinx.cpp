/* 
 * File:   ExtendedSphinx.cpp
 * Author: Workstation
 * 
 * Created on 10 August 2013, 5:30 PM
 */

#include "ExtendedSphinx.h"
#include <fstream>

ExtendedSphinx::ExtendedSphinx(int gen) : Sphinx() {
    network = new FastNeuralNetwork(384, 16, 1);
    generation = gen;
    parent1 = parent2 = -1;
    networkWeight = DEFAULT_NETWORK_WEIGHT;
}

ExtendedSphinx::ExtendedSphinx(std::string name, int gen) : Sphinx(name) {
    network = new FastNeuralNetwork(384, 16, 1);
    generation = gen;
    parent1 = parent2 = -1;
    networkWeight = DEFAULT_NETWORK_WEIGHT;
}

void ExtendedSphinx::setAllParameters(const std::vector<double>&params) {
    networkWeight = floor(params[0] + 0.5); //round
    std::vector<double> v1, v2;
    int N = params.size();
    int K = network->getNumberOfWeights();
    for (int i = 1; i < N - K; i++) {
        v1.push_back(params[i]);
    }
    for (int i = N - K; i < N; i++) {
        v2.push_back(params[i]);
    }
    board.setParameters(v1);
    network->update(v2);
}

void ExtendedSphinx::setEvaluationParameters(const std::vector<double>&params) {
    networkWeight = floor(params[0] + 0.5); //round
    std::vector<double> vec(params.begin() + 1, params.end());
    board.setParameters(vec);
}

int ExtendedSphinx::evaluatePosition() {
    if (networkWeight == 0) {
       return Sphinx::evaluatePosition();
    } else {
        return Sphinx::evaluatePosition() + board.getSide() * (networkWeight * network->activate(board)[0]);
    }
}

ExtendedSphinx::~ExtendedSphinx() {
    delete network;
}

std::string ExtendedSphinx::getName() const {
    if (name == "") {
        return "N/A";
    }
    return name;
}

int ExtendedSphinx::getGeneration() const {
    return generation;
}

std::vector<double> ExtendedSphinx::getParameters() {

    std::vector<double> params;

    params.push_back(networkWeight);
    std::vector<double> v1 = board.getParameters();
    std::vector<double> v2 = network->getWeights();

    for (int i = 0; i < v1.size(); i++) {
        params.push_back(v1[i]);
    }
    for (int i = 0; i < v2.size(); i++) {
        params.push_back(v2[i]);
    }

    return params;


}

std::string ExtendedSphinx::getData() const {
    std::vector<double> v1 = board.getParameters();
    std::vector<double> v2 = network->getWeights();
    std::string ans;
    ans += ntos(networkWeight) + " ";
    for (int i = 0; i < v1.size(); i++) {
        ans += ntos<double>(v1[i]) + " ";
    }
    for (int i = 0; i < v2.size(); i++) {
        ans += ntos<double>(v2[i]);
        if (i != v2.size() - 1) {
            ans += " ";
        }
    }
    return ans;
}

void ExtendedSphinx::read(std::string path) {

    std::ifstream file(path.c_str());

    std::string line;
    while (file.good()) {

        std::getline(file, line);
        if (line == "") {
            break;
        }
        std::vector<std::string> v = split(line, ':');

        if (v[0] == "Name") {
            name = v[1];
        } else if (v[0] == "Generation") {
            generation = atoi(v[1].c_str());
        } else if (v[0] == "Data") {

            std::vector<std::string> params = split(v[1], ' ');
            int N = params.size();
            int Q = network->getNumberOfWeights();
            int P = N - Q;
            std::vector<double> v1;
            std::vector<double> v2;
            networkWeight = floor(atof(params[0].c_str()) + 0.5);
            for (int i = 1; i < P; i++) {
                double t = atof(params[i].c_str());
                v1.push_back(t);
            }
            for (int i = P; i < N; i++) {
                double t = atof(params[i].c_str());
                v2.push_back(t);
            }
            board.setParameters(v1);
            network->update(v2);
        } else if (v[0] == "Parents") {
            std::vector<std::string> parents = split(v[1], ' ');
            parent1 = atoi(parents[0].c_str());
            parent2 = atoi(parents[1].c_str());
        }
    }
    file.close();

}

void ExtendedSphinx::write(std::string filename) {
    std::ofstream file;
    file.open(filename);
    file << "Name:" << getName() << std::endl << "Generation:" << getGeneration() << std::endl <<
            "Parents:" << parent1 << " " << parent2 << std::endl << "Data:" << getData() << std::endl;
    file.close();
}

