/* 
 * File:   KillerHeuristic.cpp
 * Author: 475531
 * 
 * Created on 11 April 2013, 8:38 AM
 */

#include "KillerHeuristic.h"

KillerHeuristic::KillerHeuristic() {

    for (int i = 0; i < DEFAULT_MAX_DEPTH; i++) {
        sizes[i] = 0;
    }
}


KillerHeuristic::~KillerHeuristic() {
}

void KillerHeuristic::sort(int ply) {
    //Insertion sort
    for (int i = 1; i < sizes[ply]; i++) {
        int key1 = map[ply][i];
        int key2 = ranks[ply][i];
        int x = i;
        while (x > 0 && key2 > ranks[ply][x - 1]) {
            map[ply][x] = map[ply][x - 1];
            ranks[ply][x] = ranks[ply][x - 1];
            x--;
        }
        map[ply][x] = key1;
        ranks[ply][x] = key2;
    }
}

void KillerHeuristic::add(int ply, int move) {
    ply--; //Convert to 0-based
    if (sizes[ply] > 0) {
        int N = sizes[ply];
        int idx = N;
        for (int i = 0; i < N; i++) {
            if (map[ply][i] == move) {
                idx = i;
                break;
            }
        }
        if (idx == N) {
            map[ply][idx] = move;
            ranks[ply][idx] = 1;
            sizes[ply]++;
        } else {
            ranks[ply][idx]++;
            sort(ply);
        }
    } else {
        map[ply][0] = move;
        ranks[ply][0] = 1;
        sizes[ply] = 1;
    }
}

int KillerHeuristic::getSize(int ply) {
    return sizes[ply - 1];
}

std::vector<int> KillerHeuristic::getMovesAt(int ply) {
    ply--;
    if (sizes[ply] == 0) {
        std::vector<int> empty;
        return empty;
    }
    std::vector<int> moves;
    for (int i = 0; i < sizes[ply]; i++) {
        moves.push_back(map[ply][i]);
    }
    return moves;
}

int KillerHeuristic::getMove(int ply, int rank) {
    if (ply < 1)
        return 0;
    return map[ply - 1][rank];
}

void KillerHeuristic::clear() {
    for (int i = 0; i < DEFAULT_MAX_DEPTH; i++) {
        sizes[i] = 0;
    }
}
