
#include "Move.h"
#include "Constants.h"
#include <cstdlib>
#include <stdio.h>
#include <iostream>

int move::getSide(int move) {

    int side = move >> 24;
    return side == 1 ? WHITE : BLACK;
}

int move::getSource(int move) {
    return move & MASK_6;
}

int move::getDestination(int move) {
    return (move >> 6) & MASK_6;
}

int move::getMovingPiece(int move) {
    int side = getSide(move);
    return side * ((move >> 12) & MASK_4);
}

int move::getCapturedPiece(int move) {
    int side = getSide(move);
    return -side * ((move >> 16) & MASK_3);
}

int move::getType(int move) {
    return (move >> 19)& MASK_3;
}

bool move::isPromotion(int move) {
    int type = getType(move);
    return (type == MOVE_PROMOTION_BISHOP) || (type == MOVE_PROMOTION_ROOK) ||
            (type == MOVE_PROMOTION_KNIGHT) || (type == MOVE_PROMOTION_QUEEN);
}

bool move::isDiscoveredCheck(int move) {

    return ((move >> 23) & MASK_1) > 0;
}

bool move::isCheck(int move) {
    return ((move >> 22) & MASK_1) > 0;
}

std::string move::bin(int n) {
    const int size = sizeof (n)*8;
    std::string res;
    bool s = 0;
    for (int a = 0; a < size; a++) {
        bool bit = n >> (size - 1);
        if (bit)
            s = 1;
        if (s)
            res.push_back(bit + '0');
        n <<= 1;
    }
    if (!res.size())
        res.push_back('0');
   return res;
}

std::string move::toInputNotation(int move) {

    int source = getSource(move);
    int dest = getDestination(move);

    int type = getType(move);

    std::string ans;
    ans.push_back('a' + source % 8);
    ans.push_back('0' + 8 - source / 8);
    ans.push_back('a' + dest % 8);
    ans.push_back('0' + 8 - dest / 8);

    switch (type) {

        case MOVE_PROMOTION_BISHOP:
            ans.push_back('b');
            break;
        case MOVE_PROMOTION_KNIGHT:
            ans.push_back('n');
            break;
        case MOVE_PROMOTION_ROOK:
            ans.push_back('r');
            break;
        case MOVE_PROMOTION_QUEEN:
            ans.push_back('q');
            break;
    }
    return ans;
}

std::string move::toString(int move) {

    std::string ans = "Move: " + bin(move) + "\n" +
            "Side: " + bin(getSide(move)) + "\n" +
            "From: " + bin(getSource(move)) + "\n" +
            "To: " + bin(getDestination(move)) + "\n" +
            "Piece: " + bin(getMovingPiece(move)) + "\n" +
            "Capture: " + bin(getCapturedPiece(move)) + "\n" +
            "Type: " + bin(getType(move)) + "\n" +
            "Check: " + (isCheck(move) ? "1" : "0") + "\n" +
            "Discovered Check: " + (isDiscoveredCheck(move) ? "1" : "0");
    ;
    return ans;
}

int move::construct(int from, int to, int piece, int capture, int type, bool check, bool dCheck) {

    int move = piece > 0 ? 1 : 0;
    
    int s = (check)?1:0;
    int t = (dCheck)?1:0;
    
    
    move = (move << 1) | t;
    move = (move << 1) | s;
    move = (move << 3) | type;
    move = (move << 3) | abs(capture);
    move = (move << 4) | abs(piece);
    move = (move << 6) | to;
    move = (move << 6) | from;
    return move;
}
