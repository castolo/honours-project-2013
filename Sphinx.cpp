/* 
 * File:   Sphinx.cpp
 * Author: Steve James
 * 
 * Created on 11 April 2013, 8:37 AM
 */

#include "Sphinx.h"
#include "TranspositionTable.h"
#include <fstream>
#include <ctime>
#include <sys/time.h>
#include <cstring>
#include <algorithm>
#include <stdio.h>
#include <unistd.h>
#include "Constants.h"
#include "HashBoard.h"
#include "Board.h"
#include "Line.h"
#include "Move.h"

Sphinx::Sphinx(std::string n) {
    openingBook = NULL;
    table = NULL;
    tableBase = NULL;
    sequenceNum = 0;
    thinking = false;
    interrupted = false;
    MAX_DEPTH = DEFAULT_MAX_DEPTH;
    MIN_DEPTH = DEFAULT_MIN_DEPTH;
    board = Board();

    for (int i = 0; i < NUM_OPTIONS; i++) {
        options[i] = true;
    }
    name = n;
}

void Sphinx::setMinDepth(int d) {
    MIN_DEPTH = d;
}

void Sphinx::reset() {
    delete openingBook;
    delete table;
    delete tableBase;
    openingBook = NULL;
    table = NULL;
    tableBase = NULL;
    sequenceNum = 0;
    thinking = false;
    interrupted = false;

    for (int i = 0; i < NUM_OPTIONS; i++) {
        options[i] = true;
    }
}

Sphinx::Sphinx() {

    openingBook = NULL;
    table = NULL;
    tableBase = NULL;
    sequenceNum = 0;
    thinking = false;
    interrupted = false;
    MAX_DEPTH = DEFAULT_MAX_DEPTH;
    MIN_DEPTH = DEFAULT_MIN_DEPTH;
    board = Board();

    for (int i = 0; i < NUM_OPTIONS; i++) {
        options[i] = true;
    }
    name = "Engine";
}

void Sphinx::setPosition(std::string pos) {
    board.setPosition(pos);
}

bool Sphinx::playMove(std::string move) {

    bool flag = false;
    int *moves = board.generatePseudoMoves();
    for (int i = 0; moves[i]; i++) {
        if (move == move::toInputNotation(moves[i])) {
            board.makeMove(moves[i]);
            flag = true;
            break;
        }
    }
    delete[]moves;
    return flag;

}

Sphinx::~Sphinx() {
    sequenceNum = 0;
    delete openingBook;
    delete table;
}

bool Sphinx::isThinking() {
    return thinking;
}

bool Sphinx::isInterrupted() {
    return interrupted;
}

void Sphinx::interrupt() {
    if (thinking) {
        interrupted = true;
    }
}

// <editor-fold defaultstate="collapsed" desc="Normal Alpha-Beta">

int Sphinx::alphaBeta(int ply, int alpha, int beta, PRINCIPAL_LINE* pline, bool nullMovesAllowed) {

    if (isInterrupted() || !isTimeLeft()) {
        interrupted = true;
        return 0;
    }



    bool usePVS = false;
    PRINCIPAL_LINE line;
    line.cmove = 0;
    nodes++;
    if (ply <= 0) {
        pline->cmove = 0;
        return quiescenceSearch(alpha, beta);
    }

    if (board.getRepetitions() >= 2) {
        return -DRAW_VALUE;
    }

    int R = 2;
    if (nullMovesAllowed && !useLastMove) {
        if (board.canMakeNullMove()) {
            board.makeMove(NULL_MOVE); //board.swapSides();
            int score = -alphaBeta(ply - R - 1, -beta, -beta + 1, &line, false);
            board.undoMove(NULL_MOVE); // board.swapSides();
            if (score >= beta) {
                return score;
            }
        }
    }
    int* moves = board.generatePseudoMoves();
    rankMoves(moves, ply);
    bool legalMove = false;
    int eval;
    for (int i = 0; int move = moves[i]; i++) {
        if ((abs(move::getCapturedPiece(move)) != KING)) {
            int reps = 0;
            if (board.makeMove(move)) {
                legalMove = true;

                if (ply >= depth - 2) {
                    reps = board.getRepetitions();
                }
                if (reps < 2) {
                    if (usePVS) {
                        eval = -alphaBeta(ply - 1, -alpha - 1, -alpha,
                                &line, true);
                        if (eval > alpha && eval < beta) {
                            eval = -alphaBeta(ply - 1, -beta, -alpha,
                                    &line, true);
                        }
                    } else {
                        eval = -alphaBeta(ply - 1, -beta, -alpha, &line, true);
                    }
                } else {
                    eval = -DRAW_VALUE;
                }
                board.undoMove(move);
                if (eval >= beta) {
                    killerHeuristic.add(ply, move);
                    useLastMove = false;
                    delete[]moves;
                    return beta;
                }
                if (eval > alpha) {
                    useLastMove = false;
                    usePVS = true;
                    pline->argmove[0] = move;
                    memcpy(pline->argmove + 1, line.argmove, line.cmove * sizeof (int));
                    pline->cmove = line.cmove + 1;
                    alpha = eval;
                }
            } else {
                board.undoMove(move);
            }
        }
    }
    delete[]moves;
    if (!legalMove) {
        return completionCheck(ply);
    }
    return alpha;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Alpha-beta with TT">

int Sphinx::alphaBeta(int ply, int alpha, int beta, bool nullMovesAllowed) {

    if (isInterrupted() || !isTimeLeft()) {
        interrupted = true;
        return 0;
    }
    nodes++;


    if (board.getRepetitions() >= 2) {
        return -DRAW_VALUE;
    }
    HashEntry entry = table->get(board.getHash(), board.getLock(), sequenceNum);
    if ((entry.hash != 0) && (entry.depth >= ply)) {
        switch (entry.flag) {
            case EVALTYPE_EXACT:
                return entry.eval;
            case EVALTYPE_UPPERBOUND:
                if (entry.eval <= alpha) {
                    return alpha;
                }
                break;
            case EVALTYPE_LOWERBOUND:
                if (entry.eval >= beta) {
                    return beta;
                }
        }
    }

    if (ply <= 0) {
        int eval = quiescenceSearch(alpha, beta);
        if (eval >= beta) {
            table->put(board.getHash(), board.getLock(), NULL_MOVE, eval, 0, EVALTYPE_LOWERBOUND, sequenceNum);
        } else if (eval <= alpha) {
            table->put(board.getHash(), board.getLock(), NULL_MOVE, eval, 0, EVALTYPE_UPPERBOUND, sequenceNum);
        } else {
            table->put(board.getHash(), board.getLock(), NULL_MOVE, eval, 0, EVALTYPE_EXACT, sequenceNum);
        }
        return eval;
    }


    int R = 2;
    if (nullMovesAllowed && !useLastMove) {
        if (board.canMakeNullMove()) {
            board.makeMove(NULL_MOVE); //board.swapSides();
            int score = -alphaBeta(ply - R - 1, -beta, -beta + 1, false);
            board.undoMove(NULL_MOVE); // board.swapSides();
            if (score >= beta) {
                if (!isInterrupted()) {
                    table->put(board.getHash(), board.getLock(), NULL_MOVE, score, ply, EVALTYPE_LOWERBOUND, sequenceNum);
                }
                return score;
            }
        }
    }
    int* moves = board.generatePseudoMoves();
    bool legalMove = false;
    int bestScore = -INFTY;
    int bestMove = 0;
    int flag = EVALTYPE_UPPERBOUND;
    rankMoves(moves, ply);
    int eval;
    for (int i = 0; int move = moves[i]; i++) {
        if ((abs(move::getCapturedPiece(move)) != KING)) {
            if (board.makeMove(move)) {
                legalMove = true;

                int reps = 0;
                if (ply >= depth - 2) {
                    reps = board.getRepetitions();
                }
                if (reps >= 2) {
                    eval = -DRAW_VALUE;
                } else {
                    if (flag == EVALTYPE_EXACT) {
                        //Principal variation search                    
                        eval = -alphaBeta(ply - 1, -alpha - 1, -alpha,
                                true);
                        //If we find a line better than PVS, redo the search:
                        if (eval > alpha && eval < beta) {
                            eval = -alphaBeta(ply - 1, -beta, -alpha,
                                    true);
                        }
                    } else {
                        eval = -alphaBeta(ply - 1, -beta, -alpha,
                                true);
                    }
                }
                board.undoMove(move);
                if (isInterrupted()) {
                    delete[] moves;
                    return 0;
                }
                if (eval > bestScore) {
                    if (eval >= beta) {
                        table->put(board.getHash(), board.getLock(), move, eval, ply, EVALTYPE_LOWERBOUND, sequenceNum);
                        killerHeuristic.add(ply, move);
                        useLastMove = false;
                        delete[] moves;
                        return beta;
                    }
                    bestScore = eval;
                    bestMove = move;
                    if (eval > alpha) {
                        flag = EVALTYPE_EXACT;
                        useLastMove = false;
                        alpha = eval;
                    }
                }
                table->put(board.getHash(), board.getLock(), bestMove, bestScore, ply, flag, sequenceNum);
            } else {
                board.undoMove(move);
            }
        }
    }
    delete[]moves;
    if (!legalMove) {
        return completionCheck(ply);
    }
    table->put(board.getHash(), board.getLock(), bestMove, bestScore, ply, flag, sequenceNum);
    return alpha;
}
// </editor-fold>

bool Sphinx::isTimeLeft() {
    timeval now;
    gettimeofday(&now, NULL);
    long int secs = now.tv_sec - searchStarted.tv_sec;
    long int usecs = now.tv_usec - searchStarted.tv_usec;
    long int mtime = (long int) (((secs) * 1000 + usecs / 1000.0) + 0.5);
    if (mtime < timeAllowed) {
        return true;
    }
    //Allow for extra time if no move has yet been found
    if (previousLine.getBestMove() <= 0) {
        return true;
    }
    if (previousLine.getDepth() < MIN_DEPTH) {
        return true;
    }
    return false;
}

int Sphinx::isSimpleEndgame() {
    int side = board.isKRKEnding();
    if (side != 0) {
        return side;
    }
    return board.isKQKEnding();
}

Line Sphinx::search(long int timeLeft) {

    if (options[OPTION_TRANSPOSITION_TABLE] && !table) {
        table = new TranspositionTable();
    }

    if (options[OPTION_OPENING_BOOK] && !openingBook) {
        openingBook = new OpeningBook();
        openingBook->read();
    }

    timeval begin, end;
    long int mtime = 0;
    thinking = true;
    interrupted = false;
    timeAllowed = calculateTime(timeLeft);
    sequenceNum++;
    nodes = 0;
    gettimeofday(&begin, NULL);
    gettimeofday(&searchStarted, NULL);
    Line ans;
    useLastMove = false;
    int alpha = -INFTY;
    int beta = INFTY;
    killerHeuristic.clear();
    int score = 0;

    // <editor-fold defaultstate="collapsed" desc="Endgame Tablebase">

    /**************************
     *  Try Endgame Tablebase *
     **************************/
    if (options[OPTION_ENDGAME_TABLEBASE]) {
        int side = isSimpleEndgame();
        if (side != 0) {
            if (!tableBase) {
                tableBase = new EndgameTable();
                tableBase->read();
            }
            Line line = tableBase->probe(&board, side);
            if ((line.getBestMove() > 0) && (line.getDepth() > 0)) {
                if (options[OPTION_VERBOSE]) {
                    std::cout << line.toString() << std::endl;
                }
                return line;
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Opening  Book">

    /**********************
     *  Try Opening Book  *
     **********************/

    if (options[OPTION_OPENING_BOOK] && openingBook) {
        OpeningMove omove = openingBook->getMove(&board);
        bool found = false;
        Line line;
        std::vector<int> moves;
        while (omove.getMove() != "") {
            found = true;
            int finalMove = -1;
            int * actualMoves = board.generatePseudoMoves();
            for (int j = 0; int move = actualMoves[j]; j++) {
                if (move::toInputNotation(move) == omove.getMove()) {
                    finalMove = move;
                    break;
                }
            }
            delete[] actualMoves;
            if (finalMove != -1) {
                moves.push_back(finalMove);
                board.makeMove(finalMove);
            } else {
                break;
            }
            omove = openingBook->getMove(&board);
        }
        for (int i = moves.size() - 1; i >= 0; i--) {
            board.undoMove(moves[i]);
        }
        if (found) {
            line = Line(moves, 0, moves.size(), 0, 0);
            if (options[OPTION_VERBOSE]) {
                std::cout << line.toString() << std::endl;
            }
            thinking = false;
            return line;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Sanity Check">
    /******************************************
     * Sanity Check - if only 1 move, play it *
     ******************************************/
    int * moves = board.generatePseudoMoves();
    int count = 0;
    int only = 0;
    for (int i = 0; int move = moves[i]; i++) {
        if ((abs(move::getCapturedPiece(move)) != KING)) {
            if (board.makeMove(move)) {
                only = move;
                count++;
            }
            board.undoMove(move);
            if (count > 1) {
                break;
            }
        }
    }
    delete[] moves;
    if (count == 1) {
        std::vector<int> temp;
        temp.push_back(only);
        Line single = Line(temp, 0, 1, 1, 0);
        return single;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Iterative Deepening">
    /**********************
     * Iterative deepening*
     **********************/
    int maxSpin = 50;
    int spin = 0;
    for (depth = 1; ((depth <= MIN_DEPTH) && (spin < maxSpin)) || ((abs(score) < MATE_VALUE) && (mtime < timeAllowed) && (depth <= MAX_DEPTH)); depth++) {
        spin++;
        PRINCIPAL_LINE line;
        line.cmove = 0;
        if (options[OPTION_TRANSPOSITION_TABLE]) {
            score = alphaBeta(depth, alpha, beta, false);
        } else {
            score = alphaBeta(depth, alpha, beta, &line, false);
        }
        if (isInterrupted()) {
            break;
        }
        useLastMove = true;
        if (score <= alpha || score >= beta) {
            //Fell out of aspiration window => redo search
            alpha = -INFTY;
            beta = INFTY;
            depth--;
        } else {
            gettimeofday(&end, NULL);
            long int secs = end.tv_sec - begin.tv_sec;
            long int usecs = end.tv_usec - begin.tv_usec;
            mtime = (long int) (((secs) * 1000 + usecs / 1000.0) + 0.5);

            std::vector<int> moves;
            if (options[OPTION_TRANSPOSITION_TABLE]) {
                moves = table->extractPrincipalVariation(&board, depth);
            } else {
                for (int i = 0; i < line.cmove; i++) {
                    moves.push_back(line.argmove[i]);
                }
            }
            if (ans.getDepth() > 0) {
                previousLine = ans;
            }

            ans = Line(moves, score, depth, nodes, mtime);
            if (options[OPTION_VERBOSE]) {
                std::cout << ans.toString() << std::endl;
            }
            /**********************
             * Aspiration Windows *
             **********************/
            alpha = score - ASPIRATION_WINDOW_SIZE;
            beta = score + ASPIRATION_WINDOW_SIZE;
        }
    }
    // </editor-fold>

    thinking = false;
    return ans;
}

int Sphinx::evaluatePosition() {
    return board.evaluate();
}

int Sphinx::completionCheck(int ply) {

    uint64_t king = board.getKing();
    board.swapSides();
    if (board.attacks(king)) {
        board.swapSides();
        return -(MATE_VALUE + ply);
    } else {
        board.swapSides();
        return -(DRAW_VALUE); // + CONTEMPT_VALUE);
    }
}

int Sphinx::quiescenceSearch(int alpha, int beta) {
    nodes++;
    int* moves = getNoisyMoves();
    int eval;
    if (moves[0] == NULL_MOVE) {
        eval = completionCheck(0); //checkmate in quiescence
    } else {
        eval = evaluatePosition();
    }
    if (eval >= beta) {
        delete[]moves;
        return beta;
    }
    if (eval > alpha) {
        alpha = eval;
    }

    for (int i = 0; int move = moves[i]; i++) {
        if (move != NULL_MOVE && abs(move::getCapturedPiece(move)) != KING) {
            if (board.makeMove(move)) {
                eval = -quiescenceSearch(-beta, -alpha);
                board.undoMove(move);
                if (eval >= beta) {
                    delete[]moves;
                    return beta;
                }
                if (eval > alpha) {
                    alpha = eval;
                }
            } else {
                board.undoMove(move);
            }
        }
    }
    delete[]moves;
    return alpha;
}

int* Sphinx::getNoisyMoves() {
    int* moves = board.generatePseudoMoves();
    int i;
    int count = 0;
    int legal = -1;
    for (i = 0; int move = moves[i]; i++) {
        if (count < 2) {
            if (board.makeMove(move)) {
                count++;
                legal = move;
            }
            board.undoMove(move);
        }
        bool A = move::isDiscoveredCheck(move);
        bool B = move::isCheck(move);
        bool C = move::getCapturedPiece(move) == EMPTY_SQUARE;
        if (!A && !B && C) {
            moves[i] = 0;
        }
    }
    if (count == 1) {
        moves[0] = legal;
    } else if (count == 0) {
        moves[0] = NULL_MOVE;
    }
    quicksort(moves, 0, i);
    return moves;
}

// <editor-fold defaultstate="collapsed" desc="Quicksort for noisy moves">

bool Sphinx::compare(int a, int b) {
    if (b == 0) {
        return true;
    }
    if (a == 0) {
        return false;
    }
    bool aCheck = move::isCheck(a);
    bool bCheck = move::isCheck(b);

    bool aDisc = move::isDiscoveredCheck(a);
    bool bDisc = move::isDiscoveredCheck(b);

    bool aDouble = aCheck && aDisc;
    bool bDouble = bCheck && bDisc;

    if (aDouble && (!bDouble)) {
        return true;
    }

    if (bDouble && (!aDouble)) {
        return false;
    }

    if (aDisc && (!bDisc)) {
        return true;
    }
    if ((!aDisc) && bDisc) {
        return false;
    }

    if (aCheck && (!bCheck)) {
        return true;
    }
    if ((!aCheck) && bCheck) {
        return false;
    }

    int aCp = abs(move::getCapturedPiece(a));
    int bCp = abs(move::getCapturedPiece(b));

    if (aCp > bCp) {
        return true;
    }
    if (aCp < bCp) {
        return false;
    }
    int aMp = abs(move::getMovingPiece(a));
    int bMp = abs(move::getMovingPiece(b));

    if (aMp > bMp) {
        return true;
    }
    if (aMp < bMp) {
        return false;
    }
    return true;
}

int Sphinx::partition(int* A, int p, int r) {
    int x = A[r];
    int i = p - 1;
    for (int j = p; j < r; j++) {
        if (compare(A[j], x)) {
            i++;
            int temp = A[i];
            A[i] = A[j];
            A[j] = temp;
        }
    }
    int temp = A[i + 1];
    A[i + 1] = A[r];
    A[r] = temp;
    return i + 1;
}

void Sphinx::quicksort(int* A, int p, int r) {
    if (p < r) {
        int q = partition(A, p, r);
        quicksort(A, p, q - 1);
        quicksort(A, q + 1, r);
    }
}

// </editor-fold>

void Sphinx::rankMoves(int* moves, int ply) {

    std::vector<int> tableMoves;
    std::vector<int> history;
    std::vector<int> primaryKillers;
    std::vector<int> secondaryKillers;
    std::vector<int> captures;
    std::vector<int> promotion;
    std::vector<int> discovered;
    std::vector<int> doubleCheck;
    std::vector<int> check;
    std::vector<int> pawn;
    std::vector<int> castle;
    std::vector<int> rest;

    int last = -1;
    if (useLastMove && (ply > 1) && (previousLine.getDepth() > 0)) {

        if (previousLine.getMoves().size() > depth - ply) {
            last = previousLine.getMoves()[depth - ply];
        }
    }

    int i;

    for (i = 0; int move = moves[i]; i++) {
        bool added = false;
        HashEntry entry;
        if (options[OPTION_TRANSPOSITION_TABLE] && table) {
            entry = table->get(board.getHash(), board.getLock());
        } else {
            entry = NULL_ENTRY;
        }
        if (move == last) {
            history.push_back(move);
            added = true;
        } else if ((entry.lock != 0) && (entry.move > 0) && (move == entry.move)) {
            tableMoves.push_back(move);
            added = true;
        } else if (killerHeuristic.getSize(ply) > 0) {
            if (move == killerHeuristic.getMove(ply, 0)) {
                primaryKillers.push_back(move);
                added = true;
            } else if ((killerHeuristic.getSize(ply) > 1) &&
                    (move == killerHeuristic.getMove(ply, 1))) {
                secondaryKillers.push_back(move);
                added = true;
            }
        }
        if (!added) {
            bool c = move::isCheck(move);
            bool d = move::isDiscoveredCheck(move);
            switch (move::getType(move)) {

                case MOVE_SHORT_CASTLE:
                case MOVE_LONG_CASTLE:
                    castle.push_back(move);
                    break;
                case MOVE_NORMAL:
                    if (d && c) {
                        doubleCheck.push_back(move);
                    } else if (d) {
                        discovered.push_back(move);
                    } else if (move::getCapturedPiece(move) != EMPTY_SQUARE) {
                        captures.push_back(move);
                    } else if (c) {
                        check.push_back(move);
                    } else if (abs(move::getMovingPiece(move)) == PAWN) {
                        pawn.push_back(move);
                    } else {
                        rest.push_back(move);
                    }
                    break;
                case MOVE_CAPTURE_EN_PASSANT:
                    if (d) {
                        discovered.push_back(move);
                    } else {
                        captures.push_back(move);
                    }
                    break;
                case MOVE_PROMOTION_KNIGHT:
                case MOVE_PROMOTION_BISHOP:
                case MOVE_PROMOTION_ROOK:
                case MOVE_PROMOTION_QUEEN:
                    promotion.push_back(move);
                    break;
            }
        }

    }
    int H = history.size();
    int T = tableMoves.size();
    int K = primaryKillers.size();
    int S = secondaryKillers.size();
    int P = promotion.size();
    int C = captures.size();
    int c = check.size();
    int D = discovered.size();
    int DD = doubleCheck.size();
    int p = pawn.size();
    int CA = castle.size();

    std::copy(history.begin(), history.end(), moves);
    std::copy(tableMoves.begin(), tableMoves.end(), moves + H);
    std::copy(primaryKillers.begin(), primaryKillers.end(), moves + H + T);
    std::copy(secondaryKillers.begin(), secondaryKillers.end(), moves + H + T + K);
    std::copy(promotion.begin(), promotion.end(), moves + H + T + K + S);
    std::copy(doubleCheck.begin(), doubleCheck.end(), moves + H + T + K + S + P);
    std::copy(discovered.begin(), discovered.end(), moves + H + T + K + S + P + DD);
    std::copy(captures.begin(), captures.end(), moves + H + T + K + S + P + DD + D);
    std::copy(check.begin(), check.end(), moves + H + T + K + S + P + DD + D + C);
    std::copy(castle.begin(), castle.end(), moves + H + T + K + S + P + DD + D + C + c);
    std::copy(pawn.begin(), pawn.end(), moves + H + T + K + S + P + DD + D + C + c + CA);
    std::copy(rest.begin(), rest.end(), moves + H + T + K + S + P + DD + D + C + c + CA + p);
}




//TODO calculate optimal time controls

long int Sphinx::calculateTime(long int time) {
    //  int moves = board.getMoves();
    return time / 30;
}


