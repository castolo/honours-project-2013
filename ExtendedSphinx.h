/* 
 * File:   ExtendedSphinx.h
 * Author: Workstation
 *
 * Created on 10 August 2013, 5:30 PM
 */

#ifndef EXTENDEDSPHINX_H
#define	EXTENDEDSPHINX_H

#include "Sphinx.h"
#include "FastNeuralNetwork.h"
#include <string>
#include <vector>

class ExtendedSphinx : public Sphinx {
public:
    ExtendedSphinx(int);
    ExtendedSphinx(std::string, int);
    void setEvaluationParameters(const std::vector<double>&);
    void setAllParameters(const std::vector<double>&);
    void read(std::string);
    void write(std::string);
    virtual ~ExtendedSphinx();
    std::string getName() const;
    int getGeneration() const;
    std::string getData() const;
    std::vector<double> getParameters();
    void setGeneration(int gen) {
        generation = gen;
    }
    
    void setNetworkWeight(int weight){
        networkWeight = weight;
    }

    void setName(std::string n) {
        name = n;
    }

    void setParents(int a, int b) {
        parent1 = a;
        parent2 = b;
    }

private:


    int networkWeight;
    int generation;
    int parent1;
    int parent2;
    std::string name;
    FastNeuralNetwork *network;
    int evaluatePosition();

};

#endif	/* EXTENDEDSPHINX_H */

