/* 
 * File:   KillerHeuristic.h
 * Author: Steve James
 *
 * Created on 11 April 2013, 8:38 AM
 */

#ifndef KILLERHEURISTIC_H
#define	KILLERHEURISTIC_H

#include "Constants.h"
#include <vector>
#include <algorithm> 

class KillerHeuristic {
public:


    /**
     * Creates a new data structure for implementing the killer move heuristic.
     */
    KillerHeuristic();
    virtual ~KillerHeuristic();

    /**
     * Adds a killer move to the data structure, inserting it into the correct
     * position in the list (as explained in the class description)
     *
     * @param ply the ply the move occurred at
     * @param move the killer move
     */
    void add(int ply, int move);

    /**
     * Returns the number of killer moves that have been found at a particular
     * ply
     *
     * @param ply the ply to check for killer moves
     * @return the number of killer moves found
     */
    int getSize(int ply);

    /**
     * Returns all killer moves for a particular ply
     *
     * @param ply the ply at which the killer moves occurred
     * @return a list of killer moves at a particular ply
     */
    std::vector<int> getMovesAt(int ply);

    /**
     * Returns a killer move for a particular ply, at a particular position in
     * the list
     *
     * @param ply the ply at which the killer move occurred
     * @param rank the position of the move in the list
     * @return a killer move at a particular ply and list position
     */
    int getMove(int ply, int rank);

    /**
     * Clears the entire data structure
     */
    void clear();

private:

    static const int MAX_KILLERS = 7; // insertion sort works well for +- 7 items

    int map[DEFAULT_MAX_DEPTH][MAX_KILLERS];
    int ranks [DEFAULT_MAX_DEPTH][MAX_KILLERS];
    int sizes[DEFAULT_MAX_DEPTH];

    void sort(int);
};

#endif	/* KILLERHEURISTIC_H */

