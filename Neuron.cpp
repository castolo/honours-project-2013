/* 
 * File:   Neuron.cpp
 * Author: Workstation
 * 
 * Created on 15 July 2013, 5:09 PM
 */

#include "Neuron.h"
#include "Constants.h"

#include <stdlib.h>

unsigned int Neuron::seed = time(NULL);

Neuron::Neuron(int N) {
    inputs = N + 1; //+1 for bias
    weights.clear();
    for (int i = 0; i < inputs; i++) {
        double v = 2 * ((double) random(&seed) / (double) 2147483647) - 1; //weights between (-1,1)
        weights.push_back(v);
    }

}

Neuron::~Neuron() {
}

