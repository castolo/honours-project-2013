/* 
 * File:   Layer.cpp
 * Author: Workstation
 * 
 * Created on 19 July 2013, 4:30 PM
 */

#include "NeuralLayer.h"



NeuralLayer::NeuralLayer(int N,int M) {
    numNeurons = N;
    for (int i=0;i<N;i++){
        neurons.push_back(Neuron(M));
    }
}

void NeuralLayer::setWeight(int neuron, int weight, double val)
{
    neurons[neuron].setWeight(weight,val);
}

NeuralLayer::~NeuralLayer() {
}

