/* 
 * File:   NeuralNetwork.cpp
 * Author: Workstation
 * 
 * Created on 19 July 2013, 4:33 PM
 */

#include "NeuralNetwork.h"
#include "Constants.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>

NeuralNetwork::NeuralNetwork() {
    inputs = outputs = hiddenLayers = 0;
    layers.clear();
}

NeuralNetwork::NeuralNetwork(int numLayers, ...) {

    layers.clear();
    this->hiddenLayers = numLayers - 2;
    va_list args; // A place to store the list of arguments
    int previous;
    va_start(args, numLayers); // Initializing arguments to store all values after num
    for (int i = 0; i < numLayers; i++) {
        int neurons = va_arg(args, int);
        if (i == 0) {
            this->inputs = neurons;
            previous = neurons;
        } else if (i == numLayers - 1) {
            this->outputs = neurons;
            layers.push_back(NeuralLayer(outputs, previous)); // output layer
        } else {
            layers.push_back(NeuralLayer(neurons, previous)); //hidden layers
            previous = neurons;
        }
    }
    va_end(args);
}

NeuralNetwork::~NeuralNetwork() {
}

std::vector<double> NeuralNetwork::activate(const Board& board) const {

    const int king[6] = {1, 0, 0, 0, 0, 0};
    const int queen[6] = {0, 1, 0, 0, 0, 0};
    const int rook[6] = {0, 0, 1, 0, 0, 0};
    const int bishop[6] = {0, 0, 0, 1, 0, 0};
    const int knight[6] = {0, 0, 0, 0, 1, 0};
    const int pawn[6] = {0, 0, 0, 0, 0, 1};
    const int empty[6] = {0,0,0,0,0,0};
    std::vector<double> inputs, outputs;
    
    inputs.reserve(this->inputs);
        
    for (int i = 0; i < 64; i++) {

        int piece = board[i];
        int side;
        if (piece > 0) {
            side = 1;
        } else if (piece < 0) {
            side = -1;
        } else {
            side = 0;
        }

       const int *array;

        switch (abs(piece)) {
            case KING:
                array = king;
                break;
            case QUEEN:
                array = queen;
                break;
            case ROOK:
                array = rook;
                break;
            case BISHOP:
                array = bishop;
                break;
            case KNIGHT:
                array = knight;
                break;
            case PAWN:
                array = pawn;
                break;
            default:
                array = empty;
                break;
        }
        for (int j = 0; j < 6; j++) {
            inputs.push_back(array[j] * side);
        }
    }
    outputs = activate(inputs);
    return outputs;
}

std::vector<double> NeuralNetwork::activate(std::vector<double> inputs) const {

    std::vector<double> outputs;
    if (inputs.size() != this->inputs) {
        std::cerr << "Incorrect input dimensions: Should be " << this->inputs << ", but found " << inputs.size() << std::endl;
        return outputs;
    }
    for (int i = 0; i < hiddenLayers + 1; i++) {
        if (i > 0) {
            inputs = outputs;
        }
        outputs.clear();
        int J = layers[i].getCount();
        for (int j = 0; j < J; j++) {
            double sum = 0;
            Neuron neuron = layers[i].getNeurons()[j];
            int K = neuron.getNumberInputs();
            int count = 0;
            for (int k = 0; k < K - 1; k++) {
                sum += neuron.getWeights()[k] * inputs[count];
                count++;
            }
            sum += neuron.getWeights()[K - 1] * BIAS;
            double out = activationFunction(sum);
            outputs.push_back(out);
        }
    }
    return outputs;
}

void NeuralNetwork::update(const std::vector<double> &weights) {

    if (weights.size() != getNumberOfWeights()) {
        std::cerr << "Error: dimensions do not match. Found " << weights.size() << ", but expected " <<
                getNumberOfWeights() << std::endl;
    } else {

        int idx = 0;
        for (int i = 0; i < layers.size(); i++) {
            for (int j = 0; j < layers[i].getCount(); j++) {
                for (int k = 0; k < layers[i].getNeurons()[j].getNumberInputs(); k++) {
                    layers[i].setWeight(j, k, weights[idx++]);

                }
            }
        }

    }

}

int NeuralNetwork::getNumberOfWeights() const {
    int x = 0;
    for (int i = 0; i < layers.size(); i++) {
        for (int j = 0; j < layers[i].getCount(); j++) {
            x += layers[i].getNeurons()[j].getNumberInputs();
        }
    }
    return x;

}

std::vector<double> NeuralNetwork::getWeights() const {
    std::vector<double> weights;
    for (int i = 0; i < layers.size(); i++) {
        for (int j = 0; j < layers[i].getCount(); j++) {
            for (int k = 0; k < layers[i].getNeurons()[j].getNumberInputs(); k++) {
                weights.push_back(layers[i].getNeurons()[j].getWeights()[k]);
            }
        }
    }
    return weights;
}

double NeuralNetwork::activationFunction(double input) const {
    //Sigmoid
    double out = 1 / (1 + exp(-input));
    //Range? [-5,5] ??????
    return out;
}

void NeuralNetwork::show() const {

    using namespace std;
    cout << "Input layer: " << inputs << " neurons\n\n";
    for (int i = 0; i < layers.size() - 1; i++) {
        cout << "Hidden Layer " << i + 1 << ": ";
        for (int j = 0; j < layers[i].getCount(); j++) {
            cout << "[";
            for (int k = 0; k < layers[i].getNeurons()[j].getNumberInputs(); k++) {
                cout << layers[i].getNeurons()[j].getWeights()[k];

                if (k != layers[i].getNeurons()[j].getNumberInputs() - 1) {
                    cout << ", ";
                }
            }
            cout << "] ";
        }
        cout << "\n\n";
    }
    cout << "Output layer: ";
    int i = layers.size() - 1;
    for (int j = 0; j < layers[i].getCount(); j++) {
        cout << "[";
        for (int k = 0; k < layers[i].getNeurons()[j].getNumberInputs(); k++) {
            cout << layers[i].getNeurons()[j].getWeights()[k];
            if (k != layers[i].getNeurons()[j].getNumberInputs() - 1) {
                cout << ", ";
            }
        }
        cout << "] ";
    }
    cout << endl << endl;
}
