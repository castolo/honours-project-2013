/* 
 * File:   TranspositionTable.h
 * Author: Steve James
 *
 * Created on 19 April 2013, 11:19 AM
 */

#ifndef TRANSPOSITIONTABLE_H
#define	TRANSPOSITIONTABLE_H

#include <unordered_map>
#include <stdint.h>
#include <vector>
#include <cstddef>
#include <math.h>
#include <stdlib.h>
#include <set>
#include "Board.h"

struct HashEntry {
    int move;
    int eval;
    int depth;
    int flag;
    int ancient;
    uint64_t hash;
    uint64_t lock;

    bool operator==(const HashEntry & other) const {
        return (move == other.move) && (eval == other.eval) &&
                (depth == other.depth) && (flag == other.flag) &&
                (ancient == other.ancient);
    }

    bool operator!=(const HashEntry & other) const {
        return !(*this == other);
    }

};



static struct HashEntry NULL_ENTRY = {0, 0, 0, 0, 0, 0ull, 0ull};

class TranspositionTable {
public:

    /**
     * The maximum number of elements in allowed in the table
     */
    static const int SIZE = 16777216 / 2;
    /**
     * The maximum number of iterations an entry may reside in the table before
     * becoming obsolete
     */
    static const short ANCIENT_DIFFERENCE = 1;
    /**
     * A multi-map class backed by a {@link ConcurrentHashMap} used to store board
     * positions and their corresponding entries. Normally, a regular
     * {@link HashMap} would be sufficient, but a synchronised map is required to
     * allow users to save a game while the table is being populated. If no
     * synchronisation is used, users would only be able to save a game when the
     * engine is not deciding on a move. Fortunately, {@link ConcurrentHashMap} has
     * undergone an overhaul and as such is not significantly slower than its non
     * thread-safe counterpart. Board positions are represented by mapping the
     * current position to a 64-bit integer. Also see {@link HashEntry}
     *
     * @author Steve James
     * @since 22/03/2013
     */
    TranspositionTable();
    virtual ~TranspositionTable();
    /**
     * Removes all the mappings from the table
     */
    void clear();
    /**
     * Returns the value to which the specified hashes are mapped, or
     * {@code null} if this map contains no mapping for the hash.
     *
     * @param hash the board's position
     * @param lock the board's position, according to the second hash
     */
    HashEntry get(const uint64_t, const uint64_t);
    /**
     * Returns the value to which the specified hashes are mapped, or
     * {@code null} if this map contains no mapping for the hash. {@code null}
     * is also returned if the table does contain an element, but its
     * {@code ancient} variable differs from {@code sequence} by more than the
     * value of {@link TranspositionTable#ANCIENT_DIFFERENCE}
     *
     * @param hash the board's position
     * @param lock the board's position, according to the second hash
     * @param sequence the current sequence number
     * @return
     */
    HashEntry get(uint64_t, uint64_t, int);
    /**
     * Maps the specified hash to the specified values in the table, provided
     * that certain conditions are met. A depth-first replacement scheme is used
     * here. This means that the entry is added to the table in any of the
     * following cases: <ul><li>There is currently no mapping for the give
     * hash</li><li> The depth to which the position has been analysed is
     * greater or equal to the current element's depth</li> <li>The current
     * element's {@code ancient} variable has rendered it obsolete</li></ul>
     *
     * @param hash the board's position
     * @param lock the board's position, according to the second hash
     * @param move the move to be played in said position
     * @param eval the score assigned to the move
     * @param depth the depth to which the position was analysed
     * @param flag the type of node to which the ecore corresponds (see {@link Constants#EVALTYPE_EXACT},
     * {@link Constants#EVALTYPE_LOWERBOUND} and
     * {@link Constants#EVALTYPE_UPPERBOUND})
     * @param ancient the iteration at which the entry is being added to the
     * table
     */
    void put(uint64_t, uint64_t, int, int, int, int, int);
    /**
     * Returns the number of key-value mappings in this table. If the table
     * contains more than <tt>Integer.MAX_VALUE</tt> elements, returns
     * <tt>Integer.MAX_VALUE</tt>.
     *
     * @return the number of key-value mappings in this table
     */
    unsigned int size();

    /**
     * Extracts the principal variation from the transposition table. Note that the PV may be
     * incomplete owing to hash table overwrites. In this case, a PV with smaller size than the prescribed 
     * depth may be returned. The PV will however contain at least one move - the first move.
     * @param board the board
     * @param depth the size of the expected principal variation
     * @return the moves that constitute the principal variation
     */
    std::vector<int> extractPrincipalVariation(Board*, int);


private:

    int hashCode(uint64_t value) {
        return (int) ((value ^ (value >> 32)) % SIZE);

    }

    int hashCode(uint64_t value, int i) {
        return (int) (((value ^ (value >> 32))+(i + i * i) / 2) % SIZE);
    }

    //HashEntry* map;
    std::vector<HashEntry> map;
};




#endif	/* TRANSPOSITIONTABLE_H */
