/* 
 * File:   Move.h
 * Author: Workstation
 *
 * Created on April 11, 2013, 5:32 PM
 */

/**
 *  ______________________________________________________________________
 * |                                                                      |     
 * |side (1) dCheck(1) check(1) type(3) capture(3) piece(4) to(6) from(6) |  = 25 bits                                        |
 * |______________________________________________________________________| 
 */
//side: white=1, black =0


#ifndef MOVE_H
#define	MOVE_H

#include <string>
#include <cstdlib>

namespace move {

    /**
     * Encodes the move parameters in a 32-bit integer
     * @param from the source square
     * @param to the destination square
     * @param piece the moving piece
     * @param capture the piece captured (or empty square, if none was)
     * @param type the type of move (8 options - see @link{Constants.h})
     * @param check if the move caused check
     * @param dCheck if the move caused a discovered check
     * @return the encoded integer
     */
    int construct(int from, int to, int piece, int capture, int type, bool check, bool dCheck);

    int getSource(int);
    int getSide(int);
    int getDestination(int);
    int getCapturedPiece(int);
    int getType(int);
    int getMovingPiece(int);
    bool isCheck(int);
    bool isDiscoveredCheck(int);
    bool isPromotion(int);
    std::string bin(int);
    std::string toString(int);
    std::string toInputNotation(int);


    static const int MASK_6 = 63;
    static const int MASK_4 = 15;
    static const int MASK_3 = 7;
    static const int MASK_1 = 1;
}

#endif	/* MOVE_H */

