/* 
 * File:   Line.cpp
 * Author: Workstation
 * 
 * Created on 19 April 2013, 11:19 AM
 */

#include "Line.h"
#include "Move.h"

#include <stdio.h>
#include <sstream>
#include <iostream>

Line::Line() {
    evaluation = 0;
    depth = 0;
    nodes = 0;
    time = 0;

}

Line::Line(std::vector<int> moves, int eval, int dep, long int nod, long int t) {

    line = moves;
    evaluation = eval;
    depth = dep;
    nodes = nod;
    time = t;

}

Line::Line(const Line& orig) {

    line = orig.line;
    evaluation = orig.evaluation;
    depth = orig.depth;
    nodes = orig.nodes;
    time = orig.time;

}

Line::~Line() {
}

std::string Line::toString() {
    std::ostringstream ss;

    float t = time / 1000.0;
    long long int nps = (t==0)?0:nodes / t;
    ss << "info depth " << depth << " score cp " << evaluation << " time " << (time) <<
            " nodes " << nodes << " nps " << nps << " pv";
    for (int i = 0; i < line.size(); i++) {
        ss << " " << move::toInputNotation(line[i]);
    }
    return ss.str();
}
