#include "Parallel.h"
#include <string.h>
#include <iostream>

void mpi::open(int argc, char** argv) {
#ifdef MPI_VERSION
    MPI::Init(argc, argv);
#endif
}

int mpi::getRank() {
#ifdef MPI_VERSION
    return MPI::COMM_WORLD.Get_rank();
#endif
    return 0;
}

int mpi::getSize() {
#ifdef MPI_VERSION
    return MPI::COMM_WORLD.Get_size();
#endif
    return 1;
}

std::string mpi::getName() {

#ifdef MPI_VERSION
    char buff[MPI_MAX_PROCESSOR_NAME];
    int len;
    MPI::Get_processor_name(buff, len);
    std::string name(buff, len);
    return name;
#endif
    return std::string("NoMPI");

}

void mpi::close() {
#ifdef MPI_VERSION
    MPI::Finalize();
#endif
}

void mpi::send(std::string message, int process, int tag) {
#ifdef MPI_VERSION
    char *buff = const_cast<char*> (message.c_str());
    MPI_Send(buff, message.size(), MPI_CHAR, process, tag, MPI_COMM_WORLD);
#endif
}

std::string mpi::receive(int process, int size) {
#ifdef MPI_VERSION
    int tag = 0;
    char buff[size];
    memset(buff, '\0', size);
    MPI_Status stat;
    MPI_Recv(buff, size, MPI_CHAR, process, tag, MPI_COMM_WORLD, &stat);
    std::string message(buff);
    for (int i = 0; (i < size) && (buff[i] != '\0'); i++) {
        message += buff[i];
    }
    return message;
#else
    return std::string("No MPI on system. Could not receive meessage");
#endif
}
