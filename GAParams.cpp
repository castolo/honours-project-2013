#include "GAParams.h"
#include <fstream>
#include <stdlib.h>

int GAParams::POPULATION_SIZE = 20;
int GAParams::MAX_GENERATIONS = 1000;
float GAParams::CROSSOVER_RATE = 0.9f;
float GAParams::MUTATION_RATE = 0.01f;
int GAParams::TIME_PER_MOVE = 150;
int GAParams::GENERATION_START = 0;
int GAParams::MIN_DEPTH = 4;
int GAParams::MAX_MOVES = 150;

void GAParams::load() {
    std::ifstream file("config.ini");
    std::string line;
    while (file.good()) {
        std::getline(file, line);
        if (line == "") {
            break;
        }
        std::vector<std::string> v = split(line, ' ');
        if (v[0] == "population") {
            GAParams::POPULATION_SIZE = atoi(v[1].c_str());
        } else if (v[0] == "iterations") {
            GAParams::MAX_GENERATIONS = atoi(v[1].c_str());
        } else if (v[0] == "crossover") {
            GAParams::CROSSOVER_RATE = atof(v[1].c_str());
        } else if (v[0] == "mutation") {
            GAParams::MUTATION_RATE = atof(v[1].c_str());
        } else if (v[0] == "time") {
            GAParams::TIME_PER_MOVE = atoi(v[1].c_str());
        } else if (v[0] == "start") {
            GAParams::GENERATION_START = atoi(v[1].c_str());
        } else if (v[0] == "depth") {
            GAParams::MIN_DEPTH = atoi(v[1].c_str());
        } else if (v[0] == "moves") {
            GAParams::MAX_MOVES = atoi(v[1].c_str());
        }
    }
    file.close();

}