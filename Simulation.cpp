/* 
 * File:   Simulation.cpp
 * Author: Workstation
 * 
 * Created on 15 August 2013, 10:20 AM
 */

#include "Simulation.h"
#include "Tournament.h"
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <sstream>

Simulation::Simulation() {
    seed = time(NULL);

    interrupted = false;
    generation = -1;
    log("\n************************************************************************\n", false);


    GAParams::load();



    std::ostringstream oss;
    oss << "Settings:\nPopulation size: " << GAParams::POPULATION_SIZE << "\nMax Iterations: " <<
            GAParams::MAX_GENERATIONS << "\nCrossover Rate: " << GAParams::CROSSOVER_RATE <<
            "\nMutation Rate: " << GAParams::MUTATION_RATE << "\nTime Per Move: " << GAParams::TIME_PER_MOVE << " ms\n";
    log(oss.str());


}

/**
 * Generates a random float in the range [a,b]
 * @param a the lower bound
 * @param b the upper bound
 * @param precision the number of digits after the decimal place
 * @return a random float between a and b (inclusive)
 */
double Simulation::rand(float a, float b, int precision) {
    double expand = pow(10, precision);
    int min = floor(a * expand + 0.5);
    int max = floor(b * expand + 0.5);
    unsigned int r = rand(min, max);
    return r / expand;
}

/**
 * Returns a random integer in the range [a, b]. 
 * @param a the lower bound
 * @param b the upper bound
 * @return a random number between a and b (inclusive).
 */
int Simulation::rand(int a, int b) {
    b++; //make it inclusive  
    unsigned int rand = random(&seed);
    return (rand % (b - a)) +a;

}

// <editor-fold defaultstate="collapsed" desc="Generate random parameters for initial population">

std::vector<double> Simulation::getRandomParameters() {

    std::vector<double> params;
    params.push_back(rand(LOWER[0], UPPER[0])); //network weight

    params.push_back(rand(LOWER[1], UPPER[1])); //queen value
    params.push_back(rand(LOWER[2], UPPER[2])); // rook value
    params.push_back(rand(LOWER[3], UPPER[3])); // bishop value
    params.push_back(rand(LOWER[4], UPPER[4])); //knight value
    params.push_back(rand(LOWER[5], UPPER[5])); //pawn value
    // params.push_back(params[0] + 2 * params[1] + 2 * params[2] + 2 * params[3] + 8 * params[4]); //start material

    params.push_back(rand(LOWER[6], UPPER[6], 4)); //knight mobility
    params.push_back(rand(LOWER[7], UPPER[7], 4)); //pawn mobility
    params.push_back(rand(LOWER[8], UPPER[8], 4)); //rook mobility
    params.push_back(rand(LOWER[9], UPPER[9], 4)); //bishop mobility
    params.push_back(rand(LOWER[10], UPPER[10], 4)); //queen mobility
    params.push_back(rand(LOWER[11], UPPER[11], 4)); //king mobility

    params.push_back(rand(LOWER[12], UPPER[12])); // knight attack
    params.push_back(rand(LOWER[13], UPPER[13])); // rook attack
    params.push_back(rand(LOWER[14], UPPER[14])); // bishop attack
    params.push_back(rand(LOWER[15], UPPER[15])); //queen attack

    params.push_back(rand(LOWER[16], UPPER[16])); // passedPawnReward

    params.push_back(-(double) rand(-UPPER[17], -LOWER[17])); // doubledPawnPenalty

    params.push_back(-(double) rand(-UPPER[18], -LOWER[18])); // isolatedPawnPenalty
    params.push_back(rand(LOWER[19], UPPER[19], 4)); //isolatedAdvancedPawnWeight
    params.push_back(rand(LOWER[20], 20, 4)); //supportedAdvancedPawnWeight

    params.push_back(-(double) rand(-UPPER[21], -LOWER[21])); //poorPawnShieldPenalty
    params.push_back(-(double) rand(-UPPER[22], -LOWER[22])); //    pawnShieldPenalty1
    params.push_back(-(double) rand(-UPPER[23], -LOWER[23])); //    pawnShieldPenalty2
    params.push_back(-(double) rand(-UPPER[24], -LOWER[24])); //    pawnShieldPenalty3
    params.push_back(-(double) rand(-UPPER[25], -LOWER[25])); //    pawnShieldPenalty4
    params.push_back(-(double) rand(-UPPER[26], -LOWER[26])); //    pawnShieldPenalty5 
    params.push_back(-(double) rand(-UPPER[27], -LOWER[27])); //    centralKingPenalty
    params.push_back(-(double) rand(-UPPER[28], -LOWER[28])); //    pawnShieldPenalty6 
    params.push_back(-(double) rand(-UPPER[29], -LOWER[29])); //    pawnShieldPenalty7
    params.push_back(-(double) rand(-UPPER[30], -LOWER[30])); //    pawnShieldPenalty8
    params.push_back(-(double) rand(-UPPER[31], -LOWER[31])); //    pawnShieldPenalty9

    return params;
}
// </editor-fold>

Simulation::~Simulation() {
    for (int i = 0; i < population.size(); i++) {
        delete population[i];
        //    tempPopulation[i] = NULL;
    }
    std::vector<ExtendedSphinx*>().swap(tempPopulation);
    std::vector<ExtendedSphinx*>().swap(population);
}

void Simulation::interrupt() {
    interrupted = true;
}

void Simulation::log(std::string data, bool withTime) {

    createDir();
    std::ofstream file;
    file.open(LOG_FILE, std::ios_base::app);
    if (!withTime) {
        file << data << std::endl;
    } else {
        file << getTime() << "--> " << data << std::endl;
    }
    file.close();
}

std::string Simulation::getTime() {
    time_t t = time(NULL);
    struct tm * now = localtime(&t);
    char buf[50];
    sprintf(buf, "%02d/%02d/%02d %02d:%02d:%02d", now->tm_year % 100,
            now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
    return std::string(buf);
}

void Simulation::savePopulation() {
    std::string dir = DIR;
    dir += ("/generation_" + ntos(generation));
    createDir(dir.c_str());
    for (int i = 0; i < population.size(); i++) {
        population[i]->write(dir + "/" + population[i]->getName() + ".txt");
    }
    log("Generation " + ntos(generation) + " saved to folder " + dir);
}

void Simulation::start() {
    interrupted = false;
    population.clear();
    tempPopulation.clear();
    generation = 0;
    log("Simulation starting");


    // <editor-fold defaultstate="collapsed" desc="Randomly create initial population">

    for (int i = 0; i < GAParams::POPULATION_SIZE; i++) {
        std::string name = ntos((generation * GAParams::POPULATION_SIZE) + i);
        ExtendedSphinx *engine = new ExtendedSphinx(generation);
        engine->setName(name);
        std::vector<double> params = getRandomParameters();
        engine->setEvaluationParameters(params);
        ExtendedSphinx *engine2 = new ExtendedSphinx(generation);
        params = engine->getParameters();
        engine2->setName(name);
        engine2->setAllParameters(params);

        population.push_back(engine);
        tempPopulation.push_back(engine2);
    }
    log("Initial population of size " + ntos(GAParams::POPULATION_SIZE) + " created");
    // </editor-fold>

    for (generation = 0; generation < GAParams::MAX_GENERATIONS; generation++) {

        // <editor-fold defaultstate="collapsed" desc="Save population to text file">
        savePopulation();
        std::string temp = DIR;
        temp += ("/generation_" + ntos(generation));
        createDir(temp.c_str());
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Play tournament and get fitness function">

        /**
         * Using a copy of the actual population since the engines are deleted by both the Tournament
         * and Matches they belong to afterwards, which may cause problems elsewhere (e.g. in this 
         * class's destructor). 
         */
        Tournament t = Tournament(tempPopulation);
        log("Starting tournament " + ntos(generation) + " (" + ntos(t.getMatches()) + " matches scheduled)");

        std::string dir = DIR;
        dir += ("/generation_" + ntos(generation)) + "/games";
        createDir(dir.c_str());
        t.start(dir);
        log("Tournament " + ntos(generation) + " completed with result:\n" + t.toString());

        int* results = t.getResults();
        log("Calculating fitness scores");
        float* fitness = calculateFitnessScore(results);
        delete [] results;
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Generate new population">

        int i = 0;
        int N = population.size();
        std::vector<std::vector<double> > newPop;
        std::vector<std::pair<int, int> > parents;

        log("Generating new population " + ntos(generation + 1));

        //ELITISM
        // for (int k = 0; k < GAParams::ELITISM; k++) {
        int A = getBest(fitness);
        newPop.push_back(population.at(A)->getParameters());
        parents.push_back(std::make_pair(atoi(population.at(A)->getName().c_str()), -2));
        i++;
        //}

        while (i < N) {

            int A = select(fitness, N);
            int B = select(fitness, N);

            std::vector<std::vector<double> > C = crossover(A, B);
            bool singleton = (C.size() == 1);
            for (int j = 0; j < C.size(); j++) {

                mutate(C[j]);

                if (singleton) {

                    parents.push_back(std::make_pair(atoi(population.at(A)->getName().c_str()), -1));
                } else {
                    parents.push_back(std::make_pair(atoi(population.at(A)->getName().c_str()), atoi(population.at(B)->getName().c_str())));
                    int t = A;
                    A = B;
                    B = t;
                }


                newPop.push_back(C[j]);
            }
            i += C.size();
        }
        delete[]fitness;
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Set the new population to be used next by the GA">

        //SET POPULATION TO NEW POP
        for (int i = 0; i < population.size(); i++) {
            delete population[i];
            tempPopulation[i] = NULL;
        }
        population.clear();
        tempPopulation.clear();

        for (int i = 0; i < GAParams::POPULATION_SIZE; i++) {
            std::string name = ntos(((generation + 1) * GAParams::POPULATION_SIZE) + i);

            int A = parents.at(i).first;
            int B = parents.at(i).second;

            ExtendedSphinx *engine = new ExtendedSphinx(generation);
            engine->setName(name);
            engine->setParents(A, B);
            std::vector<double> params = newPop.at(i);
            engine->setAllParameters(params);
            ExtendedSphinx *engine2 = new ExtendedSphinx(generation);
            engine2->setName(name);
            engine2->setParents(A, B);
            engine2->setAllParameters(params);
            population.push_back(engine);
            tempPopulation.push_back(engine2);
        }
        log("Population from generation " + ntos(generation) + " created");
        // </editor-fold>

    }

}

// <editor-fold defaultstate="collapsed" desc="Selection Operator">

float * Simulation::calculateFitnessScore(const int *results) {
    int N = population.size();
    double maxPoints = 2 * (N - 1);
    float *fitness = new float[N];
    float sum = 0.0f;
    for (int i = 0; i < N; i++) {
        float score = results[i] / 2.0f;
        fitness[i] = score / maxPoints;
        sum += fitness[i];
    }
    for (int i = 0; i < N; i++) {
        fitness[i] = fitness[i] / sum;
    }
    return fitness;
}

int Simulation::select(const float* fitness, int N) {
    int K = 10000;
    int chooser[K + 1];
    int idx = 0;
    int sum = 0;
    for (int i = 0; i < N; i++) {
        int score = (int) (fitness[i] * K);
        sum += score;
        while (score--) {
            chooser[idx++] = i;
        }
    }
    int r = rand(0, sum);
    return chooser[r];
}
// </editor-fold>



// <editor-fold defaultstate="collapsed" desc="Crossover Operator">

std::vector<std::vector<double> > Simulation::crossover(int A, int B) {
    std::vector<std::vector<double> > children;
    double r = rand(0.0f, 0.99f, 2);
    if (r > GAParams::CROSSOVER_RATE) {
        //No crossover performed!
        children.push_back(population.at(A)->getParameters());
    } else if (A == B) {
        std::vector<double> a = population.at(A)->getParameters();
        children.assign(2, a); // May have to check this?
    } else {
        //Perform crossover
        std::vector<double> a = population.at(A)->getParameters();
        std::vector<double> b = population.at(B)->getParameters();
        int N = a.size();
        std::vector<double> u, v;
        u.reserve(N);
        v.reserve(N);
        for (int i = 0; i < GAParams::CROSSOVER_POINT; i++) {
            u.push_back(a.at(i));
            v.push_back(b.at(i));
        }
        for (int i = GAParams::CROSSOVER_POINT; i < N; i++) {
            u.push_back(b.at(i));
            v.push_back(a.at(i));
        }
        children.push_back(u);
        children.push_back(v);
    }
    return children;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Gaussian Mutation Operator">

void Simulation::mutate(std::vector<double> &x) {

    for (int i = 0; i < x.size(); i++) {
        double y = x[i];
        double r = rand(0.0f, 0.99f, 2);
        if (r <= GAParams::MUTATION_RATE) {
            //mutate (uniform or gaussian????)
            //FROM AKSENOV / Genetic Algorithm + Data structures = Evolution Programss
            //            int sign = rand(0, 1) == 0 ? -1 : 1;
            //            double r = rand(0,1,6);
            //            int b = 5;            
            //            double exp = pow(1-(((double)generation)/(double)(GAParams::MAX_GENERATIONS)), b);
            //            y = y + sign*(1 - pow(r,exp));

            //FROM http://www.iue.tuwien.ac.at/phd/heitzinger/node27.html
            double a, b; //range of y i.e. y in [a,b]

            if (i < NUM_EVAL_WEIGHTS) {
                //since LOWER[i] <= y <= UPPER[i]
                a = LOWER[i];
                b = UPPER[i];
            } else {
                //since -1 < y < 1                
                b = nextafter(1, 0);
                a = -b;
            }
            double range = b - a;
            double sigma = range / 3.0; //???
            y = std::min(std::max(N(y, sigma), a), b);
        }
        x[i] = y;
    }
    // return x;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Random number from normal distribution">

double Simulation::N(double mu, double sigma) {
    std::normal_distribution<double> distribution(mu, sigma);
    double num = distribution(generator);
    return num;
}
// </editor-fold>

void Simulation::test() {

    using namespace std;



    vector<double> a, b, c;
    for (int i = 0; i < 20; i++) {
        if (i < 10) {
            a.push_back(i);
        } else {
            b.push_back(i);
        }
        c.push_back(rand(1, 100));
    }

    int N = a.size();
    std::vector<double> u, v;
    u.reserve(N);
    v.reserve(N);
    for (int i = 0; i < 5; i++) {
        u.push_back(a.at(i));
        v.push_back(b.at(i));
    }
    for (int i = 5; i < N; i++) {
        u.push_back(b.at(i));
        v.push_back(a.at(i));
    }

    for (int i = 0; i < N; i++) {
        cout << a[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < N; i++) {
        cout << b[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < N; i++) {
        cout << u[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < N; i++) {
        cout << v[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < c.size(); i++) {
        cout << c[i] << " ";
    }

    mutate(c);
    cout << endl;
    for (int i = 0; i < c.size(); i++) {
        cout << (int) c[i] << " ";
    }


}

int Simulation::getBest(const float* fitness) {
    int N = population.size();
    float max = fitness[0];
    int idx = 0;
    for (int i = 1; i < N; i++) {
        if (fitness[i] > max) {
            max = fitness[i];
            idx = i;
        }
    }
    return idx;

}