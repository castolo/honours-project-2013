/* 
 * File:   Parallel.h
 * Author: mint
 *
 * Created on September 5, 2013, 10:14 AM
 */

#ifndef PARALLEL_H
#define	PARALLEL_H

#ifndef _WIN32
#include <mpi.h>
#endif
#include <string>

namespace mpi {

    void open(int argc, char** argv);

    int getRank();

    int getSize();

    std::string getName();

    void close();

    void send(std::string message, int process, int tag = 0);
    std::string receive(int process, int size = 1024);

};

#endif	/* PARALLEL_H */
