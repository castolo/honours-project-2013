/* 
 * File:   OpeningMove.h
 * Author: Workstation
 *
 * Created on 12 June 2013, 2:03 PM
 */

#ifndef OPENINGMOVE_H
#define	OPENINGMOVE_H

#include <string>
#include <vector>

class OpeningMove {
public:

    OpeningMove(){}
    OpeningMove(std::string, std::string);
    OpeningMove(std::string, std::string, int, int, int, int, int, int, int);
    virtual ~OpeningMove();

    std::string getMove() {
        if (move == "" && opening!="")
            return "<end>";
        return move;
    }

    std::string getOpening() {
        return opening;
    }

    int getCount() {
        return count;
    }

    int getGMCount() {
        return gmCount;
    }

    int getELO() {
        return averageELO;
    }

    int getOccurences() {
        return occurences;
    }

    int getWins() {
        return wins;
    }

    int getDraws() {
        return draws;
    }

    int getLosses() {
        return losses;
    }

    std::string move;
    std::string opening;
    int count, averageELO, occurences, wins, draws, losses, gmCount;


private:






};

#endif	/* OPENINGMOVE_H */

