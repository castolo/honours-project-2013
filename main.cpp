/* 
 * File:   main.cpp
 * Author: Steve James
 *
 * Created on 11 April 2013, 8:00 AM
 */

#include <cstdlib>
#include <stdio.h>
#include <string>
#include<fstream>
#include<sstream>
#include <ctime>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <unordered_map>
#include <stdlib.h>
#include <iostream>
#include <pthread.h>
#include "Board.h"
#include "Move.h"
#include "OpeningMove.h"
#include "Sphinx.h"
#include "OpeningBook.h"
#include "Match.h"
#include "Tournament.h"
#include "EndgameTable.h"
#include "NeuralNetwork.h"
#include "Simulation.h"
#include "FastNeuralNetwork.h"
#include "ParallelSimulation.h"
#include "Parallel.h"
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#ifdef _WIN32
#include <windows.h>
#endif
using namespace std;

const string name = "Sphinx 0.1";
const string author = "Steve James";

const int MATCH = 2;
const int DEBUG = 3;
const int THINKING = 4;
const int TOURNAMENT = 5;
const int SIMULATION = 6;
const int PARALLEL_SIMULATION = 7;
const int TESTING = 8;

const bool USE_EVOLVED = true;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition = PTHREAD_COND_INITIALIZER;
pthread_t thinking, thread2;

Board board;
Tournament tournament;
Sphinx* engine;
Simulation simulation;
ParallelSimulation parallelSimulation;
int mode;
Match match;
Match *simMatch;

int argCount;
char **argVals;

// <editor-fold defaultstate="collapsed" desc="Perft and Hash Tests">

void testHash() {

    for (int k = 0; k < 10000; k++) {
        board = Board();
        board.setPosition(START_POSITION);
        uint64_t start = board.getHash();
        std::stack<uint64_t> history, h2;
        std::stack<int> mov;
        h2.push(board.getLock());
        history.push(start);
        srand(time(NULL));
        for (int i = 0; i < 100; i++) {

            bool forward = (board.getHash() == start) || (rand() % 2 == 0);

            if (forward) {

                int*moves = board.generatePseudoMoves();
                int N = 0;
                for (int j = 0; int move = moves[j]; j++) {
                    N++;
                }
                int p = 0;
                while (p < N) {
                    int next = moves[rand() % N];
                    if (board.makeMove(next)) {
                        mov.push(next);
                        h2.push(board.getLock());
                        history.push(board.getHash());
                        break;
                    } else board.undoMove(next);
                    p++;
                }


            } else {

                int back = mov.top();
                mov.pop();
                board.undoMove(back);
                h2.pop();
                history.pop();
                if (board.getHash() != history.top()) {
                    cout << "ERROR\n";
                }

                if (board.getLock() != h2.top()) {
                    cout << "ERROR\n";
                }

                string fen = board.getPosition();
                Board b = Board();
                b.setPosition(fen);

                if (board.getLock() != b.getLock()) {
                    cout << "ERROR\n";
                }

            }


        }
    }
    cout << "Done\n";
}

uint64_t miniMax(int depth) {
    if (depth == 0) {
        return 1;
    }
    uint64_t nodes = 0;
    int* moves = board.generatePseudoMoves();
    for (int i = 0; int move = moves[i]; i++) {

        bool legal = board.makeMove(move);
        if (legal) {
            nodes += miniMax(depth - 1);
        }
        board.undoMove(move);
    }
    delete[]moves;
    return nodes;
}

void perftTest(int ceiling) {
    using namespace std;
    board = Board();

    cout << "START" << endl;


    string path = "perftsuite.epd";
    ifstream file(path.c_str());
    string temp;
    long long count = 0ll;
    double time = 0;
    if (!file.good()) {
        cout << "perftsuite.epd not found!" << endl << endl;
        return;
    }

    for (int i = 0; file.good(); i++) {
        getline(file, temp);

        vector<string> vec = split(temp, ';');
        string fen = vec[0];
        board.setPosition(fen);
        for (int j = 1; j < vec.size() && j <= ceiling; j++) {

            stringstream sstr(vec[j]);
            uint64_t expected;
            sstr >> expected;

            if (expected > 0) {

                clock_t begin = clock();


                uint64_t actual = miniMax(j);
                clock_t end = clock();
                double e = double(end - begin) / CLOCKS_PER_SEC;
                time += e;
                count += actual;
                if (actual != expected) {
                    cout << "Move gen failed on " << fen << " at depth " << j <<
                            "\nExpected = " << expected << ", actual = " << actual << endl;
                }
            }
        }
        cout << i << " done" << endl;

    }
    cout << count << " nodes evaluated" << " in " << time << " at " << (count / time) / 1000.0 << " knps" << endl;
}
// </editor-fold>

void identify() {
    cout << "id name " << name << endl;
    cout << "id author " << author << endl;
    //options go here
}

void createEngine(){
    delete engine;
    if (USE_EVOLVED){
        ExtendedSphinx *e = new ExtendedSphinx("Evolved",-1);
        e->read("10000.txt");
        e->setNetworkWeight(0);        
        engine = e;
    }
    else{
        engine = new Sphinx();
    }    
}

void wait(int secs) {
    struct timespec time;
    struct timeval now;
    int rt;
    gettimeofday(&now, NULL);
    time.tv_sec = now.tv_sec + secs;
    time.tv_nsec = now.tv_usec * 1000ull;
    pthread_mutex_lock(&mutex);
    rt = pthread_cond_timedwait(&condition, &mutex, &time);
    pthread_mutex_unlock(&mutex);
}

void* timeoutMatch(void* arg) {
    wait(300); //wait 5 mins
    if ((simMatch) && (!simMatch->isInterrupted())) {
        simMatch->interrupt();
    }
    pthread_exit(NULL);
}

void* startParallelSimulation(void* arg) {
    mode = PARALLEL_SIMULATION;
    mpi::open(argCount, argVals);
    int size = mpi::getSize();
    if (size == 1) {
        cout << "Unable to run parallel simulation - not enough processors (1 found)" << endl;
    } else {
        int rank = mpi::getRank();
        if (rank == 0) {
            //master process
            cout << size << " processors were assigned" << endl;
            parallelSimulation = ParallelSimulation();
            parallelSimulation.start();
        } else {
            //slave process        
            string message = mpi::receive(0);
            while (message != "quit") {
                vector<string> files = split(message, ' ');
                int result = -1;
                ExtendedSphinx *a = new ExtendedSphinx(0);
                a->read(files[0]);
                ExtendedSphinx *b = new ExtendedSphinx(0);
                b->read(files[1]);
                cout << "Rank " << rank << ": starting match" << endl;
                simMatch = new Match(a, b, 0, 1, true);
                pthread_create(&thinking, NULL, timeoutMatch, NULL);
                simMatch->commence();
                if (!simMatch->isInterrupted()) {
                    pthread_cond_signal(&condition);
                }
                cout << "Rank " << rank << ": match ended" << endl;
                simMatch->write(files[2], true);
                result = simMatch->getResult();
                delete a;
                delete b;
                delete simMatch;
                cout << "Rank " << rank << ": Sending result" << endl;
                mpi::send(ntos(result), 0);
                condition = PTHREAD_COND_INITIALIZER;
                message = mpi::receive(0);
            }
            cout << "Exited" << endl;
        }
    }
    cout << "End of parallel simulation" << endl;
    mpi::close();
    pthread_exit(NULL);
}

void* startSimulation(void* arg) {
    mode = SIMULATION;
    simulation = Simulation();
    simulation.start();
    cout << "End of simulation" << endl;
    pthread_exit(NULL);
}

void* startComparisonMatches(void *arg) {
    mode = TESTING;
    Sphinx *A = new Sphinx("Control");
    ExtendedSphinx *B = new ExtendedSphinx("Evolved", -1);
    B->read("10000.txt");
#ifdef _WIN32
    mkdir("comparison");
#else
    mkdir(folder, 0777);
#endif
    tournament = Tournament(A, B);
    tournament.start("comparison");
    ofstream file;
    file.open("comparison/output.txt", std::ios_base::app);
    cout << tournament.toString() << endl;
    file << tournament.toString() << endl;
    file.close();
    tournament.print();
    pthread_exit(NULL);
}

void* startTournament(void* arg) {
    mode = TOURNAMENT;
    tournament = Tournament(5, true);
    tournament.start();
    pthread_exit(NULL);
}

void* startMatch(void* arg) {
    mode = MATCH;
    Sphinx* white = new Sphinx();
    Sphinx* black = new Sphinx();
    match = Match(white, black, "white", "black");
    match.commence();
    match.write("tournaments/");
    delete black;
    delete white;
    cout << ">" << endl;
    pthread_exit(NULL);
}

void* startEngine(void* arg) {
    mode = THINKING;
    long int* time = (long int*) arg;
    Line line = engine->search(*time);
    int move = line.getBestMove();

    if (move != 0) {
        cout << "bestmove " << move::toInputNotation(move) << endl;
    }
    pthread_exit(NULL);
}

void listenForInput() {
    string input;
    bool flag = true;
    int q = 0;
    while (flag) {
        getline(cin, input);
        vector<string> tokens = split(input, ' ');
        if (tokens.size() > 0) {
            string head = tokens[0];
            if (head == "uci") {
                identify();
                cout << "uciok" << endl;
            } else if (head == "isready") {
                //no options => return immeidately
                cout << "readyok" << endl;
            } else if (head == "ucinewgame") {
                createEngine();
                engine->setPosition(START_POSITION);
            } else if (head == "position") {
                if (engine == NULL) {
                    createEngine();
                }
                if (tokens.size() > 1) {
                    int j = 3;
                    if (tokens[1] == "startpos") {
                        engine->setPosition(START_POSITION);
                    } else if (tokens[1] == "fine70") {
                        engine->setPosition(FINE70_POSITION);
                    } else if (tokens[1] == "fen") {
                        j = 3;
                        string fen = tokens[2];
                        while (j < tokens.size() && tokens[j] != "moves") {
                            fen += " " + tokens[j];
                            j++;
                        }
                        j++;
                        engine->setPosition(fen);

                    }
                    for (; j < tokens.size(); j++) {
                        bool valid = engine->playMove(tokens[j]);
                        if (!valid) {
                            break;
                        }
                    }
                }
            } else if (head == "go") {
                if (!engine) {
                    createEngine();
                    engine->setPosition(START_POSITION);
                }
                engine->setMaxDepth(DEFAULT_MAX_DEPTH);
                short side = engine->getSide();
                long int time = 30000;
                for (int j = 1; j < tokens.size(); j++) {
                    if (side == WHITE && tokens[j] == "wtime") {
                        time = atoi(tokens[j + 1].c_str());
                        engine->setMaxDepth(DEFAULT_MAX_DEPTH);
                    } else if (side == BLACK && tokens[j] == "btime") {
                        time = atol(tokens[j + 1].c_str());
                        engine->setMaxDepth(DEFAULT_MAX_DEPTH);
                    } else if (tokens[j] == "depth") {
                        time = 100000000;
                        engine->setMaxDepth(atoi(tokens[j + 1].c_str()));
                    }
                }
                int rc = pthread_create(&thinking, NULL, startEngine, (void*) &time);
                if (rc > 0) {
                    cout << "Error No. " << rc << endl;
                }
            } else if (head == "stop") {
                switch (mode) {
                    case THINKING:
                        if (engine && engine->isThinking()) {
                            engine->interrupt();
                        }
                        break;
                    case MATCH:
                        if (match.getResult() == Match::IN_PROGRESS) {
                            match.interrupt();
                        }
                        break;
                    case TOURNAMENT:
                        tournament.interrupt();
                        break;
                    case SIMULATION:
                        simulation.interrupt();
                        break;
                    case PARALLEL_SIMULATION:
                        parallelSimulation.interrupt();
                        break;
                    case TESTING:
                        tournament.interrupt();
                        break;
                }

            } else if (head == "ponderhit") {

            } else if (head == "quit") {
                flag = false;
            } else if (head == "perft") {
                mode = DEBUG;
                int K = 4;
                if (tokens.size() >= 2) {
                    K = atoi(tokens[1].c_str());
                }
                perftTest(K);
            } else if (head == "testhash") {
                mode = DEBUG;
                testHash();
            } else if (head == "makebook") {
                mode = DEBUG;
                OpeningBook *ob;
                ob = new OpeningBook();
                ob->build();
                ob->write();
                delete ob;
            } else if (head == "showbook" || head == "printbook") {
                if (!engine) {
                    createEngine();
                    engine->setPosition(START_POSITION);
                }
                engine->showOpeningBook();
            } else if (head == "match") {
                cout << "<";
                int rc = pthread_create(&thinking, NULL, startMatch, NULL);
                if (rc > 0) {
                    cout << "Error No. " << rc << endl;
                }
            } else if (head == "tournament") {
                int rc = pthread_create(&thinking, NULL, startTournament, NULL);
                if (rc > 0) {
                    cout << "Error No. " << rc << endl;
                }
            } else if (head == "simulation") {
                int rc = pthread_create(&thinking, NULL, startSimulation, NULL);
                if (rc > 0) {
                    cout << "Error No. " << rc << endl;
                }
            } else if (head == "parallelsimulation") {

#ifdef MPI_VERSION
                int rc = pthread_create(&thread2, NULL, startParallelSimulation, NULL);
                if (rc > 0) {
                    cout << "Error No. " << rc << endl;
                }
#else
                cerr << "No MPI found on the system. Simulation could not be run" << endl;
#endif
            } else if (head == "testing") {
                int rc = pthread_create(&thinking, NULL, startComparisonMatches, NULL);
                if (rc > 0) {
                    cout << "Error No. " << rc << endl;
                }
            }
        }
    }
    delete engine;
    delete simMatch;
}

/*
 * 
 */
int main(int argc, char** argv) {
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    cout << name << endl << author << " (2013)" << endl << endl;
    argCount = argc;
    argVals = argv;
    listenForInput();
    return 0;
}

















/*
 * Something wrong here r4rk1/ppp1qppp/3pnn2/1B2p1B1/3PP3/2P2Q1P/P1P2PP1/R3R1K1 w - - 1 13 
 * after BxN, QxB, QxQ, PxQ, Rad1, NxP??????
 */



// <editor-fold defaultstate="collapsed" desc="Generate Magic Nums (http://chessprogramming.wikispaces.com/Looking+for+Magics)">


//#include <stdio.h>
//#include <stdlib.h>
//
//#define USE_32_BIT_MULTIPLICATIONS
//
//typedef unsigned long long uint64;
//
//uint64 random_uint64() {
//    uint64 u1, u2, u3, u4;
//    u1 = (uint64) (random()) & 0xFFFF;
//    u2 = (uint64) (random()) & 0xFFFF;
//    u3 = (uint64) (random()) & 0xFFFF;
//    u4 = (uint64) (random()) & 0xFFFF;
//    return u1 | (u2 << 16) | (u3 << 32) | (u4 << 48);
//}
//
//uint64 random_uint64_fewbits() {
//    return random_uint64() & random_uint64() & random_uint64();
//}
//
//int count_1s(uint64 b) {
//    int r;
//    for (r = 0; b; r++, b &= b - 1);
//    return r;
//}
//
//const int BitTable[64] = {
//    63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
//    51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
//    26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
//    58, 20, 37, 17, 36, 8
//};
//
//int pop_1st_bit(uint64 *bb) {
//    uint64 b = *bb ^ (*bb - 1);
//    unsigned int fold = (unsigned) ((b & 0xffffffff) ^ (b >> 32));
//    *bb &= (*bb - 1);
//    return BitTable[(fold * 0x783a9b23) >> 26];
//}
//
//uint64 index_to_uint64(int index, int bits, uint64 m) {
//    int i, j, k;
//    uint64 result = 0ULL;
//    for (i = 0; i < bits; i++) {
//        j = pop_1st_bit(&m);
//        if (index & (1 << i)) result |= (1ULL << j);
//    }
//    return result;
//}
//
//uint64 rmask(int sq) {
//    uint64 result = 0ULL;
//    int rk = sq / 8, fl = sq % 8, r, f;
//    for (r = rk + 1; r <= 6; r++) result |= (1ULL << (fl + r * 8));
//    for (r = rk - 1; r >= 1; r--) result |= (1ULL << (fl + r * 8));
//    for (f = fl + 1; f <= 6; f++) result |= (1ULL << (f + rk * 8));
//    for (f = fl - 1; f >= 1; f--) result |= (1ULL << (f + rk * 8));
//    return result;
//}
//
//uint64 bmask(int sq) {
//    uint64 result = 0ULL;
//    int rk = sq / 8, fl = sq % 8, r, f;
//    for (r = rk + 1, f = fl + 1; r <= 6 && f <= 6; r++, f++) result |= (1ULL << (f + r * 8));
//    for (r = rk + 1, f = fl - 1; r <= 6 && f >= 1; r++, f--) result |= (1ULL << (f + r * 8));
//    for (r = rk - 1, f = fl + 1; r >= 1 && f <= 6; r--, f++) result |= (1ULL << (f + r * 8));
//    for (r = rk - 1, f = fl - 1; r >= 1 && f >= 1; r--, f--) result |= (1ULL << (f + r * 8));
//    return result;
//}
//
//uint64 ratt(int sq, uint64 block) {
//    uint64 result = 0ULL;
//    int rk = sq / 8, fl = sq % 8, r, f;
//    for (r = rk + 1; r <= 7; r++) {
//        result |= (1ULL << (fl + r * 8));
//        if (block & (1ULL << (fl + r * 8))) break;
//    }
//    for (r = rk - 1; r >= 0; r--) {
//        result |= (1ULL << (fl + r * 8));
//        if (block & (1ULL << (fl + r * 8))) break;
//    }
//    for (f = fl + 1; f <= 7; f++) {
//        result |= (1ULL << (f + rk * 8));
//        if (block & (1ULL << (f + rk * 8))) break;
//    }
//    for (f = fl - 1; f >= 0; f--) {
//        result |= (1ULL << (f + rk * 8));
//        if (block & (1ULL << (f + rk * 8))) break;
//    }
//    return result;
//}
//
//uint64 batt(int sq, uint64 block) {
//    uint64 result = 0ULL;
//    int rk = sq / 8, fl = sq % 8, r, f;
//    for (r = rk + 1, f = fl + 1; r <= 7 && f <= 7; r++, f++) {
//        result |= (1ULL << (f + r * 8));
//        if (block & (1ULL << (f + r * 8))) break;
//    }
//    for (r = rk + 1, f = fl - 1; r <= 7 && f >= 0; r++, f--) {
//        result |= (1ULL << (f + r * 8));
//        if (block & (1ULL << (f + r * 8))) break;
//    }
//    for (r = rk - 1, f = fl + 1; r >= 0 && f <= 7; r--, f++) {
//        result |= (1ULL << (f + r * 8));
//        if (block & (1ULL << (f + r * 8))) break;
//    }
//    for (r = rk - 1, f = fl - 1; r >= 0 && f >= 0; r--, f--) {
//        result |= (1ULL << (f + r * 8));
//        if (block & (1ULL << (f + r * 8))) break;
//    }
//    return result;
//}
//
//int transform(uint64 b, uint64 magic, int bits) {
//#if defined(USE_32_BIT_MULTIPLICATIONS)
//    return
//    (unsigned) ((int) b * (int) magic ^ (int) (b >> 32)*(int) (magic >> 32)) >> (32 - bits);
//#else
//    return (int) ((b * magic) >> (64 - bits));
//#endif
//}
//
//uint64 find_magic(int sq, int m, int bishop) {
//    uint64 mask, b[4096], a[4096], used[4096], magic;
//    int i, j, k, n, mbits, fail;
//
//    mask = bishop ? bmask(sq) : rmask(sq);
//    n = count_1s(mask);
//
//    for (i = 0; i < (1 << n); i++) {
//        b[i] = index_to_uint64(i, n, mask);
//        a[i] = bishop ? batt(sq, b[i]) : ratt(sq, b[i]);
//    }
//    for (k = 0; k < 100000000; k++) {
//        magic = random_uint64_fewbits();
//        if (count_1s((mask * magic) & 0xFF00000000000000ULL) < 6) continue;
//        for (i = 0; i < 4096; i++) used[i] = 0ULL;
//        for (i = 0, fail = 0; !fail && i < (1 << n); i++) {
//            j = transform(b[i], magic, m);
//            if (used[j] == 0ULL) used[j] = a[i];
//            else if (used[j] != a[i]) fail = 1;
//        }
//        if (!fail) return magic;
//    }
//    printf("***Failed***\n");
//    return 0ULL;
//}
//
//int RBits[64] = {
//    12, 11, 11, 11, 11, 11, 11, 12,
//    11, 10, 10, 10, 10, 10, 10, 11,
//    11, 10, 10, 10, 10, 10, 10, 11,
//    11, 10, 10, 10, 10, 10, 10, 11,
//    11, 10, 10, 10, 10, 10, 10, 11,
//    11, 10, 10, 10, 10, 10, 10, 11,
//    11, 10, 10, 10, 10, 10, 10, 11,
//    12, 11, 11, 11, 11, 11, 11, 12
//};
//
//int BBits[64] = {
//    6, 5, 5, 5, 5, 5, 5, 6,
//    5, 5, 5, 5, 5, 5, 5, 5,
//    5, 5, 7, 7, 7, 7, 5, 5,
//    5, 5, 7, 9, 9, 7, 5, 5,
//    5, 5, 7, 9, 9, 7, 5, 5,
//    5, 5, 7, 7, 7, 7, 5, 5,
//    5, 5, 5, 5, 5, 5, 5, 5,
//    6, 5, 5, 5, 5, 5, 5, 6
//};
//
//int main() {
//    int square;
//    uint64 magic;
//
//    uint64_t bb1[64] = {0};
//    uint64_t bb2[64] = {0};
//
//    int count = 56;
//
//    for (square = 0; square < 64; square++) {
//        uint64 ans = find_magic(square, RBits[square], 0);
//        bb1[count] = ans;
//        count++;
//        if (count % 8 == 0) {
//            count -= 16;
//        }
//    }
//    cout << ("const uint64_t ROOK_MAGIC[64] = {\n");
//    int x = 1;
//    for (int i = 0; i < 64; i++) {
//
//        cout<<bb1[i]<<"ull,";
//        if (x % 3 == 0) {
//            cout << ("\n");
//        }
//        x++;
//    }
//    cout<<("};\n\n");
//    
//    
//    count = 56;
//
//    for (square = 0; square < 64; square++) {
//        uint64 ans = find_magic(square, BBits[square], 1);
//        bb1[count] = ans;
//        count++;
//        if (count % 8 == 0) {
//            count -= 16;
//        }
//    }
//    cout << ("const uint64_t BISHOP_MAGIC[64] = {\n");
//     x = 1;
//    for (int i = 0; i < 64; i++) {
//
//        cout<<bb1[i]<<"ull,";
//        if (x % 3 == 0) {
//            cout << ("\n");
//        }
//        x++;
//    }
//    cout<<("};\n\n");
//
//    return 0;
//}
// </editor-fold>
