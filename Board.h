/* 
 * File:   Board.h
 * Author: 475531
 *
 * Created on 11 April 2013, 8:37 AM
 */

#ifndef BOARD_H
#define	BOARD_H

#include "HashBoard.h"
#include <string>
#include <vector>
#include <stack>

#include <stdio.h>
#include <sstream>
#include <iostream>

struct History {
    int enPassant, fiftyMove;
    short whiteCastle, blackCastle;
};


// <editor-fold defaultstate="collapsed" desc="Bitboard Operations">

namespace bitboard {

    void out(uint64_t);

    std::string show(uint64_t);
    /**
     * Find the position of the least significant 1 bit.
     */
    int LS1B(uint64_t);
    /**
     * Get the bitboard that represents a square at a given index (where 
     * 0 = a8 and 63 = h1)
     * @param 
     * @return 
     */
    uint64_t getBitBoard(int);

    /**
     * Use this for sparse bitboards
     * @param bb the bitboard
     * @return the number of bits set
     */
    int getSparseBitCount(uint64_t);

    /**
     * Use this for dense bitboards
     * @param bb the bitboard
     * @return the number of bits set
     */
    int getDenseBitCount(uint64_t);

};
// </editor-fold>

class Board : public HashBoard {
public:
    
    int operator[](int i) const{
        return board[i];
    }

    bool operator==(const Board &other) const {
        return getLock() == other.getLock();
    }

    bool operator!=(const Board &other) const {
        return !(*this == other);
    }

    uint64_t getKing() {
        return side == WHITE ? WHITE_KING_BOARD : BLACK_KING_BOARD;
    }

    int getFiftyRule() {
        return fiftyRule;
    }

    /**
     * Evaluates the current position
     * @return an evaluation of the position
     */
    int evaluate();

    std::string getPosition();
    Board();

    virtual ~Board();
    bool setPosition(std::string);
    bool makeMove(int);
    bool undoMove(int);
    void swapSides();
    short getSide();

    int isKRKEnding();
    int isKQKEnding();

    uint64_t getOpeningHash() const;

    // std::vector<int> filterMoves(std::vector<int>);

    int* generatePseudoMoves() const;

    /**
     * Determines if moving the piece at the source square results in a discovered check
     * @param source the moving piece
     * @param destination the moving piece
     * @param king the bitboard of the opponent's king
     * @param diagonal the diagonal to check
     * @return 
     */
    bool isDiscoveredCheckOnDiagonal(uint64_t source, uint64_t destination, uint64_t king, uint64_t diagonal) const;


    /**
     * Determines if moving the piece at the source square results in a discovered check
     * @param source the moving piece
     * @param destination the moving piece
     * @param king the bitboard of the opponent's king
     * @param rank the horizontal to check
     * @return 
     */
    bool isDiscoveredCheckOnHorizontal(uint64_t source, uint64_t destination, uint64_t king, uint64_t horizintal) const;


    void show();

    bool hasSufficientMaterial();

    short getRepetitions();
    bool canMakeNullMove();
    short getMoves();
    bool attacks(uint64_t);

    void setParameters(const std::vector<double>&);
    std::vector<double> getParameters() const;
    
    
private:


    void setDefaultParameters();


    bool isAttacked(int, ...);

    uint64_t getRookMoves(int a, uint64_t b) const {
        return getRookMoves(a, b, ALL_PIECES_BOARD);
    }

    uint64_t getBishopMoves(int a, uint64_t b) const {
        return getBishopMoves(a, b, ALL_PIECES_BOARD);
    }

    uint64_t getRookMoves(int, uint64_t, uint64_t) const;
    /**
     * Generate bishop moves from a particular square
     * @param from the source square
     * @param target the space the bishop can occupy (usually all square that aren't occupied
     * by its own pieces)
     * @return a bitboard consisting of the possible bishop moves
     */
    uint64_t getBishopMoves(int from, uint64_t target, uint64_t) const;

    std::vector<int> generateCastlingMoves() const;
    std::vector<int> generateKnightMoves() const;
    std::vector<int> generateKingMoves() const;
    std::vector<int> generatePawnMoves() const;
    std::vector<int> generateRookMoves() const;
    std::vector<int> generateBishopMoves() const;
    std::vector<int> generateQueenMoves() const;

    std::vector<int> generateEnPassantMoves() const;


    void shiftPieceBoards(int);
    void orBitboards(uint64_t, int);
    void andBitboards(uint64_t, int);
    void movePiece(int piece, int dest, int source);
    void addPiece(int piece, int idx);

    void removePiece(int piece, int idx);


    // <editor-fold defaultstate="collapsed" desc="Evaluation Parameters">

    int queenValue, rookValue, bishopValue, knightValue, pawnValue;
    double knightMobility, pawnMobility, rookMobility, bishopMobility, queenMobility, kingMobility;
    int knightAttackWeight, rookAttackWeight, bishopAttackWeight, queenAttackWeight;
    int startMaterial;
    int poorPawnShieldPenalty, pawnShieldPenalty1, pawnShieldPenalty2, pawnShieldPenalty3,
    pawnShieldPenalty4, pawnShieldPenalty5, centralKingPenalty, pawnShieldPenalty6, pawnShieldPenalty7
    , pawnShieldPenalty8, pawnShieldPenalty9;

    int passedPawnReward, doubledPawnPenalty, isolatedPawnPenalty;
    double isolatedAdvancedPawnWeight, supportedAdvancedPawnWeight;
    // </editor-fold>

    int evaluatePawnStructure();

    int evaluateMobilityandKingSafety(int, int);
    int scale(int, int);
    int whiteKingShield();
    int blackKingShield();

    short side;
    short whiteCastle;
    short blackCastle;
    short enPassant;
    short fiftyRule;
    short moves;

    int whiteKing;
    int blackKing;

    /*
     * Bitboards
     */
    uint64_t BLACK_PAWN_BOARD;
    uint64_t WHITE_PAWN_BOARD;
    uint64_t BLACK_KNIGHT_BOARD;
    uint64_t WHITE_KNIGHT_BOARD;
    uint64_t BLACK_BISHOP_BOARD;
    uint64_t WHITE_BISHOP_BOARD;
    uint64_t BLACK_ROOK_BOARD;
    uint64_t WHITE_ROOK_BOARD;
    uint64_t BLACK_QUEEN_BOARD;
    uint64_t WHITE_QUEEN_BOARD;
    uint64_t BLACK_KING_BOARD;
    uint64_t WHITE_KING_BOARD;
    uint64_t WHITE_PIECES_BOARD;
    uint64_t BLACK_PIECES_BOARD;
    uint64_t ALL_PIECES_BOARD;
    uint64_t EMPTY_BOARD;
    short board[64];

    std::vector<int> movesPlayed;
    std::vector<uint64_t> repetitions;
    std::stack<History> history; //stores 50 move rule numbers of played moves, 
    //prevEp,whiteCastle,BlackCastle;



};

#endif	/* BOARD_H */

