/* 
 * File:   NeuralNetwork.h
 * Author: Workstation
 *
 * Created on 19 July 2013, 4:33 PM
 */

#ifndef NEURALNETWORK_H
#define	NEURALNETWORK_H
#include "NeuralLayer.h"
#include "Board.h"
#include <cstdarg>

class NeuralNetwork {
public:
    NeuralNetwork();
    NeuralNetwork(int, ...);
    virtual ~NeuralNetwork();
    
    std::vector<double> activate(std::vector<double>) const;
    std::vector<double> activate(const Board&) const;
    void update(const std::vector<double> &);
    std::vector<double> getWeights() const;
    int getNumberOfWeights() const;
    void show() const;
    
private:
    int inputs;
    int outputs;
    int hiddenLayers;    
    std::vector<NeuralLayer> layers; //excludes input layer
    double activationFunction(double) const;
    



};

#endif	/* NEURALNETWORK_H */

