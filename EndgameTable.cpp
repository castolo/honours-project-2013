/* 
 * File:   EndgameTable.cpp
 * Author: Steve
 * 
 * Created on 03 July 2013, 11:42 AM
 */

#include "EndgameTable.h"
#include "Move.h"
#include "Sphinx.h"
#include <ctime>
#include <time.h>
#include <string.h>
#include <string>

EndgameTable::EndgameTable() {
    seed = time(NULL);
}

EndgameTable::~EndgameTable() {
}

// <editor-fold defaultstate="collapsed" desc="Generate all Positions">

std::vector<std::string> EndgameTable::generateAllPositions(int piece) {

    std::vector<std::string> ans, temp;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            for (int ii = 0; ii < 8; ii++) {
                for (int jj = 0; jj < 8; jj++) {

                    for (int iii = 0; iii < 8; iii++) {
                        for (int jjj = 0; jjj < 8; jjj++) {

                            int board[8][8]; //[rows][cols]
                            memset(board, 0, sizeof (board[0][0]) * 8 * 8);

                            board[i][j] = WHITE_KING;
                            if (board[ii][jj] != 0) {
                                continue;
                            }
                            board[ii][jj] = piece;

                            if (board[iii][jjj] != 0) {
                                continue;
                            }
                            board[iii][jjj] = BLACK_KING;

                            if (abs(iii - i) <= 1 && abs(jjj - j) <= 1) {
                                continue;
                            }
                            std::string fen = arrayToFEN(board);
                            temp.push_back(fen + " b - - 0 1");
                            temp.push_back(fen + " w - - 0 1");
                        }
                    }
                }
            }
        }
    }

    for (int i = 0; i < temp.size(); i++) {
        if (legal(temp[i])) {
            ans.push_back(temp[i]);
        }
    }
    return ans;

}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Build Database">

int EndgameTable::evaluate(std::vector<std::string>& fens, std::vector<std::string>& moves,
        std::vector<int>& depth) {

    int count = 0;
    int N = fens.size();

    int side = WHITE;


    for (int i = 0; i < N; i++) {
        if (isMate(fens[i])) {
            depth[i] = 0;
            moves[i] = "-";
            count++;
            std::cout << FENToString(fens[i]) << "," << depth[i] << "," << moves[i] << std::endl;
        }
    }




    int x = 1;
    while (count > 0) {

        std::cout << "Searching depth: " << x << std::endl;

        count = 0;


        for (int i = 0; i < N; i++) {

            if (depth[i] != -INFTY) {
                continue;
            }

            Board board;
            board.setPosition(fens[i]);

            if (board.getSide() == side) {

                int *aMoves = board.generatePseudoMoves();


                if (side == WHITE) {
                    for (int j = 0; int move = aMoves[j]; j++) {

                        if ((abs(move::getCapturedPiece(move)) != KING)) {
                            board.makeMove(move);

                            std::string next = board.getPosition();
                            next = split(next, ' ')[0];
                            next += " b - - 0 1";

                            bool reach = canReach(next, fens, depth, -(x - 1));
                            board.undoMove(move);

                            if (reach) {
                                depth[i] = x;
                                moves[i] = move::toInputNotation(move);
                                count++;

                                std::cout << FENToString(fens[i]) << "," << depth[i] << "," << moves[i] << std::endl;
                                break;
                            }
                        }
                    }
                } else {
                    bool flag = true;
                    bool one = false;
                    std::string m;
                    for (int j = 0; int move = aMoves[j]; j++) {

                        if ((abs(move::getCapturedPiece(move)) != KING)) {


                            bool legal = board.makeMove(move);

                            if (legal) {
                                one = true;
                            }

                            if (!legal) {
                                board.undoMove(move);
                                continue;
                            }
                            std::string next = board.getPosition();
                            next = split(next, ' ')[0];
                            next += " w - - 0 1";

                            int d = -INFTY;
                            int idx = find(fens, next);
                            if (idx == -1) {
                                d = -INFTY;
                            } else {
                                d = depth[idx];
                            }
                            board.undoMove(move);

                            if (d == -INFTY) {
                                flag = false;
                                break;
                            } else if (d > abs(x)) {
                                flag = false;
                                break;
                            } else if (d == abs(x)) {
                                m = move::toInputNotation(move);
                            }

                        }
                    }

                    if (one && flag) {
                        depth[i] = x;
                        moves[i] = m;
                        count++;
                        std::cout << FENToString(fens[i]) << "," << depth[i] << "," << moves[i] << std::endl;
                    }

                }
                delete[] aMoves;
            }
        }

        side *= -1;
        if (x > 0) {
            x = -x;
        } else {
            x = -x + 1;
        }

    }
    return 0;
}
// </editor-fold>

void EndgameTable::buildQueen() {
    std::vector<std::string> fens;
    fens = generateAllPositions(WHITE_QUEEN);
    std::cout << "All positions loaded." << std::endl;
    std::vector<std::string> moves;
    std::vector<int> depth;
    for (int i = 0; i < fens.size(); i++) {
        depth.push_back(-INFTY);
        moves.push_back("");
    }
    evaluate(fens, moves, depth);
    std::cout << "\n" << std::endl;
    std::ofstream file;
    file.open("kqk.txt");
    for (int i = 0; i < fens.size(); i++) {
        file << FENToString(fens[i]) << "," << depth[i] << "," << moves[i] << std::endl;
    }
    file.close();
}

void EndgameTable::buildRook() {
    std::vector<std::string> fens;
    fens = generateAllPositions(WHITE_ROOK);

    std::cout << "All positions loaded." << std::endl;
    std::vector<std::string> moves;
    std::vector<int> depth;
    for (int i = 0; i < fens.size(); i++) {
        depth.push_back(-INFTY);
        moves.push_back("");
    }
    evaluate(fens, moves, depth);
    std::cout << "\n" << std::endl;
    std::ofstream file;
    file.open("krk.txt");
    for (int i = 0; i < fens.size(); i++) {
        file << FENToString(fens[i]) << "," << depth[i] << "," << moves[i] << std::endl;
    }
    file.close();
}

void EndgameTable::read() {

    rookMap.clear();
    queenMap.clear();
    std::string path = "krk.txt";
    std::ifstream file(path.c_str());
    std::string line;
    while (file.good()) {
        std::getline(file, line);
        if (line != "") {
            std::vector<std::string> v = split(line, ',');
            long long int count = atoi(v[4].c_str());
            if (count >= -200) { //ignores drawn positions, whose MATE COUNT is -2147483647
                std::string key = v[0] + "," + v[1] + "," + v[2] + "," + v[3];
                rookMap[key] = v[5];
            }
        }
    }
    file.close();
    path = "kqk.txt";
    std::ifstream file1(path.c_str());

    while (file1.good()) {
        std::getline(file1, line);
        if (line != "") {
            std::vector<std::string> v = split(line, ',');
            long long int count = atoi(v[4].c_str());
            if (count >= -200) { //ignores drawn positions, whose MATE COUNT is -2147483647
                std::string key = v[0] + "," + v[1] + "," + v[2] + "," + v[3];
                queenMap[key] = v[5];
            }
        }
    }
    file1.close();
}

std::string EndgameTable::convertFEN(std::string fen, int upside, int side) {

    fen = split(fen, ' ')[0];
    if (upside == BLACK) {
        replace(fen, "q", "Q");
        replace(fen, "r", "R");
        replace(fen, "k", "?");
        replace(fen, "K", "k");
        replace(fen, "?", "K");
        if (side == WHITE) {
            fen += " b - - 0 1";
        } else {
            fen += " w - - 0 1";
        }
    } else {
        if (side == WHITE) {
            fen += " w - - 0 1";
        } else {
            fen += " b - - 0 1";
        }
    }
    return fen;

}

Line EndgameTable::probe(Board* board, int side) {

    int orig = board->getSide();
    std::string fen = board->getPosition();

    std::string key = FENToString(convertFEN(fen, side, board->getSide()));

    Line ans;
    std::vector<int> moves;
    if (board->isKRKEnding()) {
        if (rookMap.find(key) != rookMap.end()) {
            std::string s = rookMap[key];
            s.erase(std::remove(s.begin(), s.end(), '\r'), s.end());
            while (s != "-") {
                int m = -1;
                int * actualMoves = board->generatePseudoMoves();
                for (int j = 0; int move = actualMoves[j]; j++) {
                    if (move::toInputNotation(move) == s) {
                        m = move;
                        break;
                    }
                }
                delete[] actualMoves;
                if (m != -1) {
                    moves.push_back(m);
                    board->makeMove(m);
                    fen = board->getPosition();
                    key = FENToString(convertFEN(fen, side, board->getSide()));
                } else {
                    break;
                }
                s = rookMap[key];
                s.erase(std::remove(s.begin(), s.end(), '\r'), s.end());
            }
        }
    } else {
        if (queenMap.find(key) != queenMap.end()) {
            std::string s = queenMap[key];
            s.erase(std::remove(s.begin(), s.end(), '\r'), s.end());
            while (s != "-") {
                int m = -1;
                int * actualMoves = board->generatePseudoMoves();
                for (int j = 0; int move = actualMoves[j]; j++) {
                    if (move::toInputNotation(move) == s) {
                        m = move;
                        break;
                    }
                }
                delete[] actualMoves;
                if (m != -1) {
                    moves.push_back(m);
                    board->makeMove(m);
                    fen = board->getPosition();
                    key = FENToString(convertFEN(fen, side, board->getSide()));
                } else {
                    break;
                }
                s = queenMap[key];
                s.erase(std::remove(s.begin(), s.end(), '\r'), s.end());
            }
        }
    }
    for (int i = moves.size() - 1; i >= 0; i--) {
        board->undoMove(moves[i]);
    }
    int e = (orig == side) ? MATE_VALUE : -MATE_VALUE;
    ans = Line(moves, e, moves.size(), 0, 0);

    return ans;
}