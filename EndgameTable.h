/* 
 * File:   EndgameTable.h
 * Author: Steve
 *
 * Created on 03 July 2013, 11:42 AM
 */

#ifndef ENDGAMETABLE_H
#define	ENDGAMETABLE_H

#include <unordered_map>
#include <string>
#include "Board.h"
#include "Move.h"
#include <vector>
#include <fstream>
#include "Line.h"
class EndgameTable {
public:

    Line probe(Board*, int side);

    bool legal(std::string fen) {
        Board board;
        board.setPosition(fen);
        int * moves = board.generatePseudoMoves();
        for (int i = 0; int move = moves[i]; i++) {
            if (abs(move::getCapturedPiece(move)) == KING) {
                delete[] moves;
                return false;
            }
        }
        delete[] moves;
        return true;
    }


    std::vector<std::string> generateAllPositions(int);

    EndgameTable();
    void buildRook();
    void buildQueen();
    virtual ~EndgameTable();

    int negamax(int);
    void write();

    void read();



    int evaluate(std::vector<std::string> &fens, std::vector<std::string> &moves, std::vector<int> &depth);


private:

    bool canReach(std::string fen, const std::vector<std::string>& fens,const std::vector<int>& depth, int d) {

        int idx = find(fens, fen);
        if (idx == -1) {
            return false;
        }

        return depth[idx] == d;

    }

    bool isMate(std::string fen) {



        Board board;
        board.setPosition(fen);

        if (board.getSide() == WHITE)return false;

        int * movs = board.generatePseudoMoves();

        for (int i = 0; int move = movs[i]; i++) {

            if ((abs(move::getCapturedPiece(move)) != KING)) {

                if (board.makeMove(move)) {
                    return false;
                }
                board.undoMove(move);
            }
        }
        uint64_t king = board.getKing();
        board.swapSides();
        return board.attacks(king);
    }

    int find(const std::vector<std::string> &fens, std::string fen) {

        for (int i = 0; i < fens.size(); i++) {
            if (fens[i] == fen) {
                return i;
            }
        }
        return -1;
    }


    std::unordered_map<std::string, std::string> rookMap,queenMap;
    Board board;

    unsigned int seed, nodes;

    void replace(std::string &source, const std::string find, std::string replace) {

        size_t j;
        for (; (j = source.find(find)) != std::string::npos;) {
            source.replace(j, find.length(), replace);
        }
    }

    int size(int* moves) {
        int M = 0;
        while (moves[M]) M++;
        return M;
    }

    void shuffle(int * moves) {
        int M = size(moves);
        for (int i = 0; i < M; i++) {
            int j = (random(&seed) % (M - i)) + i;
            if (j != i) {
                int temp = moves[i];
                moves[i] = moves[j];
                moves[j] = temp;
            }
        }

    }

    std::string itoa(int n) {
        std::ostringstream convert;
        convert << n;
        return convert.str();
    }

    std::string convertFEN(std::string, int, int);


    std::string FENToString(std::string fen) {
        std::string wk, bk, wr, side;
        int row = 0, col = 0;
        std::string temp = split(fen, ' ')[0];
        for (int i = 0; i < temp.size(); i++) {
            if (temp[i] == '/') {
                row++;
                col = 0;
            } else if (temp[i] == 'K') {
                char c = (col + 'a');
                wk = c + itoa(8 - row);
                col++;
            } else if (temp[i] == 'k') {
                char c = (col + 'a');
                bk = c + itoa(8 - row);
                col++;
            } else if (temp[i] == 'R' || temp[i] == 'Q') {
                char c = (col + 'a');
                wr = c + itoa(8 - row);
                col++;
            } else {
                int space = (temp[i] - '0');
                for (int j = 0; j < space; j++) {
                    col++;
                }
            }
        }





        return wk + "," + wr + "," + bk + "," + split(fen, ' ')[1];
    }

    std::string arrayToFEN(int board[][8]) {

        std::string fen = "";

        int spaces;
        for (int i = 0; i < 8; i++) {
            spaces = 0;
            for (int j = 0; j < 8; j++) {
                switch (board[i][j]) {
                    case WHITE_KING:
                        if (spaces > 0) {
                            fen += itoa(spaces);
                            spaces = 0;
                        }
                        fen += "K";
                        break;
                    case BLACK_KING:
                        if (spaces > 0) {
                            fen += itoa(spaces);
                            spaces = 0;
                        }
                        fen += "k";
                        break;

                    case WHITE_ROOK:
                        if (spaces > 0) {
                            fen += itoa(spaces);
                            spaces = 0;
                        }
                        fen += "R";
                        break;

                    case WHITE_QUEEN:
                        if (spaces > 0) {
                            fen += itoa(spaces);
                            spaces = 0;
                        }
                        fen += "Q";
                        break;

                    default:
                        spaces++;
                }
            }
            if (spaces > 0) {
                fen += itoa(spaces);
                spaces = 0;
            }
            if (i != 7) {
                fen += "/";
            }
        }
        return fen;
    }

};

#endif	/* ENDGAMETABLE_H */

