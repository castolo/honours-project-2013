/* 
 * File:   ParallelSimulation.h
 * Author: mint
 *
 * Created on September 5, 2013, 12:11 PM
 */

#ifndef PARALLELSIMULATION_H
#define	PARALLELSIMULATION_H

#include "Simulation.h"

class ParallelSimulation : public Simulation {
public:
    ParallelSimulation();
    virtual ~ParallelSimulation();    
    void start();
};

#endif	/* PARALLELSIMULATION_H */

