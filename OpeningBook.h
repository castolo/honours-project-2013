/* 
 * File:   OpeningBook.h
 * Author: Workstation
 *
 * Created on 12 June 2013, 1:34 PM
 */

#ifndef OPENINGBOOK_H
#define	OPENINGBOOK_H

#include <unordered_map>
#include "OpeningMove.h"
#include "Board.h"
#include <vector>
#include <string>
#include<sstream>
#include<algorithm>

class OpeningBook {
public:

    static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
    }

    // trim from end

    static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
    }

    // trim from both ends

    static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
    }

    OpeningBook();

    void close() {
        open = false;
    }

    bool isOpen() {
        return open;
    }


    void write();
    void read();
    void build();

    OpeningMove getMove( Board*);
    std::vector<OpeningMove> getMoves( Board*);
    OpeningBook(const OpeningBook& orig);
    virtual ~OpeningBook();
    void addOpening(std::string, const std::vector<std::string>&);
 void print(const std::vector<OpeningMove>&);
private:

    
    double rateOpening(OpeningMove);
    
    int findMove(std::string, int*,std::string);
    
    void addStats();

    void replace(std::string &source, const std::string find, std::string replace) {

        size_t j;
        for (; (j = source.find(find)) != std::string::npos;) {
            source.replace(j, find.length(), replace);
        }
    }


    unsigned int seed;
    std::vector<uint64_t> keySet;
    bool open;
    std::unordered_map<uint64_t, std::vector<OpeningMove> > map;
};

#endif	/* OPENINGBOOK_H */

