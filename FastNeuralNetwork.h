/* 
 * File:   FastNeuralNetwork.h
 * Author: Workstation
 *
 * Created on 30 August 2013, 6:49 PM
 */

#ifndef FASTNEURALNETWORK_H
#define	FASTNEURALNETWORK_H


#include "Board.h"

class FastNeuralNetwork {
public:
    FastNeuralNetwork(int, int, int);
    FastNeuralNetwork();

    virtual ~FastNeuralNetwork();

    std::vector<double> activate(std::vector<double>) const;
    std::vector<double> activate(const Board&) const;
    void update(const std::vector<double> &);
    std::vector<double> getWeights() const;
    int getNumberOfWeights() const;
    //   void show() const;





private:

    int inputs;
    int outputs;
    int hidden;
    static unsigned int seed;

    double* inputLayer;
    double* hiddenLayer;
    double* outputLayer;



    //weights
    double** wInputHidden;
    double** wHiddenOutput;
    inline double activationFunction(double) const;
    void randomizeWeights();

};

#endif	/* FASTNEURALNETWORK_H */

//
///* 
// * File:   NeuralNetwork.h
// * Author: Workstation
// *
// * Created on 19 July 2013, 4:33 PM
// */
//
//#ifndef NEURALNETWORK_H
//#define	NEURALNETWORK_H
//#include "NeuralLayer.h"
//#include "Board.h"
//#include <cstdarg>
//
//class NeuralNetwork {
//public:
//    NeuralNetwork();
//    NeuralNetwork(int, ...);
//    virtual ~NeuralNetwork();
//    
//    std::vector<double> activate(std::vector<double>) const;
//    std::vector<double> activate(const Board&) const;
//    void update(const std::vector<double> &);
//    std::vector<double> getWeights() const;
//    int getNumberOfWeights() const;
//    void show() const;
//    
//private:
//    int inputs;
//    int outputs;
//    int hiddenLayers;    
//    std::vector<NeuralLayer> layers; //excludes input layer
//    double activationFunction(double) const;
//    
//
//
//
//};
//
//#endif	/* NEURALNETWORK_H */
//
