/* 
 * File:   Neuron.h
 * Author: Workstation
 *
 * Created on 15 July 2013, 5:09 PM
 */

#ifndef NEURON_H
#define	NEURON_H

#include <vector>
#include <time.h>

class Neuron {
public:
    Neuron(int);
    virtual ~Neuron();

    int getNumberInputs() const {
        return inputs;
    }

    std::vector<double> getWeights() const {
        return weights;
    }
void setWeight(int idx,double weight){
        weights[idx] = weight;
    }
private:
    
    
    
    int inputs;
    std::vector<double> weights;
    static unsigned int seed;

};

#endif	/* NEURON_H */

