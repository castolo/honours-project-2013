#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=mpicc
CCC=mpic++.openmpi
CXX=mpic++.openmpi
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MPI-Windows
CND_DLIB_EXT=dll
CND_CONF=MPIValgrind
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Board.o \
	${OBJECTDIR}/EndgameTable.o \
	${OBJECTDIR}/ExtendedSphinx.o \
	${OBJECTDIR}/FastNeuralNetwork.o \
	${OBJECTDIR}/GAParams.o \
	${OBJECTDIR}/HashBoard.o \
	${OBJECTDIR}/KillerHeuristic.o \
	${OBJECTDIR}/Line.o \
	${OBJECTDIR}/Match.o \
	${OBJECTDIR}/Move.o \
	${OBJECTDIR}/NeuralLayer.o \
	${OBJECTDIR}/NeuralNetwork.o \
	${OBJECTDIR}/Neuron.o \
	${OBJECTDIR}/OpeningBook.o \
	${OBJECTDIR}/OpeningMove.o \
	${OBJECTDIR}/Parallel.o \
	${OBJECTDIR}/ParallelSimulation.o \
	${OBJECTDIR}/ParallelTournament.o \
	${OBJECTDIR}/Simulation.o \
	${OBJECTDIR}/Sphinx.o \
	${OBJECTDIR}/Tournament.o \
	${OBJECTDIR}/TranspositionTable.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++0x
CXXFLAGS=-std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lm -lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	mpic++ -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Board.o: Board.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Board.o Board.cpp

${OBJECTDIR}/EndgameTable.o: EndgameTable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/EndgameTable.o EndgameTable.cpp

${OBJECTDIR}/ExtendedSphinx.o: ExtendedSphinx.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/ExtendedSphinx.o ExtendedSphinx.cpp

${OBJECTDIR}/FastNeuralNetwork.o: FastNeuralNetwork.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/FastNeuralNetwork.o FastNeuralNetwork.cpp

${OBJECTDIR}/GAParams.o: GAParams.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/GAParams.o GAParams.cpp

${OBJECTDIR}/HashBoard.o: HashBoard.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/HashBoard.o HashBoard.cpp

${OBJECTDIR}/KillerHeuristic.o: KillerHeuristic.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/KillerHeuristic.o KillerHeuristic.cpp

${OBJECTDIR}/Line.o: Line.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Line.o Line.cpp

${OBJECTDIR}/Match.o: Match.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Match.o Match.cpp

${OBJECTDIR}/Move.o: Move.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Move.o Move.cpp

${OBJECTDIR}/NeuralLayer.o: NeuralLayer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/NeuralLayer.o NeuralLayer.cpp

${OBJECTDIR}/NeuralNetwork.o: NeuralNetwork.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/NeuralNetwork.o NeuralNetwork.cpp

${OBJECTDIR}/Neuron.o: Neuron.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Neuron.o Neuron.cpp

${OBJECTDIR}/OpeningBook.o: OpeningBook.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/OpeningBook.o OpeningBook.cpp

${OBJECTDIR}/OpeningMove.o: OpeningMove.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/OpeningMove.o OpeningMove.cpp

${OBJECTDIR}/Parallel.o: Parallel.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Parallel.o Parallel.cpp

${OBJECTDIR}/ParallelSimulation.o: ParallelSimulation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/ParallelSimulation.o ParallelSimulation.cpp

${OBJECTDIR}/ParallelTournament.o: ParallelTournament.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/ParallelTournament.o ParallelTournament.cpp

${OBJECTDIR}/Simulation.o: Simulation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Simulation.o Simulation.cpp

${OBJECTDIR}/Sphinx.o: Sphinx.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Sphinx.o Sphinx.cpp

${OBJECTDIR}/Tournament.o: Tournament.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Tournament.o Tournament.cpp

${OBJECTDIR}/TranspositionTable.o: TranspositionTable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/TranspositionTable.o TranspositionTable.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -O -I. -I. -std=c++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
